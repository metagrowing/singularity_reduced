from osc4py3.as_eventloop import *
from osc4py3 import oscbuildparse
from sense_hat import SenseHat
import sys

def mapval(value, inputMin, inputMax, outputMin, outputMax):
    return ((value - inputMin) / (inputMax - inputMin) * (outputMax - outputMin) + outputMin)

osc_startup()
# 12345 is the port where singularity
# is listening for incomming OSC commands
osc_udp_client("192.168.2.4", 12345, "minibee_simulation")

sense = SenseHat()
# compass       disabled
# gyro          disabled
# accelerometer enabled
sense.set_imu_config(False,False,True)

while True:
    try:
        acceleration = sense.get_accelerometer_raw()
        x = acceleration ['x']
        y = acceleration ['y']
        z = acceleration ['z']
        x = mapval(x, -1, 1, 0.46, 0.65)
        y = mapval(y, -1, 1, 0.46, 0.65)
        z = mapval(z, -1, 1, 0.46, 0.65)

        msg = oscbuildparse.OSCMessage("/minibee/data", ",ifff", [9,x,y,z])
        osc_send(msg, "minibee_simulation")
        osc_process()

        print("x={0}, y={2}, z={2}".format(x,y,z))
    except Exception as err:
        print(str(err))
