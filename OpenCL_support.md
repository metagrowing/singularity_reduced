# OpenCL
> :warning: OpenCl support is cannot be used in singularity yet

[[_TOC_]]


## Structure of OpenCL related scenes

### ClPingPong structure
- This class can be used for for a range of simulations like reaction diffusion or Turing patterns.

- Some examples can be found in ready. https://github.com/GollyGang/ready Ready can export its domain specific language as OpenCL source code. For other examples ready uses OpenCL as native language.

- The OpenCL programs exported from Ready must be adapted to ClPingPong. But ClPingPong works not very different to the calculation strategy of ready.

#### Repeat Phase 0 and Phase 1
1. All data flow through the kernel function from upper buffers to the lowers buffer.
2. The data flow works parallel per pixel.
Pixel values can be modified by a kernel function.
The kernel function can use some input parameter.
3. After all OpenCL kernels have finished their jobs the result can be drawn by OpenGL
4. Then we had an buffer swap. But not physically copying or moving the memory regions. Instead only the indices to the memory regions are swapped.
5. Continue at step 1.

![](./doc/cl_ping_pong_datafow.png)


## Building singularity with OpenCl support

### Build with the scenes that are based on OpenCL
You can enable OpenCL:
- To enable OpenCL support edit `operate.h`. Switch from `#undef WITH_openCL` to `#define WITH_openCL`.
- And a line containing `ofxMSAOpenCL` to the `addons.make` file.

```c
// to enable OpenCL:
//   add an ofxMSAOpenCL line to addons.make
//   define WITH_openCL
#define WITH_openCL

// to disable OpenCL:
//   remove ofxMSAOpenCL from addons.make
//   undefine WITH_openCL
//#undef WITH_openCL
```

### OpenCL addon for openFrameworks:

| addon | link | description |
| --- | --- | --- |
| ofxMSAOpenCL | https://github.com/memo/ofxMSAOpenCL |  wrapper for OpenCL |
| ofxMSAPingPong | https://github.com/memo/ofxMSAPingPong | needed by ofxMSAOpenCL |

```bash
cd ~/of_v0.11.0_linux64gcc6_release/addons/
git clone https://github.com/memo/ofxMSAOpenCL
git clone https://github.com/memo/ofxMSAPingPong
```

### Install OpenCL run time support:
As root user install OpenCL support suitable for you operating system and your graphics card.

> :warning: This is only tested with the NVIDIA proprietary driver. Package nvidia-opencl-icd provides the NVIDIA installable client driver (ICD) for OpenCL.

> :warning: This is only tested Debian 10.

```bash
apt install ocl-icd-opencl-dev
apt install nvidia-opencl-icd
```

#### OpenGL on Debian 10
These packages where installed for OpneCL and the client driver (ICD) for OpenCL.

| package | version | architecture | description |
| --- | --- | --- | --- |
| nvidia-opencl-common     | 418.197.02-1              | amd64 | NVIDIA OpenCL driver - common files |
| nvidia-opencl-dev:amd64  | 9.2.148-7                 | amd64 | NVIDIA OpenCL development files |
| nvidia-opencl-icd:amd64  | 418.197.02-1              | amd64 | NVIDIA OpenCL installable <br> client driver (ICD) |
| ocl-icd-libopencl1:amd64 | 2.2.12-2                  | amd64 | Generic OpenCL ICD Loader |
| ocl-icd-opencl-dev:amd64 | 2.2.12-2                  | amd64 | OpenCL development files |
| opencl-c-headers         | 2.2~2019.01.17-g49f07d3-1 | all   | OpenCL (Open Computing Language) C header files |
| opencl-clhpp-headers     | 2.0.10+git26-g806646c-1   | all   | C++ headers for OpenCL development |
| opencl-headers           | 2.2~2019.01.17-g49f07d3-1 | all   | OpenCL (Open Computing Language) header files |

### Trouble shooting OpenCL
| error | when | description |
| --- | --- | --- |
| make[1]: *** No rule to make target of_v0.11.0_linux64gcc6_release/addons/obj/linux64/Release/ofxMSAOpenCL/./src/ | build | addons.make missed ofxMSAOpenCL line |
| fatal error: CL/opencl.h: No such file or directory #include <CL/opencl.h> | build | OpenCL headers are missing.<br>Install ocl-icd-opencl-dev package. |
| /usr/bin/ld.gold: error: cannot find -lOpenCL | build | OpenCL libraries are missing<br>Install ocl-icd-opencl-dev package. |
| [ error ] Error finding clDevices. | runtime | Singularity needs OpenCL support on the GPU because OpenGL buffers and OpenCL memory must be shared on the GPU. |
