# Singularity - WIP

## Singularity – original code
This is the source code for the visuals of [RoosnaFlak's](https://roosnaflak.com) new performance [Singularity](https://roosnaflak.com/performances/singularity/). Very much a work in progress, enter at own risk. Do feel free to scavenge the code for your own creative projects! Released under the [Peer Production License](http://wiki.p2pfoundation.net/Peer_Production_License#Source)

[[_TOC_]]


## Singularity - reduced
- This is a very reduced version to refactor the original code, improve stability and performance.

- `Configuration.h` is a central configuration place. Please recompile `singularity_reduced` to activate a different configuration.

- This version shows two windows 960x540. Half the size of singularity running in full screen mode. This is for debugging.

- To build the full screen version you have to change `screenMode = ScreenMode::WINDOW` in `Configuration.h` to `screenMode = ScreenMode::FULLSCREEN`.

- This reduced version did not contain images or videos from the performance to keep the repository small.

### File structure
```
├── bin
│   └── data
│       ├── dance_p1080     scaled videos 1920x1080
│       │   ├── kulliStoneDance_p1080.mp4
│       │   └── snowdance1_p1080.mp4
│       ├── dance_p540      scaled videos 960x540
│       │   ├── kulliStoneDance_p540.mp4
│       │   └── snowdance1_p540.mp4
│       ├── kulliStoneDance.mp4
│       ├── numbers         *.png images of numbers
│       ├── shader          OpenGL 3.x shaders
│       └── trees           *.png tree images
└── src                     main, ofApp, configuration ...
    ├── media               management for image pools and media streams
    ├── scene               base for all scene classes
    │   ├── black           all black
    │   ├── fragment        graphic effects made with fragment shaders only
    │   ├── numberflock     animation
    │   ├── physarum        animation
    │   └── testcard        test image
    ├── sensor              base for all sensor inputs
    │   └── minibee         mini bee accelerometer
    └── util                some helper classes
```

### Scene inheritance graph
Classes marked with `<<abstract>>` should not be instantiated directly. They are for common functionality.

![](./doc/scene_inherit_graph.png)

#### Scenes
| scene index | description                              |
| ---         | ---                                      |
| 0           | show test image for both windows         |
| 1           | show both windows completely black       |
| 2           | Physarum animation "blue"                |
| 3           | Physarum animation "orange"              |
| 4           | NumberFlock animation                    |
| 5           | Mixing two videos in the fragment shader |
| 6           | Glitch effects in the fragment shader    |


### Media files
- A List of media files used for testing can be found in file 'media_files'.

- Some movies are scaled to 960x540 and 1920x1080. Physarum works better when the size of drawing area and the dimension of the movie are equal.

#### Media stream inheritance graph
Classes marked with `<<interface>>` should not be instantiated directly. They are an interface for different implementations of media streams.

![](./doc/media_inherit_graph.png)

#### Scaling videos
To fit the full screen size.
```bash
cd bin/data/dance_p1080
ffmpeg -i ../kulliStoneDance.mp4 -vf scale=1920:1080 kulliStoneDance_p1080.mp4
ffmpeg -i ../snowdance1.mp4 -vf scale=1920:1080 snowdance1_p1080.mp4
```

To fit the full window size for debugging.
```bash
cd bin/data/dance_p540
ffmpeg -i ../kulliStoneDance.mp4 -vf scale=960:540 kulliStoneDance_p540.mp4
ffmpeg -i ../snowdance1.mp4 -vf scale=960:540 snowdance1_p540.mp4
```

### Scenes and media streams
- Scenes and media streams are separated.

- A scene did not load media files, but it can use media streams more like global containers.

- One or more media streams can be used over and over again in completely different scenes.

- Media streams are initialized and loaded during start up. Want to avoid that the system runs out of memory space during the show. (If there are errors -> show them early.)

- Media streams and scene classes are initialized in `ofApp::setup`


### Simple Lisp like functions
Everything that is time critical is implemented in C++ or GLSL shader code.
But small parts of singularity are dynamically reprogram-able. This is a fake Lisp. Only arithmetic expression in a "functional" style are supported.

This simple Lisp like functions are stored in files with the extension `sl`.
These files are monitored. When the file modification date is changed they are reloaded and activated in singularity. Just in time compilation while singularity is running.

Read more about the [Simple Lisp like functions](./simple_lisp_functions.md).

### Structure

#### ofApp structure
- All animations are derived from the base class `Scene`.

- All animations are managed in the vector `scenePool` as shared pointer.

- `ofApp` updates the scene `current` and displays it.
Thereby `ofApp` works without knowing the details of a specific animation.

- In the current implementation, only one animation is visible at a time. The same algorithm is executed on the BackWall as well as on the Floor.

- Each animation provides two drawing routines. One for the BackWall and one for the Floor.

- `ofApp` receives all key press events that go to the BackWall window. It only processes the general keys. The key press events are passed to all animations in the `scenePool` to trigger specific behavior in the animation.

#### Physarum structure
- For the trailmap Physarum uses a pixel buffer that is so large that it contains both the backwall and the floor. This means: at 1920x1080 full screen mode -> 1920x2160 trailmap size.

- If images are loaded with 8 bits per color channel in RGB format the memory layout is known. The algorithm uses pointers instead of functions to access single pixels. This allows a slightly higher frame rate

- `BackWallVideoPhysarum` and `MovieImagePhysarum` are two examples how to make different effects based on the `Physarum` class.

- The memory area of the trail map is sporadically corrupted by RGB images or videos. Or the trail map is changed by some specific algorithms. This method is based on the fact that today's CPUs can copy data very efficiently inside the memory of the CPU. However, it is C programming, without a safety net. For details see: `BackWallVideoPhysarum::patchPixelsBackWall()`,  `BackWallVideoPhysarum::patchPixelsFloor()`, `MovieImagePhysarum::patchPixelsBackWall()` and   `MovieImagePhysarum::patchPixelsFloor()`

### Key codes
The BackWall window must own the focus to process keys. Please click on this window first.

| key | description |
| ---    | ---                                                    |
| .      | toggle the visibility of the frames per second counter |
| ,      | show GUI, only some scenes have a GUI                  |
| q      | stop  with exit code 137                               |
| 0 .. 9 | switch to a specific scene                             |
| space  | switch to next scene                                   |
| a      | processed by Physarum                                  |
| s      | processed by Physarum                                  |
| d      | processed by Physarum                                  |
| f      | processed by Physarum                                  |
| g      | processed by Physarum                                  |
| z      | processed by Physarum                                  |
| x      | processed by Physarum                                  |


### OpenCl
> :warning: OpenCl support is cannot be used in singularity yet

Read more about the [OpenCl support](./OpenCL_support.md).

### Helper
| bash script         | description                                                                                       |
| ---                 | ---                                                                                               |
| clean_build_release | use parallel make to recompile singularity_reduced<br>Please set  OF_ROOT in your login script    |
| run_release         | tries to restart  after a crash. Exit singularity_reduced with ‘q’ to break the restart behavior. |
| list_media_files    | list media file used during testing.<br>Media files are not part of the repository.               |


### Memory checker
It is possible to run singularity with the memory checker valgrind. This can be used to find memory accesses outside the permissible range and similar errors.

- Programs run very slowly within valgrind.
- valgrind generates false positives. You have to look at the messages in detail.
- valgrind can only generate understandable stack traces if the binary contains a symbol table.
- The NVIDIA drivers lead to error messages. But since the source code is not public one can only hope that these are not fatal errors.

```bash
valgrind bin/singularity_reduced_debug
```
