# Simple Lisp like functions
This will be translated to byte running on the CPU

[[_TOC_]]

In the C++ code you can insert alien functions to continuously change parameters. These functions read global variables and sensor values to calculate the current value of the parameter to manipulate the animation. From the syntax these functions look like Lisp, but are only a limited approach so that singularity can be adjusted just in time without having to follow the C++ edit, compile, test cycle.

The functions are stored in sl files. A sl file that is currently in use is monitored for the last modified date. If you save this file from an editor, it will be integrated directly into the running program.

In the update functions of each scene, arithmetic expressions are
evaluated. These expressions make use of the global variables. The
result is assigned to the appropriate parameters. Other expressions are
evaluated on "Result >= 0". If the condition is met, the corresponding
restart, randomize or ... function will be executed.

## Restrictions
- This only a fake Lisp.
- All functions are without input parameters yet. They can only read from variables predefined in the C++ part.
- One function definition per line. (Will remove this restriction later.)
- A function must not include new line characters or line breaks. (Will remove this restriction later.)


## Examples
### Using in the C++ part
```clojure
; used in Physarum. Physarum is implemented in the 0 .. 255 range, unsigned char
; animate hue
(defun hue (+ 30 (* 10 (sin (* 0.05 f)))))
(defun sat 127)
(defun val 255)
```
In the C++ code you find examples searching the source in folder scene for execute0.
```cpp

void Physarum::reloadSl(const bool force) {
	bool loaded = mng.reloadSlProg(force, sl_src);
	if (loaded || force) {
		mng.compile(slHue, "hue", "0", sl_src);
		mng.compile(slSaturation, "sat", "0", sl_src);
		mng.compile(slValue, "val", "1.0", sl_src);
	}
}

void Physarum::update() {
	reloadSl(false);
	hue = slHue.rpn.execute0();
	sat = slSaturation.rpn.execute0();
	val = slValue.rpn.execute0();
...
```

### Using in the fragment shader
It is possible to calculate sl expressions in the C++ openFrameworks part and then pass them as uniforms to a GLSL fragment shader. This is done in FragmentScene.

```clojure
; p0 and p1 are send as uniforms ti the fragment shader
(defun p0 (* 50 (unoise (* 0.071 f) 0.1 0.3)))
(defun p1 (* 50 (unoise (* 0.053 f) 0.3 0.1)))
```

```cpp
#version 330
in vec2 texCoordVarying; // not used
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform int w; // width,  not used
uniform int h; // height, not used
uniform float p0; // generic parameter 0, calculated by a *.sl program
uniform float p1; // generic parameter 1, calculated by a *.sl program

#define width_of_two_stipes 30

void main()
{
    float m = int(gl_FragCoord.x) % width_of_two_stipes;
    if(m < (width_of_two_stipes / 2)) {
        vec3 cin0 = texture(tex0, vec2(gl_FragCoord.x,
                                       h-gl_FragCoord.y-p0)).xyz;
        // draw originnal inverted color channels
        outputColor = vec4(vec3(1)-cin0, 1.0);
    }
    else {
        vec3 cin1 = texture(tex1, vec2(gl_FragCoord.x,
                                       h-gl_FragCoord.y+p1)).xyz;
        // draw originnal color channels
        outputColor = vec4(cin1, 1.0);
    }
}
```

## Read-only variables
### Read internal variables
> :warning: This is still completely unfinished. Only the frame counter and 8 miniBees can be read.

| read from | description                                                              | example                                     |
| ---       | ---                                                                      | ---                                         |
| f         | frame counter,<br> incremented automatically after every displayed frame | (defun sat (+ 150 (* 100 (sin (* 0.1 f))))) |


### Read values from miniBee sensors
| read from | description | example |
| --- | --- |  --- |
| mb0_iscon<br>...<br>mb7_iscon | miniBee 0 to miniBee 7<br>returns `1` when data are received from the sensor<br>returns `-1` during timeout | (defun connectedLeft mb0_iscon) |
| mb0x<br>...<br>mb7x | miniBee 0 to miniBee 7<br> acceleration x axes | (defun mb0avr (/ (+ mb0x mb0y mb0z) 3)) |
| mb0y<br>...<br>mb7y | miniBee 0 to miniBee 7<br> acceleration y axes | |
| mb0z<br>...<br>mb7z | miniBee 0 to miniBee 7<br> acceleration z axes | |
| mb0x_smooth<br>...<br>mb7x_smooth | miniBee 0 to miniBee 7<br> smooth acceleration x axes | |
| mb0y_smooth<br>...<br>mb7y_smooth | miniBee 0 to miniBee 7<br> smooth acceleration y axes | |
| mb0z_smooth<br>...<br>mb7z_smooth | miniBee 0 to miniBee 7<br> smooth acceleration z axes | |
| mb0_delta<br>...<br>mb7_delta | miniBee 0 to miniBee 7<br> | |
| mb0_smooth_delta<br>...<br>mb7z_smooth_delta | miniBee 0 to miniBee 7<br> | (defun sdmb7 mb7_smooth_delta) |

| description | example |
| --- | --- |
| Testing if 4 sensors are connected.<br>A NOT connected sensor is represeted by `-1`<br>Only when all sensors are connected `min` evaluates to `1` |`(min mb0_iscon mb1_iscon mb2_iscon mb3_iscon)` |
| Testing if at least one sensor is connected.<br>A connected sensor is represeted by `1`<br>When at least one sensor is connected `max` evaluates to `1` |`(max mb0_iscon mb1_iscon mb2_iscon mb3_iscon)` |
| Simulation of a boolean not with floating point values. | `(* -1 mb0_iscon)`<br>`(* -1 (min mb0_iscon mb1_iscon))` |

### Read values from nanoKONTROL2
Input values can be read in expressions. It does not make sense to overwrite these values within an expression.

| read from | description | range |
| --- | --- |  --- |
| ks0 | nanoKONTROL2 slider 0 | 0.0 .. 1.0 |
| ks1 | nanoKONTROL2 slider 1 | 0.0 .. 1.0 |
| ks2 | nanoKONTROL2 slider 2 | 0.0 .. 1.0 |
| ks3 | nanoKONTROL2 slider 3 | 0.0 .. 1.0 |
| ks4 | nanoKONTROL2 slider 4 | 0.0 .. 1.0 |
| ks5 | nanoKONTROL2 slider 5 | 0.0 .. 1.0 |
| ks6 | nanoKONTROL2 slider 6 | 0.0 .. 1.0 |
| ks7 | nanoKONTROL2 slider 7 | 0.0 .. 1.0 |
| ks0 | nanoKONTROL2 potentiometer 0 | 0.0 .. 1.0 |
| ks1 | nanoKONTROL2 potentiometer 1 | 0.0 .. 1.0 |
| ks2 | nanoKONTROL2 potentiometer 2 | 0.0 .. 1.0 |
| ks3 | nanoKONTROL2 potentiometer 3 | 0.0 .. 1.0 |
| ks4 | nanoKONTROL2 potentiometer 4 | 0.0 .. 1.0 |
| ks5 | nanoKONTROL2 potentiometer 5 | 0.0 .. 1.0 |
| ks6 | nanoKONTROL2 potentiometer 6 | 0.0 .. 1.0 |
| ks7 | nanoKONTROL2 potentiometer 7 | 0.0 .. 1.0 |
| kb0 | nanoKONTROL2 S M R button 0 | -1.0, 0.0, 1.0 |
| kb1 | nanoKONTROL2 S M R button 1 | -1.0, 0.0, 1.0 |
| kb2 | nanoKONTROL2 S M R button 2 | -1.0, 0.0, 1.0 |
| kb3 | nanoKONTROL2 S M R button 3 | -1.0, 0.0, 1.0 |
| kb4 | nanoKONTROL2 S M R button 4 | -1.0, 0.0, 1.0 |
| kb5 | nanoKONTROL2 S M R button 5 | -1.0, 0.0, 1.0 |
| kb6 | nanoKONTROL2 S M R button 6 | -1.0, 0.0, 1.0 |
| kb7 | nanoKONTROL2 S M R button 7 | -1.0, 0.0, 1.0 |

![](./doc/nanoKONTROL2.jpg)

## Functions

### Simple arithmetic
| function | number of<br>parameters | description                                            | example                             |
| ---      | ---                     | ---                                                    | ---                                 |
| +        | 2 .. n                  | sum of all parameters                                  | (+ 0.1 mb0x)<br><br>(+ 1 2 3)       |
| -        | 2 .. n                  | subtract from first parameter all following parameters | (- 0.1 mb0x)<br><br>(- 1 2 3)       |
| *        | 2 .. n                  | multiply all parameters                                | (* 0.1 mb0x)<br><br>(* 0.05 f mb0x) |
| /        | 2 .. n                  | dived the first parameter by all following parameters  | (/ mb0x 2)<br><br>(+ 16 4 2)        |
| %        | 2                       | modulo                                                 | (% i 4)                             |
| sqr      | 1                       | square of x<br>same as (* x x)                         | (sqr mb0x)                          |
| sqrt     | 1                       | square root                                            | (sqrt mb0x)                         |

### Conditionals
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| zero? | 3 | If the first term evaluates to 0 the second term is evaluated. If not the third term is evaluated. | (zero?<br>(/ mb0x 1000)<br>1<br>(+ 1 mb0x)) |
| pos? | 3 | If the first term evaluates to an positive number the second term is evaluated. If not the third term is evaluated. | (pos?<br>mb0x<br>mb0x<br>(* -1.0 mb0x)) |

### Common functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| abs | 1 | absolute value | (abs mb0x) |
| floor | 1 | nearest integer less than x | (floor mb0x) |
| ceil | 1 | nearest integer greater than x | (ceil mb0x) |
| clamp | 3 | returns (min (max x minVal) maxVal) | (clamp mb0x 0.4 0.6) |
| sign | 1 | signum function | (sign y) |
| trunc | 1 | truncate | (trunc 1.3) |
| round | 1 | round to nearest | (round mb0x) |
| map | 5 | map parameter 1 form<br>input range given in parameter 2 and 3<br>output range given in parameter 3 and 4.<br>result is NOT clamped to output range. | (map mb0x 0 1 3 300) |

### Exponential functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| pow | 2 | returns x raised to the y power | (pow mb0x y) |
| log | 1 | returns the natural logarithm of x | (log (abs mb0x)) |
| log2 | 1 | returns the binary logarithm of x | (log2 (abs mb0x)) |

### Trigonometry functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| cos | 1 | cosine | (cos 0) |
| ucos | 1 | like cosine but returns values in the range 0.0 to 1.0 | (cos (* 0.1 f)) |
| acos | 1 | inverse cosine | (acos 0) |
| sin | 1 | sine | (sin 1.57) |
| usin | 1 | like sine but returns values in the range 0.0 to 1.0 | (usin -0.5) |
| asin | 1 | inverse sine | (asin 0) |
| tan | 1 | tangent | (tan 0.5) |
| atan | 2 | arc tanget for y over x | (atan y mb0x) |

### Merge values
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| max | 2 | Maximum value | (max 0 mb0x) |
| min | 2 | Minimum value | (min f 100) |
| mix | 3 | linear blend of x and y | (mix mb0x y 0.75) |

### Noise functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| rand | 0 | random numbers range 0.0 to 1.0 | (rand) |
| snoise | 3 | simplex noise function | (snoise mb0x y z) |
| unoise | 3 | like noise but returns values approximately in the range 0.0 to 1.0 | ((unoise mb0x y z)) |
| turb | 4 | turbulence noise function<br>last parameter stets the octaves | (turb 0.1 0.2 0.3 3) |

see: http://mrl.nyu.edu/~perlin/noise/

### Procedural textures functions
seen: Darwyn Peachey: Building Procedural Textures, in Texturing and Modeling, 1994

| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| step | 2 |  | (step y -0.99) |
| smooth | 3 | smooth step | (smooth -1 1 mb0x) |
| pulse | 3 |  | (puls -1 1 mb0x) |
| clamp | 3 |  | (clamp -1 1 mb0x) |
| box | 3 |  | (box -1 1 mb0x) |
| gamma | 2 |  | (gamma 0.7 mb0x) |
| gain | 2 |  | (gain 0.8 0.5) |
| bias | 2 |  | (bias 0.7 mb0x) |


### Geometric functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| distance | 4 | euclidean distance between two points  | (TODO) |
| mdist | 4 | Manhattan distance between two points  | (mdist 11 12 mb0x y) |
