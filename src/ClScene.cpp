#include "operate.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "MSAOpenCL.h"
#include "Configuration.h"
#include "ofApp.h"
#include "ClScene.h"

ClScene::ClScene() : Scene() {
}

ClScene::~ClScene() {
}

void ClScene::setup() {
	Scene::setup();

    /// allocate an OpenGL buffer with 4 float channels
    clOutFbo.allocate(CL_TILES_X * w, CL_TILES_Y * h, GL_RGBA32F_ARB);
    clOutFbo.begin();
    ofClear(0, 0, 0, 255);
    clOutFbo.end();
    /// OpenCL uses the same buffer
    clOutImage.initFromTexture(clOutFbo.getTexture());

    clProgName = selectClProgram();
    pathClProg = clProgName;
    if(!std::filesystem::exists(pathClProg)) {
        fatalClProg = true;
        ofLog(OF_LOG_FATAL_ERROR) << "Cannot access '" << clProgName << "'";
    }
    else {
        reloadClProg(true);
    }
}

bool ClScene::reloadClProg(const bool force) {
    if(fatalClProg || clProgName.empty())
        return false;

    bool need = force;
    if(!need) {
        long sourceModTime = std::filesystem::last_write_time(pathClProg);
        need = sourceModTime != clProgModTime;
    }

    if(need) {
        clProgModTime = std::filesystem::last_write_time(pathClProg);
        openCL.clearPrograms();
        ofLog(OF_LOG_NOTICE) << "compiling " << clProgName;
        pcl = openCL.loadProgramFromFile(clProgName);
        setup_kernel = openCL.loadKernel("image_setup");
        update_kernel = openCL.loadKernel("image_update");
        std::string kn = setup_kernel->getName();
        ofLog(OF_LOG_NOTICE) << "setup  kernel: " << (kn.empty() ? "N/A" : kn);
        kn = update_kernel->getName();
        ofLog(OF_LOG_NOTICE) << "update kernel: " << (kn.empty() ? "N/A" : kn);
    }
    return need;
}

void ClScene::drawLeftWall() {
    if(fatalClProg || clProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
        // draw the left wall first
        // make sure all OpenCL kernels have finished executing before drawing with OpenGL.
        openCL.finish();
        clOutImage.draw(0, 0);
    }
}

void ClScene::drawRightWall() {
    if(fatalClProg || clProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
        ofPushMatrix();
        ofTranslate(-w, 0);
        clOutFbo.draw(0, 0);
        ofPopMatrix();
    }
}

#endif // WITH_openCL
