#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "sensor/minibee/MiniBeeData.h"

/// \class base class for all scene classes
/// \brief provides empty default functions for derivative subclasses

// Mermaid diagram
//
//classDiagram
//    class Scene
//    <<abstract>> Scene
//
//    class Physarum
//    <<abstract>> Physarum
//
//    Scene <|-- Black
//    Scene <|-- NumberFlock
//    Scene <|-- Physarum
//    Scene <|-- Testcard
//    Physarum <|-- BackWallImagePhysarum
//    Physarum <|-- MovieImagePhysarum
//
//    Scene <|-- FragmentScene
//    FragmentScene <|-- MovieMixer
//    MovieMixer .. mix2channels_frag
//    MovieMixer .. stripes_frag
//    FragmentScene <|-- MovieDistortion
//    MovieDistortion .. distort_by_channel_frag
//    MovieDistortion .. glitch_frag
//
//    class Physarum
//    <<abstract>> Physarum
//    class ClScene
//    <<abstract>> ClScene
//    class ClPingPong
//    <<abstract>> ClPingPong
//
//    Scene <|-- ClScene
//    ClScene <|-- ClPingPong
//    ClPingPong <|-- ClMacCabe
//    ClMacCabe .. FastMcCabeBuffer_cl
//    FastMcCabeBuffer_cl : +image_setup
//    FastMcCabeBuffer_cl : +image_update

class Scene {
public:
	Scene();
	virtual ~Scene();

	/// file name for debugging
	virtual const char * sceneName() = 0;

	/// Set up scene class.
	/// Must be called only once.
	///
	/// Call setup after openFramework is fully initialized.
	/// This is for reservation of system resources like network connections.
	virtual void setup();

	virtual void exit();

	/// Per frame update of the data model.
	/// Overwrite this function only if a subclass requires its own specific algorithm.
	virtual void update();

	/// Per frame drawing on the left wall.
	/// Every subclasses should provide its own implementation.
	virtual void drawLeftWall();

	/// Per frame drawing on the right wall.
	/// Every subclasses should provide its own implementation.
	virtual void drawRightWall();

	/// Subclass specific key handler.
	/// Only subclasses with special key handling must provide an own implementation.
	virtual void keyPressed(int key);

	virtual void setSlProgName(string pn);
	virtual void reloadSl(const bool force);

protected:
	bool showGui = false;
	int w = 1;
	int h = 1;
};
