#pragma once
#include "operate.h"

#include "ofMain.h"

#ifdef WITH_openCL
#include "MSAOpenCL.h"
#endif
#include "Configuration.h"
#include "sensor/minibee/MiniBeeData.h"
#include "sensor/NanoKontrol2/NanoKontrol2_midi_input.h"
#include "Scene.h"

#ifdef WITH_openCL
extern msa::OpenCL openCL;
#endif


class ofApp: public ofBaseApp {

public:
	void setup();
	void update();
	void draw();
	void drawRight(ofEventArgs &args);
	void exit();
	void keyPressed(int key);

	shared_ptr<ofAppBaseWindow> rightProjectionWindow;

private:
	/// Selects which scene is visible.
	void switchScene(size_t nextIndex, const string& nextSl_name);

    void toggleSoundPlaying(size_t index);

	/// pointer to the visible scene
	shared_ptr<Scene> current;
	/// index of the visible scene
	size_t currentIndex;
	/// all scenes that are setup. But only one is active and visible.
	std::vector<std::shared_ptr<Scene>> scenePool;
    
    size_t currentMoviePool;

	bool showFrameRate = false;

#ifdef WITH_nanoKontrol2_midi_input
    NanoKontrol2 nk0;
#endif // WITH_js_dev_input

    const int port = 12345;
    ofxOscReceiver oscReceiver;
};
