#include "operate.h"
#include "Pool.h"
/// Every media container as a separate container.
/// The consequence is that one has to complete Pool.cpp consistently in 4 places.
///     1. add member declaration in Pool.h
///     2. add member definition in Pool.cpp
///     3. add to void Pool::clear()
///     4. add to void Pool::update()

MovieStream Pool::roadMoviePool;
MovieStream Pool::stoneMoviePool;
MovieStream Pool::hangingMoviePool;
ImageStream Pool::treeImagePool;
MovieStream Pool::snowyMoviePool;
MovieStream Pool::iceScapeMoviePool;
SoundStream Pool::soundPool;
#ifdef WITH_Cv
CvImageStream Pool::treeCvImagePool;
#endif // WITH_Cv

void Pool::clear() {
	roadMoviePool.clear();
	stoneMoviePool.clear();
	treeImagePool.clear();
    snowyMoviePool.clear();
    hangingMoviePool.clear();
    iceScapeMoviePool.clear();
	soundPool.clear();
#ifdef WITH_Cv
	treeCvImagePool.clear();
#endif // WITH_Cv
}

void Pool::update() {
	roadMoviePool.update();
	stoneMoviePool.update();
	treeImagePool.update();
	snowyMoviePool.update();
    iceScapeMoviePool.update();
	hangingMoviePool.update();
#ifdef WITH_Cv
	treeCvImagePool.update();
#endif // WITH_Cv
}

void Pool::pause() {
	roadMoviePool.switch_to_pause();
	stoneMoviePool.switch_to_pause();
	snowyMoviePool.switch_to_pause();
    iceScapeMoviePool.switch_to_pause();
	hangingMoviePool.switch_to_pause();
}

std::string Pool::dumpMovieState() {
    std::stringstream info;
    info.precision(5);
    info.width(9);
    info.flags(std::ios::fixed);
    info.flags(std::ios::right); //TODO
    info << "roadMovie "    << roadMoviePool.update_counter    << "\n";
    info << "stoneMovie "   << stoneMoviePool.update_counter   << "\n";
    info << "snowyMovie "   << snowyMoviePool.update_counter   << "\n";
    info << "hangingMovie " << hangingMoviePool.update_counter << "\n";
    info << "iceScapeMovie " << iceScapeMoviePool.update_counter << "\n";
    return info.str();
}


