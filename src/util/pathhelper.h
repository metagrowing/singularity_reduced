#pragma once
#include <string>
#include <vector>

#include "Configuration.h"

extern bool file_exists(const std::string &name);
extern std::vector<std::string> file_names(const std::string pattern);
