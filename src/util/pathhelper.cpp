#include <glob.h>
#include <sys/stat.h>

#include "ofMain.h"
#include "util/pathhelper.h"

bool file_exists(const std::string &name) {
	if(name.empty())
		return false;

    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

// see: https://stackoverflow.com/questions/8401777/simple-glob-in-c-on-unix-system
std::vector<std::string> file_names(const std::string pattern) {
    vector<string> filenames;
    glob_t glob_result;
    memset(&glob_result, 0, sizeof(glob_result));

    // do the glob operation
    int return_value = glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result);
    if(return_value != 0) {
		ofLogError() << "collecting file names '" << pattern << "' failed with return value " << return_value;
    } else {
        // collect all the filenames into a std::list<std::string>
        for(size_t i = 0; i < glob_result.gl_pathc; ++i) {
            filenames.push_back(string(glob_result.gl_pathv[i]));
        }
    }
    globfree(&glob_result);

    std::sort(filenames.begin(), filenames.end());
    return filenames;
}
