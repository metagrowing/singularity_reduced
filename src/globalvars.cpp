#include "globalvars.h"
#include "util/stringhelper.h"
#include "sensor/minibee/MiniBeeData.h"

class ofApp;

ofApp *apptarget = nullptr;

bool waiting_osc = true;
int num_cl_gpu = 0;

unsigned int fps = 25;
unsigned int frame = 0;

MiniBeeData mbData;
const size_t numMBs = 4;

float amplitude = 0;
float pitch0 = 0;
float pitch1 = 0;

float dimmer = 1.0;

float onset = 0;
float minimumThreshold = 0.05;
float decayRate = 0.05;

float u0 = 0;
float u1 = 0;
float u2 = 0;
float u3 = 0;
float u4 = 0;
float u5 = 0;
float u6 = 0;
float u7 = 0;

ScalarLisp slT0;
ScalarLisp slT1;
ScalarLisp slT2;
ScalarLisp slT3;
ScalarLisp slT4;
ScalarLisp slT5;
ScalarLisp slT6;
ScalarLisp slT7;
float t0 = 0;
float t1 = 0;
float t2 = 0;
float t3 = 0;
float t4 = 0;
float t5 = 0;
float t6 = 0;
float t7 = 0;

float kp0 = 0, kp1 = 0, kp2 = 0, kp3 = 0, kp4 = 0, kp5 = 0, kp6 = 0, kp7 = 0;
float ks0 = 0, ks1 = 0, ks2 = 0, ks3 = 0, ks4 = 0, ks5 = 0, ks6 = 0, ks7 = 0;
float kb0 = 0, kb1 = 0, kb2 = 0, kb3 = 0, kb4 = 0, kb5 = 0, kb6 = 0, kb7 = 0;

float h0 = 0;
float s0 = 0;
float v0 = 0;

float lx = 0;
float ly = 0;
float rx = 0;
float ry = 0;
float l2 = 0;
float r2 = 0;


#ifdef WITH_openCL
// Inspector inspector;
#endif // WITH_openCL
ScalarLisp slInspectChannels;
ScalarLisp slInspectX0;
ScalarLisp slInspectY0;
ScalarLisp slInspectR0;
ScalarLisp slInspectX1;
ScalarLisp slInspectY1;
ScalarLisp slInspectR1;
ScalarLisp slInspectX2;
ScalarLisp slInspectY2;
ScalarLisp slInspectR2;
ScalarLisp slInspectX3;
ScalarLisp slInspectY3;
ScalarLisp slInspectR3;

std::string dumpState() {
    std::stringstream info;
    info.precision(5);
    info.width(9);
    info.flags(std::ios::fixed);

    info
    << "\nf " << frame << '\n'

    << (mbData.isConnected(0) ? '+' : '-')
	<< "mb0 xyz map "
    << mbData.getMiniBee(0).getX() << "  "
    << mbData.getMiniBee(0).getY() << "  "
    << mbData.getMiniBee(0).getZ() << "  "
	<< " xyz smooth "
    << mbData.getMiniBee(0).getXSmooth() << "  "
    << mbData.getMiniBee(0).getYSmooth() << "  "
    << mbData.getMiniBee(0).getZSmooth() << "  "
	<< " delta "
    << mbData.getMiniBee(0).getDelta() << "  "
	<< " delta smooth "
    << mbData.getMiniBee(0).getDeltaSmooth() << '\n'

    << (mbData.isConnected(1) ? '+' : '-')
	<< "mb1 xyz map "
    << mbData.getMiniBee(1).getX() << "  "
    << mbData.getMiniBee(1).getY() << "  "
    << mbData.getMiniBee(1).getZ() << "  "
	<< " xyz smooth "
    << mbData.getMiniBee(1).getXSmooth() << "  "
    << mbData.getMiniBee(1).getYSmooth() << "  "
    << mbData.getMiniBee(1).getZSmooth() << "  "
	<< " delta "
    << mbData.getMiniBee(1).getDelta() << "  "
	<< " delta smooth "
    << mbData.getMiniBee(1).getDeltaSmooth() << '\n'

    << (mbData.isConnected(2) ? '+' : '-')
	<< "mb2 xyz map "
    << mbData.getMiniBee(2).getX() << "  "
    << mbData.getMiniBee(2).getY() << "  "
    << mbData.getMiniBee(2).getZ() << "  "
	<< " xyz smooth "
    << mbData.getMiniBee(2).getXSmooth() << "  "
    << mbData.getMiniBee(2).getYSmooth() << "  "
    << mbData.getMiniBee(2).getZSmooth() << "  "
	<< " delta "
    << mbData.getMiniBee(2).getDelta() << "  "
	<< " delta smooth "
    << mbData.getMiniBee(2).getDeltaSmooth() << '\n'

    << (mbData.isConnected(3) ? '+' : '-')
	<< "mb3 xyz map "
    << mbData.getMiniBee(3).getX() << "  "
    << mbData.getMiniBee(3).getY() << "  "
    << mbData.getMiniBee(3).getZ() << "  "
	<< " xyz smooth "
    << mbData.getMiniBee(3).getXSmooth() << "  "
    << mbData.getMiniBee(3).getYSmooth() << "  "
    << mbData.getMiniBee(3).getZSmooth() << "  "
	<< " delta "
    << mbData.getMiniBee(3).getDelta() << "  "
	<< " delta smooth "
    << mbData.getMiniBee(3).getDeltaSmooth() << '\n';

 //    << (mbData.isConnected(4) ? '+' : '-')
	// << "mb4 xyz map "
 //    << mbData.getMiniBee(4).getX() << "  "
 //    << mbData.getMiniBee(4).getY() << "  "
 //    << mbData.getMiniBee(4).getZ() << "  "
	// << " xyz smooth "
 //    << mbData.getMiniBee(4).getXSmooth() << "  "
 //    << mbData.getMiniBee(4).getYSmooth() << "  "
 //    << mbData.getMiniBee(4).getZSmooth() << "  "
	// << " delta "
 //    << mbData.getMiniBee(4).getDelta() << "  "
	// << " delta smooth "
 //    << mbData.getMiniBee(4).getDeltaSmooth() << '\n'

 //    << (mbData.isConnected(5) ? '+' : '-')
	// << "mb5 xyz map "
 //    << mbData.getMiniBee(5).getX() << "  "
 //    << mbData.getMiniBee(5).getY() << "  "
 //    << mbData.getMiniBee(5).getZ() << "  "
	// << " xyz smooth "
 //    << mbData.getMiniBee(5).getXSmooth() << "  "
 //    << mbData.getMiniBee(5).getYSmooth() << "  "
 //    << mbData.getMiniBee(5).getZSmooth() << "  "
	// << " delta "
 //    << mbData.getMiniBee(5).getDelta() << "  "
	// << " delta smooth "
 //    << mbData.getMiniBee(5).getDeltaSmooth() << '\n'

 //    << (mbData.isConnected(6) ? '+' : '-')
	// << "mb6 xyz map "
 //    << mbData.getMiniBee(6).getX() << "  "
 //    << mbData.getMiniBee(6).getY() << "  "
 //    << mbData.getMiniBee(6).getZ() << "  "
	// << " xyz smooth "
 //    << mbData.getMiniBee(6).getXSmooth() << "  "
 //    << mbData.getMiniBee(6).getYSmooth() << "  "
 //    << mbData.getMiniBee(6).getZSmooth() << "  "
	// << " delta "
 //    << mbData.getMiniBee(6).getDelta() << "  "
	// << " delta smooth "
 //    << mbData.getMiniBee(6).getDeltaSmooth() << '\n'

 //    << (mbData.isConnected(7) ? '+' : '-')
	// << "mb7 xyz map "
 //    << mbData.getMiniBee(7).getX() << "  "
 //    << mbData.getMiniBee(7).getY() << "  "
 //    << mbData.getMiniBee(7).getZ() << "  "
	// << " xyz smooth "
 //    << mbData.getMiniBee(7).getXSmooth() << "  "
 //    << mbData.getMiniBee(7).getYSmooth() << "  "
 //    << mbData.getMiniBee(7).getZSmooth() << "  "
	// << " delta "
 //    << mbData.getMiniBee(7).getDelta() << "  "
	// << " delta smooth "
 //    << mbData.getMiniBee(7).getDeltaSmooth();

    return info.str();
}

std::string dumpNkState() {
    std::stringstream info;
    info.precision(5);
    info.width(9);
    info.flags(std::ios::fixed);
    info.flags(std::ios::right); //TODO
    info <<   "\nkp0    " << kp0 << "\nkp1    " << kp1 << "\nkp2    " << kp2 << "\nkp3    " << kp3 << "\nkp4    " << kp4 << "\nkp5    " << kp5 << "\nkp6    " << kp6 << "\nkp7    " << kp7;
    info << "\n\nks0    " << ks0 << "\nks1    " << ks1 << "\nks2    " << ks2 << "\nks3    " << ks3 << "\nks4    " << ks4 << "\nks5    " << ks5 << "\nks6    " << ks6 << "\nks7    " << ks7;
    info << "\n\nkb0    " << kb0 << "\nkb1    " << kb1 << "\nkb2    " << kb2 << "\nkb3    " << kb3 << "\nkb4    " << kb4 << "\nkb5    " << kb5 << "\nkb6    " << kb6 << "\nkb7    " << kb7;
    return info.str();
}

#define STATE_FILE "bin/data/nanoKontrol2.state"
void saveState() {
    std::ofstream state_file;
    state_file.open(STATE_FILE);
    if(state_file.is_open()) {
        state_file.precision(5);
        state_file << "\n\nkp0," << kp0 << "\nkp1," << kp1 << "\nkp2," << kp2 << "\nkp3," << kp3 << "\nkp4," << kp4 << "\nkp5," << kp5 << "\nkp6," << kp6 << "\nkp7," << kp7;
        state_file << "\n\nks0," << ks0 << "\nks1," << ks1 << "\nks2," << ks2 << "\nks3," << ks3 << "\nks4," << ks4 << "\nks5," << ks5 << "\nks6," << ks6 << "\nks7," << ks7;
        state_file << "\n\nkb0," << kb0 << "\nkb1," << kb1 << "\nkb2," << kb2 << "\nkb3," << kb3 << "\nkb4," << kb4 << "\nkb5," << kb5 << "\nkb6," << kb6 << "\nkb7," << kb7;
        state_file.close();
    }
}

void loadState() {
    std::ifstream state_file;
    state_file.open(STATE_FILE);
    if(state_file.is_open()) {
        std::string line;
        while(getline(state_file, line)) {
            std::vector<std::string> parts;
            split(line, ',', parts);
            if(parts.size() == 2) {
                if(parts[0] == "kp0") kp0 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp1") kp1 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp2") kp2 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp3") kp3 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp4") kp4 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp5") kp5 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp6") kp6 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp7") kp7 = std::atof(parts[1].c_str());

                else if(parts[0] == "ks0") ks0 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks1") ks1 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks2") ks2 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks3") ks3 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks4") ks4 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks5") ks5 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks6") ks6 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks7") ks7 = std::atof(parts[1].c_str());

                else if(parts[0] == "kb0") kb0 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb1") kb1 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb2") kb2 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb3") kb3 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb4") kb4 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb5") kb5 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb6") kb6 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb7") kb7 = std::atof(parts[1].c_str());
            }
        }
        state_file.close();
    }
}

