#pragma once

#include "operate.h"

#include "ofMain.h"

#include "Configuration.h"
#include "media/ImageStream.h"
#include "media/MovieStream.h"
#include "media/CvImageStream.h"
#include "media/SoundStream.h"

/// \class Container for all media streams
///
/// Scenes and media data are separated.
/// So we can use the same media stream in different scenes.
///
/// To avoids trouble during the show:
/// Try to load all media data shortly after startup of singularity.
///     Or at least check if the files and streams can be allocated.
///     If files are missing or if we run out of memory wee should see this fast.
///
/// This will slow down the the start of the animation.

class Pool {
public:
	static void clear();

	/// Per frame update of the media data.
	/// This is called by ofApp.
	static void update();

	static void pause();

	static std::string dumpMovieState();

	/// Every media container as a separate container.
	/// The consequence is that one has to complete Pool.cpp consistently in 4 places.
	///     1. add member declaration in Pool.h
	///     2. add member definition in Pool.cpp
	///     3. add to void Pool::clear()
    ///     4. add to void Pool::update()
	static MovieStream roadMoviePool;
	static MovieStream stoneMoviePool;
    static MovieStream hangingMoviePool;
	static ImageStream treeImagePool;
	static MovieStream snowyMoviePool;
    static SoundStream soundPool;
    static MovieStream iceScapeMoviePool;
#ifdef WITH_Cv
	static CvImageStream treeCvImagePool;
#endif // WITH_Cv
};
