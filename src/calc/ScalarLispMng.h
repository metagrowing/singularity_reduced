#pragma once

#include <string>

#include "calc/ScalarLisp.h"

class ScalarLispMng {
public:
    ScalarLispMng();
    ~ScalarLispMng();
    void setSlProgName(string pn);
    bool reloadSlProg(const bool force, std::vector<std::string>& sl_src);
    int compile(ScalarLisp &sl, string key, string fallback, std::vector<std::string> src);

#ifdef WITH_testcases
    static void runTestCases();
#endif
    bool hasSlProg = false;

private:
    string scanForString(string key, std::vector<std::string> src);

    string slProgName;
    long sliProgModTime = 0;
    bool fatalSliProg = false;
    std::filesystem::path pathSlProg;
};
