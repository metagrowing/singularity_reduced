#include "ofMain.h"

#include "Configuration.h"
#include "globalvars.h"
#include "util/stringhelper.h"
#include "util/pathhelper.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"

ScalarLispMng::ScalarLispMng() {
}

ScalarLispMng::~ScalarLispMng() {
}

void ScalarLispMng::setSlProgName(string pn) {
	hasSlProg = false;
	if(!pn.empty()) {
		if(file_exists(pn)) {
			slProgName = pn;
			pathSlProg = pn;
			hasSlProg = true;
		}
		else {
			ofLog(OF_LOG_ERROR) << "missing file '" << pn << "'";
		}
	}
}

bool ScalarLispMng::reloadSlProg(const bool force, std::vector<std::string>& sl_src) {
    if(!hasSlProg || fatalSliProg || slProgName.empty())
        return false;

    bool need = force;
    if(!need) {
        long sourceModTime = std::filesystem::last_write_time(pathSlProg);
        need = sourceModTime != sliProgModTime;
    }

    if(need) {
    	ofLog(OF_LOG_NOTICE) << "reading " << pathSlProg;
    	sliProgModTime = std::filesystem::last_write_time(pathSlProg);
        std::ifstream file(slProgName);
        std::string line;
        sl_src.clear();
        while (std::getline(file, line)) {
            sl_src.push_back(line);
        }
    }
    return need;
}

int ScalarLispMng::compile(ScalarLisp &sl, string key, string fallback, std::vector<std::string> src) {
    if(key.empty())
        ofLog(OF_LOG_FATAL_ERROR) << "ScalarLispMng::compile Key to search for is empty";
    if(hasSlProg) {
		string pattern1 = "(defun " + key + " "; //TODO use a tokenizer
		string pattern2 = "(defun " + key + "(";
		size_t nargs = src.size();
		for(size_t ax = 0; ax < nargs; ++ax) {
	    	auto expr = cleanWhite(trim(src[ax]));
			if(expr.find(pattern1) == 0) {
				ofLog(OF_LOG_NOTICE) << "compiling " << src[ax];
				return sl.compile(trim(expr, pattern1, ")"));
			}
			if(expr.find(pattern2) == 0) {
				ofLog(OF_LOG_NOTICE) << "compiling " << src[ax];
				return sl.compile(trim(expr, pattern2, ")") + "(");
			}
		}
    }
    return sl.compile(fallback);
}

string ScalarLispMng::scanForString(string key, std::vector<std::string> src) {
    size_t nargs = src.size();
    string pattern1 = "(" + key + " ";//TODO use a tokenizer
    for(size_t ax = 0; ax < nargs; ++ax) {
        auto expr = trim(src[ax]);
        if(expr.find(pattern1) == 0) {
            //TODO use a tokenizer
            string v = trim(trim(expr.substr(pattern1.size()), ' ', ')'), '"', '"');
            return v;
        }
    }
    return "";
}

#ifdef WITH_testcases
void ScalarLispMng::runTestCases() {
    std::ifstream file("bin/data/sltest/hsv_test.sl");
    std::string line;
    std::vector<std::string> sl_src;
    while (std::getline(file, line)) {
        sl_src.push_back(line);
    }

    ScalarLispMng mng;
    ScalarLisp slHue;
    mng.compile(slHue,        "hue", "0",  sl_src);
    ScalarLisp slSaturation;
    mng.compile(slSaturation, "sat", "0",  sl_src);
    ScalarLisp slValue;
    mng.compile(slValue,      "val", "1.0", sl_src);

    frame = 0;
    {
    double hue = slHue.rpn.execute0();
    double saturation = slSaturation.rpn.execute0();
    double value = slValue.rpn.execute0();
    ofLog(OF_LOG_NOTICE) << hue << " " << saturation<< " "  << value;
    }

    frame = 1;
    {
    double hue = slHue.rpn.execute0();
    double saturation = slSaturation.rpn.execute0();
    double value = slValue.rpn.execute0();
    ofLog(OF_LOG_NOTICE) << hue << " " << saturation<< " "  << value;
    }
}
#endif
