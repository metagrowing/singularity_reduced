#pragma once

#include <map>

#include "Configuration.h"
#include "calc/ScalarRPN.h"

class opr_desc {
public:
    opr_desc() {
        token = 0;
        rpnoper = 0;
        rewind = 0;
    }
    opr_desc(int t, int o, int r) {
        token = t;
        rpnoper = o;
        rewind = r;
    }
    int token;
    int rpnoper;
    int rewind;
};

#define tUnknown 0
#define tEOF     1
#define tClose   2
#define tOpen    3
#define tNumber  4
#define tVar     5
#define tOprAdd  (tVar +  1)
#define tOprSub  (tVar +  2)
#define tOprMult (tVar +  3)
#define tOprDiv  (tVar +  4)
#define tOprIfPositive (tVar + 5)
#define tOprPow   (tVar +  6)
#define tOprSqrt  (tVar +  7)
#define tOprTan   (tVar +  8)
#define tOprSin   (tVar +  9)
#define tOprCos   (tVar + 10)
#define tOprAtan2 (tVar + 11)
//#define tOprAtan  (tVar + 12)
#define tOprLog   (tVar + 13)
#define tOprMix   (tVar + 14)
//#define tOprMixx  (tVar + 15)
#define tOprIfZero (tVar + 15)
#define tOprStep  (tVar + 16)
#define tOprPuls  (tVar + 17)
#define tOprClamp (tVar + 18)
#define tOprMax   (tVar + 19)
#define tOprMin   (tVar + 20)
#define tOprAbs   (tVar + 21)
#define tOprSmoothstep (tVar + 22)
#define tOprBoxstep (tVar + 23)
#define tOprMod   (tVar + 24)
#define tOprFloor (tVar + 25)
#define tOprCeil  (tVar + 26)
#define tOprGamma (tVar + 27)
#define tOprGain  (tVar + 28)
#define tOprBias  (tVar + 29)
#define tOprEuclideDistance (tVar + 30)
#define tOprAbsoluteDistance (tVar + 31)
#define tOprUNoise (tVar + 32)
#define tOprSNoise (tVar + 33)
#define tOprURandom (tVar + 34)
#define tOprTurbulence (tVar + 35)
#define tOprUSin   (tVar + 36)
#define tOprUCos   (tVar + 37)
#define tOprASin   (tVar + 38)
#define tOprACos   (tVar + 39)
#define tOprTrunc  (tVar + 40)
#define tOprRound  (tVar + 41)
#define tOprSign   (tVar + 42)
#define tOprLog2   (tVar + 43)
#define tOprSqr    (tVar + 44)
#define tOprMap    (tVar + 45)

class ScalarLisp {
public:
    ScalarLisp();
    virtual ~ScalarLisp();
    int compile(std::string src);

    ScalarRPN rpn;

private:
    std::string tokBuffer;

    bool isNumber(char test);
    void skip();
    void collect();
    void compileTerm();
    void appendVar(std::string &t);
    inline int genLabel() {
        return label_counter++;
    }
    void genCode();
    void calculateIfConst();
    void countParenthesis();

    const char *inp = nullptr;
    int len = 0;
    int ix = 0;
    int tok = tUnknown;
    int err = 0;
    std::map<std::string, opr_desc> map_opr;
    std::map<int, int> map_label;
    int label_counter = 0;
};
