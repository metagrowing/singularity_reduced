#pragma once
#include <random>

#include "operate.h"
#include "Configuration.h"
#include "calc/Noise.h"

#define oprStop        0
#define oprReturn      1 // reserved
#define oprPushConst   2
#define oprPushX       3
#define oprPushY       4
#define oprPushZ       5
#define oprPushW       6
#define oprPushI       7
#define oprPushJ       8
#define oprPushK       9
#define oprPushF      10
#define oprPushLx     11
#define oprPushLy     12
#define oprPushRx     13
#define oprPushRy     14
#define oprPushL2     15
#define oprPushR2     16
#define oprPushOnset  17
#define oprPushDimmer 18
#define oprPushU0     19
#define oprPushU1     20
#define oprPushU2     21
#define oprPushU3     22
#define oprPushU4     23
#define oprPushU5     24
#define oprPushU6     25
#define oprPushU7     26
#define oprPushMx     27
#define oprPushMy     28
#define oprPushH0     29
#define oprPushS0     30
#define oprPushV0     31

#define oprPushMb0x                (1*32)
#define oprPushMb0y                (oprPushMb0x + 1)
#define oprPushMb0z                (oprPushMb0x + 2)
#define oprPushMb0x_smooth         (oprPushMb0x + 3)
#define oprPushMb0y_smooth         (oprPushMb0x + 4)
#define oprPushMb0z_smooth         (oprPushMb0x + 5)
#define oprPushMb0_delta           (oprPushMb0x + 6)
#define oprPushMb0_smooth_delta    (oprPushMb0x + 7)
#define oprPushIsConnectedMb0      (oprPushMb0x + 8)

#define oprPushMb1x                (2*32)
#define oprPushMb1y                (oprPushMb1x + 1)
#define oprPushMb1z                (oprPushMb1x + 2)
#define oprPushMb1x_smooth         (oprPushMb1x + 3)
#define oprPushMb1y_smooth         (oprPushMb1x + 4)
#define oprPushMb1z_smooth         (oprPushMb1x + 5)
#define oprPushMb1_delta           (oprPushMb1x + 6)
#define oprPushMb1_smooth_delta    (oprPushMb1x + 7)
#define oprPushIsConnectedMb1      (oprPushMb1x + 8)

#define oprPushMb2x                (3*32)
#define oprPushMb2y                (oprPushMb2x + 1)
#define oprPushMb2z                (oprPushMb2x + 2)
#define oprPushMb2x_smooth         (oprPushMb2x + 3)
#define oprPushMb2y_smooth         (oprPushMb2x + 4)
#define oprPushMb2z_smooth         (oprPushMb2x + 5)
#define oprPushMb2_delta           (oprPushMb2x + 6)
#define oprPushMb2_smooth_delta    (oprPushMb2x + 7)
#define oprPushIsConnectedMb2      (oprPushMb2x + 8)

#define oprPushMb3x                (4*32)
#define oprPushMb3y                (oprPushMb3x + 1)
#define oprPushMb3z                (oprPushMb3x + 2)
#define oprPushMb3x_smooth         (oprPushMb3x + 3)
#define oprPushMb3y_smooth         (oprPushMb3x + 4)
#define oprPushMb3z_smooth         (oprPushMb3x + 5)
#define oprPushMb3_delta           (oprPushMb3x + 6)
#define oprPushMb3_smooth_delta    (oprPushMb3x + 7)
#define oprPushIsConnectedMb3      (oprPushMb3x + 8)

#define oprPushMb4x                (5*32)
#define oprPushMb4y                (oprPushMb4x + 1)
#define oprPushMb4z                (oprPushMb4x + 2)
#define oprPushMb4x_smooth         (oprPushMb4x + 3)
#define oprPushMb4y_smooth         (oprPushMb4x + 4)
#define oprPushMb4z_smooth         (oprPushMb4x + 5)
#define oprPushMb4_delta           (oprPushMb4x + 6)
#define oprPushMb4_smooth_delta    (oprPushMb4x + 7)
#define oprPushIsConnectedMb4      (oprPushMb4x + 8)

#define oprPushMb5x                (6*32)
#define oprPushMb5y                (oprPushMb5x + 1)
#define oprPushMb5z                (oprPushMb5x + 2)
#define oprPushMb5x_smooth         (oprPushMb5x + 3)
#define oprPushMb5y_smooth         (oprPushMb5x + 4)
#define oprPushMb5z_smooth         (oprPushMb5x + 5)
#define oprPushMb5_delta           (oprPushMb5x + 6)
#define oprPushMb5_smooth_delta    (oprPushMb5x + 7)
#define oprPushIsConnectedMb5      (oprPushMb5x + 8)

#define oprPushMb6x                (7*32)
#define oprPushMb6y                (oprPushMb6x + 1)
#define oprPushMb6z                (oprPushMb6x + 2)
#define oprPushMb6x_smooth         (oprPushMb6x + 3)
#define oprPushMb6y_smooth         (oprPushMb6x + 4)
#define oprPushMb6z_smooth         (oprPushMb6x + 5)
#define oprPushMb6_delta           (oprPushMb6x + 6)
#define oprPushMb6_smooth_delta    (oprPushMb6x + 7)
#define oprPushIsConnectedMb6      (oprPushMb6x + 8)

#define oprPushMb7x                (8*32)
#define oprPushMb7y                (oprPushMb7x + 1)
#define oprPushMb7z                (oprPushMb7x + 2)
#define oprPushMb7x_smooth         (oprPushMb7x + 3)
#define oprPushMb7y_smooth         (oprPushMb7x + 4)
#define oprPushMb7z_smooth         (oprPushMb7x + 5)
#define oprPushMb7_delta           (oprPushMb7x + 6)
#define oprPushMb7_smooth_delta    (oprPushMb7x + 7)
#define oprPushIsConnectedMb7      (oprPushMb7x + 8)

#define oprPushAmplitude           (9*32)
#define oprPushPitch0              (oprPushAmplitude + 1)
#define oprPushPitch1              (oprPushAmplitude + 2)

#define oprPushKP0    512
#define oprPushKP1    (oprPushKP0 + 1)
#define oprPushKP2    (oprPushKP0 + 2)
#define oprPushKP3    (oprPushKP0 + 3)
#define oprPushKP4    (oprPushKP0 + 4)
#define oprPushKP5    (oprPushKP0 + 5)
#define oprPushKP6    (oprPushKP0 + 6)
#define oprPushKP7    (oprPushKP0 + 7)

#define oprPushKS0    (oprPushKP0 + 8)
#define oprPushKS1    (oprPushKP0 + 9)
#define oprPushKS2    (oprPushKP0 + 10)
#define oprPushKS3    (oprPushKP0 + 11)
#define oprPushKS4    (oprPushKP0 + 12)
#define oprPushKS5    (oprPushKP0 + 13)
#define oprPushKS6    (oprPushKP0 + 14)
#define oprPushKS7    (oprPushKP0 + 15)

#define oprPushKB0    (oprPushKP0 + 16)
#define oprPushKB1    (oprPushKP0 + 17)
#define oprPushKB2    (oprPushKP0 + 18)
#define oprPushKB3    (oprPushKP0 + 19)
#define oprPushKB4    (oprPushKP0 + 20)
#define oprPushKB5    (oprPushKP0 + 21)
#define oprPushKB6    (oprPushKP0 + 22)
#define oprPushKB7    (oprPushKP0 + 23)


#define oprIfPositive 1024
#define oprIfZero     1025
#define oprLabel      1026
#define oprJmp        1027

#define oprAdd         (oprJmp +  1)
#define oprAdd2        (oprJmp +  2) // reserved
#define oprMult        (oprJmp +  3)
#define oprMult2       (oprJmp +  4) // reserved
#define oprSub         (oprJmp +  5)
#define oprSub1        (oprJmp +  6)
#define oprSub2        (oprJmp +  7) // reserved
#define oprDiv         (oprJmp +  8)
#define oprDiv1        (oprJmp +  9)
#define oprDiv2        (oprJmp + 10) // reserved
#define oprUNoise      (oprJmp + 11)
#define oprURandom     (oprJmp + 12)
#define oprSNoise      (oprJmp + 13)
#define oprTurbulence  (oprJmp + 14)
#define oprPow         (oprJmp + 15)
#define oprSqrt        (oprJmp + 16)
#define oprTan         (oprJmp + 17)
#define oprSin         (oprJmp + 18)
#define oprCos         (oprJmp + 19)
#define oprAtan2       (oprJmp + 20)
#define oprLog         (oprJmp + 21)
#define oprMix         (oprJmp + 22)
#define oprStep        (oprJmp + 23)
#define oprPuls        (oprJmp + 24)
#define oprClamp       (oprJmp + 25)
#define oprMax         (oprJmp + 26)
#define oprMin         (oprJmp + 27)
#define oprAbs         (oprJmp + 28)
#define oprSmoothstep  (oprJmp + 29)
#define oprBoxstep     (oprJmp + 30)
#define oprMod         (oprJmp + 31)
#define oprFloor       (oprJmp + 32)
#define oprCeil        (oprJmp + 33)
#define oprGamma       (oprJmp + 34)
#define oprGain        (oprJmp + 35)
#define oprBias        (oprJmp + 36)
#define oprEuclideanDistance (oprJmp + 37)
#define oprAbsoluteDistance  (oprJmp + 38)
#define oprUSin        (oprJmp + 39)
#define oprUCos        (oprJmp + 40)
#define oprASin        (oprJmp + 41)
#define oprACos        (oprJmp + 42)
#define oprTrunc       (oprJmp + 43)
#define oprRound       (oprJmp + 44)
#define oprSign        (oprJmp + 45)
#define oprLog2        (oprJmp + 46)
#define oprSqr         (oprJmp + 47)
#define oprMap         (oprJmp + 48)

#define rpn_epsilon 1.0E-7

class ScalarRPN {
public:
    ScalarRPN();
    virtual ~ScalarRPN();
    void seed(long seed);
    double execute4d3i(double x, double y, double z, double w, int i, int j, int k);
    inline double execute3d1i(double x, double y, double z, int i) {
        return isConst ? const_result : execute4d3i(x, y, z, 0.0, i, 0, 0);
    }
    inline double execute0() {
        return isConst ? const_result : execute4d3i(0.0, 0.0, 0.0, 0.0, 0, 0, 0);
    }

    void clear();
    void appendOpr(int o, double d = 0.0, int i = 0);

    void dumpOpr(int ip, std::string &a);
    std::string dump();

    bool isConst = false;
    double const_result = 0.0;

#ifdef DEBUG
		void protectRPN(bool cond, const char* const msg);
#endif

    ImprovedNoise improved_noise;
    std::mt19937 mt;
    std::uniform_real_distribution<double> dist;
    using dist_t = std::uniform_real_distribution <>;
    using param_t = dist_t::param_type;
    param_t range { 0.0, 1.0 };

    static const int maxTapeLen = 1024;
    int tapeLength = 0;
    int codeTape[maxTapeLen + 1];
    bool gotos = false;
    void *gotoTape[maxTapeLen + 1];
    int iOper[maxTapeLen + 1];
    double dOper[maxTapeLen + 1];
    double dStack[1024 + 1];

public:
#ifdef WITH_testcases
    static void runTestCases();
    void test1();
#endif
};
