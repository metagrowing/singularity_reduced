#include "ofMain.h"
#include "Configuration.h"
#include "MediaStream.h"

MediaStream::MediaStream() {
	fallback.allocate(Configuration::windowWidth, Configuration::windowHeight, OF_IMAGE_COLOR);
	fallback.clear();
}

MediaStream::~MediaStream() {
}

void MediaStream::setup() {
	w = ofGetWidth();
	h = ofGetHeight();
}

void MediaStream::append(std::string path, size_t limit) {
}

void MediaStream::clear() {
}

void MediaStream::resize(size_t w, size_t h) {
	this->w = w;
	this->h = h;
}

void MediaStream::update() {
}

void MediaStream::draw(size_t index) {
}

ofPixels& MediaStream::getRandom() {
	return fallback;
}

ofPixels& MediaStream::getAt(size_t index) {
	return fallback;
}
