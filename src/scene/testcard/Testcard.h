#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "Scene.h"

/// \class test image for both windows
/// \brief draws a small frame around both display areas
class Testcard: public Scene {
public:
	Testcard();
	virtual ~Testcard();
	virtual const char * sceneName();
	virtual void drawLeftWall();
	virtual void drawRightWall();

private:
	void drawBoundingBox(const string &txt);
};
