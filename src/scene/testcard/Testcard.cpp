#include "Testcard.h"

Testcard::Testcard() : Scene() {
}

Testcard::~Testcard() {
}

const char * Testcard::sceneName() {
	return __FILE__;
}

void Testcard::drawBoundingBox(const string &txt) {
	// TODO
	// If you activate this line the following error occurs:
	// If you switch back and forth between scene Testcard and scene MovieStream,
	// MovieStream is no longer displayed. Only a thin line appears at the edge.
	ofClear(0);
//	ofNoFill();
//  ofSetColor(255);
//	ofDrawRectangle(0.1, 0.1, w-0.1, h-0.1);
	ofDrawBitmapStringHighlight(txt, 200, 200);
}

void Testcard::drawLeftWall() {
	drawBoundingBox("Singularity left wall");
}

void Testcard::drawRightWall() {
	drawBoundingBox("Singularity  right wall");
}
