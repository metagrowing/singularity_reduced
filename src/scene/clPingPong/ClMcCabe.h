#pragma once
#include "operate.h"
#ifdef WITH_openCL

#include "ofxOsc.h"
#include "MSAOpenCL.h"
#include "Configuration.h"
#include "ClPingPong.h"

class ClMcCabe : public ClPingPong {
	public:
		ClMcCabe();
		virtual ~ClMcCabe();
		virtual const char * sceneName();
		virtual void setup();
	    virtual const string& selectClProgram();
		virtual void setInitial();
		virtual void setUpdate();
};
#endif // WITH_openCL
