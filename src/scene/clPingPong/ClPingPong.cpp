#include "operate.h"
#ifdef WITH_openCL
#include <random>

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "Configuration.h"
#include "ofApp.h"
#include "ClPingPong.h"

#define CL_TILES_X 2
#define CL_TILES_Y 1

ClPingPong::ClPingPong() : ClScene() {
}

ClPingPong::~ClPingPong() {
}

void ClPingPong::setup() {
	ClScene::setup();

//    pingPongIndex = 0;
	const size_t buflen = CL_TILES_X * w * CL_TILES_Y * h;
	for(auto bx = 0u; bx < half_as_much_buffers; ++bx) {
        clBuffers[2 * bx].initBuffer(buflen * sizeof(float), CL_MEM_READ_WRITE);
		clBuffers[2 * bx + 1].initBuffer(buflen * sizeof(float), CL_MEM_READ_WRITE);
	}

    glFinish();
    runSetupKernel();
}

void ClPingPong::runSetupKernel() {
    if(!fatalClProg) {
        pingPongIndex = 0;
        for (auto bx = 0u; bx < half_as_much_buffers; ++bx) {
            setup_kernel->setArg(2 * bx, clBuffers[2 * bx + pingPongIndex]);
            setup_kernel->setArg(2 * bx + 1, clBuffers[2 * bx + (1 - pingPongIndex)]);
        }
        setInitial();
        setup_kernel->setArg(2 * half_as_much_buffers,     initial_p0);
        setup_kernel->setArg(2 * half_as_much_buffers + 1, initial_p1);
        setup_kernel->setArg(2 * half_as_much_buffers + 2, initial_p2);
        setup_kernel->setArg(2 * half_as_much_buffers + 3, initial_p3);
        setup_kernel->setArg(2 * half_as_much_buffers + 4, initial_p4);
        setup_kernel->setArg(2 * half_as_much_buffers + 5, initial_p5);
        setup_kernel->setArg(2 * half_as_much_buffers + 6, initial_p6);
        setup_kernel->setArg(2 * half_as_much_buffers + 7, initial_p7);
        setup_kernel->setArg(2 * half_as_much_buffers + 8, clOutImage);
        setup_kernel->run2D(CL_TILES_X * w, CL_TILES_Y * h);
        pingPongIndex = 1;
    }
}

void ClPingPong::runUpdateKernel() {
    if(!fatalClProg) {
        for (auto bx = 0u; bx < half_as_much_buffers; ++bx) {
            update_kernel->setArg(2 * bx, clBuffers[2 * bx + pingPongIndex]);
            update_kernel->setArg(2 * bx + 1, clBuffers[2 * bx + (1 - pingPongIndex)]);
        }
        setUpdate();
        update_kernel->setArg(2 * half_as_much_buffers,     update_p0);
        update_kernel->setArg(2 * half_as_much_buffers + 1, update_p1);
        update_kernel->setArg(2 * half_as_much_buffers + 2, update_p2);
        update_kernel->setArg(2 * half_as_much_buffers + 3, update_p3);
        update_kernel->setArg(2 * half_as_much_buffers + 4, update_p4);
        update_kernel->setArg(2 * half_as_much_buffers + 5, update_p5);
        update_kernel->setArg(2 * half_as_much_buffers + 6, update_p6);
        update_kernel->setArg(2 * half_as_much_buffers + 7, update_p7);
        update_kernel->setArg(2 * half_as_much_buffers + 8, clOutImage);
        update_kernel->run2D(CL_TILES_X * w , CL_TILES_Y * h);
        pingPongIndex = 1 - pingPongIndex;
    }
}

void ClPingPong::update(){
    if(clProgName.empty())
        return;

    /// To avoid concurrency glitches all OpenGL work should be done
    /// before writing to the texture buffer from OpenCL.
    glFinish();
    if(reloadClProg(false)) {
        runSetupKernel();
    }
    for(size_t li=0; li<calculation_loops; ++li) {
        runUpdateKernel();
    }
}
#endif // WITH_openCL
