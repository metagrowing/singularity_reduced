#include "operate.h"
#ifdef WITH_openCL
#include <random>

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "Configuration.h"
#include "scene/clPingPong/ClMcCabe.h"

ClMcCabe::ClMcCabe() : ClPingPong() {
}

ClMcCabe::~ClMcCabe() {
}

const char * ClMcCabe::sceneName() {
	return __FILE__;
}

void ClMcCabe::setup() {
	ClPingPong::setup();

	/// run the image_update kernels twice before drawing to screen.
	/// The result will be more dynamic.
	/// Warning: If you set this value to high the GUI will get unresponsive,
	/// When investing all the GPU power for the numerical calculation,
	/// then there is nothing left for the GUI.
    calculation_loops = 1;
}

const string& ClMcCabe::selectClProgram() {
	static string prog_path = "bin/data/clMcCabe/FastMcCabeBuffer.cl";
	return prog_path;
}

void ClMcCabe::setInitial() {
	// seeds for the pseudo random number generator
    initial_p0 = ofGetElapsedTimeMillis();
    // p1 .. p7 are not used in this kernel
}

void ClMcCabe::setUpdate() {
    // p0 .. p7 are not used in this kernel
}

#endif // WITH_openCL
