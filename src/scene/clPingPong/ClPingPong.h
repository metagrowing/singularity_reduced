#pragma once
#include "operate.h"
#ifdef WITH_openCL

#include "ofxOsc.h"
#include "MSAOpenCL.h"
#include "Configuration.h"
#include "ClScene.h"

/// \class base class for all OpenCL programs with ping pong buffers of type float.
///
/// This can be used for for a range of simulations like reaction diffusion or Turing patterns.
/// Some examples can be found in ready. https://github.com/GollyGang/ready
/// Ready can export its domain specific language as OpenCL source code.
/// For some examples ready uses OpenCL as native language.
///
/// ClPingPong is not very different to the core of ready.
/// The OpenCL programs exported from Ready must be adapted to ClPingPong.
/// But this is a manageable effort.

// Mermaid diagram
//
//flowchart
//subgraph parmeter
//direction LR
//    p0
//    p1
//    px
//    pn
//end
//
//subgraph dataflow
//subgraph source
//    B0_ping
//    B1_ping
//    Bx_ping
//    Bn_ping
//end
//subgraph destination
//    B0_pong
//    B1_pong
//    Bx_pong
//    Bn_pong
//end
//end
//    p0(p0) --> kernel
//    p1(p1) --> kernel
//    px(p..) --> kernel
//    pn(pn) --> kernel
//    B0_ping[Buffer 0 ping] --> kernel((kernel)) --> B0_pong[Buffer 0 pong]
//    B1_ping[Buffer 1 ping] --> kernel((kernel)) --> B1_pong[Buffer 0 pong]
//    Bx_ping[Buffer .. ping] --> kernel((kernel)) --> Bx_pong[Buffer .. pong]
//    Bn_ping[Buffer n ping] --> kernel((kernel)) --> Bn_pong[Buffer n pong]

class ClPingPong : public ClScene {
	public:
		ClPingPong();
		virtual ~ClPingPong();
		virtual void setup();
		virtual void update();
		virtual void setInitial() = 0;
		virtual void setUpdate() = 0;

    protected:
		/// 8 different "chemicals"
        static const size_t half_as_much_buffers = 8;
        /// but 2*8 memory buffers
        static const size_t num_buffers = 2*half_as_much_buffers;
        /// ping pong buffers with the same size
        msa::OpenCLBuffer clBuffers[num_buffers];

        size_t calculation_loops = 1;
        int  pingPongIndex = 0;

        float initial_p0 = 0.0f;
        float initial_p1 = 0.0f;
        float initial_p2 = 0.0f;
        float initial_p3 = 0.0f;
        float initial_p4 = 0.0f;
        float initial_p5 = 0.0f;
        float initial_p6 = 0.0f;
        float initial_p7 = 0.0f;

        float update_p0 = 0.0f;
        float update_p1 = 0.0f;
        float update_p2 = 0.0f;
        float update_p3 = 0.0f;
        float update_p4 = 0.0f;
        float update_p5 = 0.0f;
        float update_p6 = 0.0f;
        float update_p7 = 0.0f;

        virtual void runSetupKernel();
        virtual void runUpdateKernel();
};
#endif // WITH_openCL
