#pragma once
#include <string>
#include <vector>

#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"

#define maxCharacters 127
#define maxRules 64
#define maxSuccessorCharacters 64
#define maxString (32 * 1024 * 1024)
#define sys0   0
#define sysDOL 1
#define sysOL  2
#define sysIL  3
#define sys2L  4

#define maxDepthLeafStack 100
#define maxPolyPoints 100

#define maxDepthBranchStack 1000
#define maxDepthLeafStack 100

class LsysMachine {
public:
    LsysMachine();
    virtual ~LsysMachine();
    void setIgnore(std::string ignore0);
    void setLsys(std::string ignore0,
                 std::string axiom0,
                 std::vector<std::string> predecessor0,
                 std::vector<std::string> successor0,
                 std::vector<std::string> leftContext0,
                 std::vector<std::string> rightContext0);
    const char * charInterpretation();

    int err = 0;

    void expand(size_t depth);
    bool testRightContext(size_t actPos, char testCharacter);
    bool testLeftContext(size_t actPos, char testCharacter);
    char mapChar(const std::string& src, size_t i);
    void postProcess();
    void filter();
    const std::stringstream& toCodeString();

    std::string axiom;
    bool ignoreCharacter[maxCharacters];
    size_t src_length = 0;
    char* src_string = nullptr;
    size_t dst_length = 0;
    char* dst_string = nullptr;
    int typeOfLSystem = sys0;
    int numRules[maxCharacters];
    int len_rule_pred[maxCharacters][maxRules];
    char rule_pred[maxCharacters][maxRules][maxSuccessorCharacters];
    char lContext[maxCharacters][maxRules];
    char rContext[maxCharacters][maxRules];

    ScalarLisp** dst_sl = nullptr;
    ScalarLisp** src_sl = nullptr;
    ScalarLisp* rule_pred_sl[maxCharacters][maxRules][maxSuccessorCharacters];

    ScalarLisp slDepth;

private:
    char* rule_buf0 = nullptr;
    char* rule_buf1 = nullptr;
    ScalarLisp** sl_buf0 = nullptr;
    ScalarLisp** sl_buf1 = nullptr;
    std::stringstream code;

    void setParamtericSuccessor(const std::vector<std::string> &successor, size_t pred_char, size_t vi);
    bool isLeNumber(char c);
};
