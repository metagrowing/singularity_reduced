#include <time.h>
#include <stdlib.h>

#include "ofMain.h"

#include "globalvars.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "lsys/LsysMachine.h"
#include "lsys/LsysTurtle.h"
#include "lsys/LsysCurve.h"
#include "color/Palette.h"
#include "LsysScene.h"

LsysScene::LsysScene() : Scene() {
}

LsysScene::~LsysScene() {
}

const char * LsysScene::sceneName() {
	return __FILE__;
}

void LsysScene::setLm(LsysMachine& lm,
        const std::string& ignore,
        const std::string &axiom,
        const std::vector<std::string> &predecessor,
        const std::vector<std::string> &successor,
        const std::vector<std::string> &leftContext,
        const std::vector<std::string> &rightContext,
        const std::string depth,
        const std::string forward,
        const std::string angle) {
    lm.setLsys(ignore, axiom, predecessor, successor, leftContext, rightContext);

    int compile_err = lm.slDepth.compile(depth.empty() ? "2" : depth);
    if(compile_err) {
        ofLog(OF_LOG_ERROR) << depth << " syntax error found " << compile_err;
    }
    compile_err = it.slForward.compile(forward.empty() ? "1" : forward);
    if(compile_err) {
        ofLog(OF_LOG_ERROR) << forward << " syntax error found " << compile_err;
    }
    compile_err = it.slAngel.compile(angle.empty() ? "0.08726646259971647" : angle);
    if(compile_err) {
        ofLog(OF_LOG_ERROR) << angle << " syntax error found " << compile_err;
    }
    compile_err = ic.slForward.compile(forward.empty() ? "1" : forward);
    if(compile_err) {
        ofLog(OF_LOG_ERROR) << forward << " syntax error found " << compile_err;
    }
    compile_err = ic.slAngel.compile(angle.empty() ? "0.08726646259971647" : angle);
    if(compile_err) {
        ofLog(OF_LOG_ERROR) << angle << " syntax error found " << compile_err;
    }

    frame = 0;
    lm.expand(lm.slDepth.rpn.execute0());
    lm.postProcess();
#ifdef WITH_testcases
    ofLog(OF_LOG_NOTICE) << "expand\n" << lm.charInterpretation();
#endif // WITH_testcases
}

//void LsysScene::setL1() {
//    // "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
//    // page 10, figure 1.9 f
//    // node rewriting
//    std::string ignore = "+-";
//    std::string axiom = "F-F-F-F";
//    std::vector<std::string> predecessor = { "F" };
//    std::vector<std::string> successor =   { "F-F+F+FF-F-F+F" };
//    std::vector<std::string> leftContext;
//    std::vector<std::string> rightContext;
//    setLm(l1, ignore, axiom, predecessor, successor, leftContext, rightContext,
//                           "4", // depth
//                           "1", // default forward step size
//                           "(* 0.5 3.14159)"); // default angel
//    l = &l1;
//}

void LsysScene::setL1() {
    std::string ignore = "+-";
    std::string axiom = "-'A";
    std::vector<std::string> predecessor = { "A",        };
    std::vector<std::string> successor =   { "F(0.1)[+'(2)F|F]A", };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l1, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "100", // depth
                           "(* 10 (snoise (* 0.5 x) 1 (* 0.001 f)))", // default forward step size
                           "(* (/ 90 180) 3.14159)"); // default angel

    l = &l1;
    t = &it;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefaultColors();
}

//// drunk-walk-II-random-lattice-walk
//void LsysScene::setL1() {
//    std::string ignore = "+";
//    std::string axiom = "F";
//    std::vector<std::string> predecessor = { "F",  "F",   "F",    "F",     };
//    std::vector<std::string> successor =   { "FF", "F+F", "F++F", "F+++F", };
//    std::vector<std::string> leftContext;
//    std::vector<std::string> rightContext;
//    setLm(l1, ignore, axiom, predecessor, successor, leftContext, rightContext,
//                           "10", // depth
//                           "1", // default forward step size
//                           "(* (/ 90 180) 3.14159)"); // default angel
//
//    l = &l1;
//    t = &it;
//    t->init(l->dst_length, l->dst_string, l->dst_sl);
//    t->initCanvas(w, h, 0.05);
//    t->lut.setDefaultColors();
//}

//// drunk-walk-I-brownian-motion
//void LsysScene::setL1() {
//    std::string ignore = "+";
//    std::string axiom = "''F";
//    std::vector<std::string> predecessor = { "F",              };
//    std::vector<std::string> successor =   { "F+(snoise x y 0.1)F" };
//    std::vector<std::string> leftContext;
//    std::vector<std::string> rightContext;
//    setLm(l1, ignore, axiom, predecessor, successor, leftContext, rightContext,
//                           "18", // depth
//                           "1", // default forward step size
//                           "(* (/ 90 180) 3.14159)"); // default angel
//
//    l = &l1;
//    t = &it;
//    t->init(l->dst_length, l->dst_string, l->dst_sl);
//    t->initCanvas(w, h, 0.05);
//    t->lut.setDefaultColors();
//}

//// islands and lakes
//void LsysScene::setL1() {
//    std::string ignore = "+-";
//    std::string axiom = "F-F-F-F";
//    std::vector<std::string> predecessor = { "F",                                  "f"};
//    std::vector<std::string> successor =   { "F-f+FF-F-FF-Ff-FF+f-FF+F+FF+Ff+FFF", "ffffff" };
//    std::vector<std::string> leftContext;
//    std::vector<std::string> rightContext;
//    setLm(l1, ignore, axiom, predecessor, successor, leftContext, rightContext,
//                           "2", // depth
//                           "1", // default forward step size
//                           "1.57079632679"); // default angel
//
//    l = &l1;
//    t = &it;
//    t->init(l->dst_length, l->dst_string, l->dst_sl);
//    t->initCanvas(w, h, 0.05);
//    t->lut.setDefaultColors();
//}

void LsysScene::setL2() {
    // "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
    // page 25, figure 1.24 c
    // edge rewriting
    std::string ignore = "+-";
    std::string axiom = "F";
    std::vector<std::string> predecessor = { "F" };
    std::vector<std::string> successor =   { "FF-[-F+F+F]+[+F-F-F]" };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l2, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "4", // depth
                           "1", // default forward step size
                           "(* (/ 20.5 180) 3.14159)"); // default angel
    l = &l2;
    t = &ic;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefault();
}

void LsysScene::setL3() {
    // "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
    // page 25, figure 1.24 d
    // node rewriting
    std::string ignore = "+-";
    std::string axiom = "X";
    std::vector<std::string> predecessor = { "X",            "F" };
    std::vector<std::string> successor =   { "F[+X]F[-X]+X", "FF" };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l3, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "8", // depth
                           "1", // default forward step size
//                           "(* 0.005 f)");
                           "(* (/ 20 180) 3.14159)"); // default angel
    l = &l3;
    t = &it;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefault();
}

void LsysScene::setL4() {
    // "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
    // page 35, figure 1.31 b
    // Hogeweg and Hesper
    std::string ignore = "+-F";
    std::string axiom = "F1F1F1F1";
    std::vector<std::string> leftContext  = { "0", "0",        "0", "0", "1", "1",   "1", "1", "",  "",  };
    std::vector<std::string> predecessor  = { "0", "0",        "1", "1", "0", "0",   "1", "1", "+", "-", };
    std::vector<std::string> rightContext = { "0", "1",        "0", "1", "0", "1",   "0", "1", "",  "",  };
    std::vector<std::string> successor    = { "1", "1[-F1F1]", "1", "1", "0", "1F1", "1", "0", "-", "+", };
    setLm(l4, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "30", // depth
                           "1", // default forward step size
                           "(* (/ 22.5 180) 3.14159)"); // default angel
    l = &l4;
    t = &it;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefault();
}

void LsysScene::setL5() {
    // "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
    // page 35, figure 1.31 b
    // Hogeweg and Hesper
    std::string ignore = "+-F";
    std::string axiom = "F1F1F1F1";
    std::vector<std::string> leftContext  = { "0", "0",        "0", "0", "1", "1",   "1", "1", "",  "",  };
    std::vector<std::string> predecessor  = { "0", "0",        "1", "1", "0", "0",   "1", "1", "+", "-", };
    std::vector<std::string> rightContext = { "0", "1",        "0", "1", "0", "1",   "0", "1", "",  "",  };
    std::vector<std::string> successor    = { "1", "1[-F1F1]", "1", "1", "0", "1F1", "1", "0", "-", "+", };
    setLm(l5, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "30", // depth
                           "(+ 1.2 (snoise x y (* 0.001 f)))", // default forward step size
                           "(+ 0.4 (* 0.2 (snoise y x (* 0.005 f))))"); // default angel
    l = &l5;
    t = &it;
    t->expand_only = true;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefault();
}

void LsysScene::setL6() {
    // "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
    // page 123, figure 5.5
    std::string ignore = "+-";
    std::string axiom = "[A][B]";
    std::vector<std::string> predecessor = { "A",          "B",          "C",  };
    std::vector<std::string> successor =   { "[+A{.].C.}", "[-B{.].C.}", "'fC", };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l6, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "12", // depth
                           "1", // default forward step size
                           "0.4"); // default angel
    l = &l6;
    t = &it;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefaultTransparents();
}

void LsysScene::setL7() {
    std::string ignore = "+-";
    std::string axiom = "'FU";
    std::vector<std::string> predecessor = { "U",             "X",             "Y",             "Z",      "A",          "B",          "C",  };
    std::vector<std::string> successor =   { "F[-FX]F[+FX]X", "F[-FY]F[+FY]Y", "F[-FZ]F[+FZ]Z", "'[A][B]", "[+A{.].C.}", "[-B{.].C.}", "F(min 0.05 (* f 0.0005))C", };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l7, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "16", // depth
                           "1", // default forward step size
                           "0.6"); // default angel
    l = &l7;
    t = &it;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefaultTransparents();
}

void LsysScene::setL8() {
    std::string ignore = "+-";
    std::string axiom = "F";
    std::vector<std::string> predecessor = { "F" };
    std::vector<std::string> successor =   { "F[-F][+(+ 0.4 (* 0.2 (sin (* 0.05 f))))F]" };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l8, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "9", // depth
                           "(- 10 i)", // default forward step size
                           "0.4"); // default angel
    l = &l8;
    t = &it;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefault();
}

void LsysScene::setL9() {
    // "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
    // animate version of page 25, figure 1.24 c
    // edge rewriting
    std::string ignore = "+-";
    std::string axiom = "F";
    std::vector<std::string> predecessor = { "F" };
    std::vector<std::string> successor =   { "F(+ 1.1 (snoise x y (* 0.001 f)))-[-F+F+F]+[+F-F-F]" };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l9, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "4", // depth
                           "0.1", // default forward step size
                           "(* (/ 20.5 180) 3.14159)"); // default angel
    l = &l9;
//    ofLog(OF_LOG_NOTICE) << " " << l->charInterpretation();
    t = &ic;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefault();
}

void LsysScene::setL0() {
    // Random walk as non deterministic L-System
    std::string ignore = "+-";
    std::string axiom = "F";
    std::vector<std::string> predecessor = { "F",                      "F" };
    std::vector<std::string> successor =   { "'F+F(sin (* 0.0211 f))", ",F-F(sin (* 0.0131 f))" };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l0, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "12", // depth
                           "1", // default forward step size
                           "(+ 0.4 (* 0.1 (sin (* 0.0031 f))))"); // default angel
    l = &l0;
//    ofLog(OF_LOG_NOTICE) << " " << l->charInterpretation();
    t = &it;
    t->expand_only = true;
    t->init(l->dst_length, l->dst_string, l->dst_sl);
    t->initCanvas(w, h, 0.05);
    t->lut.setDefaultColors();
}

void LsysScene::setup() {
	Scene::setup();

    ofTrueTypeFont::setGlobalDpi(96);
    infofont.load("mono", 14);
    frame = 0;
    t = &it;
    current_l = 9;
    setL9();
}


void LsysScene::update() {
}

void LsysScene::drawLeftWall() {
    if(l && t) {
        t->expandBoundigBox();
        t->draw();
        if(showGui) {
        ofSetColor(ofColor::white);
        	infofont.drawString(l->toCodeString().str(), 20, 30);
        }
    }
    ++frame;
}

void LsysScene::keyPressed(int key) {
    bool exp = false;
    switch(key) {
    case 'l':
    	switch(current_l) {
			case 0: setL0(); break;
			case 1: setL1(); break;
			case 2: setL2(); break;
			case 3: setL3(); break;
			case 4: setL4(); break;
			case 5: setL5(); break;
			case 6: setL6(); break;
			case 7: setL7(); break;
			case 8: setL8(); break;
			case 9: setL9(); break;
    	}
    	current_l = (current_l + 1) % 10;
        break;

    case 'a':
        exp = t->expand_only;
        t = &it;
        t->init(l->dst_length, l->dst_string, l->dst_sl);
        t->expand_only = exp;
        t->initCanvas(w, h, 0.05);
        break;
    case 's':
        exp = t->expand_only;
        t = &ic;
        t->init(l->dst_length, l->dst_string, l->dst_sl);
        t->expand_only = exp;
        t->initCanvas(w, h, 0.05);
        break;

    case 'w':
        t->lut.setDefault();
        break;
    case 'c':
        t->lut.setDefaultColors();
        break;
    case 'e':
        t->lut.setDefaultTransparents();
        break;
    }
}
