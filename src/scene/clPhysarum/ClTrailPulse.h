#pragma once
#include "operate.h"
#ifdef WITH_openCL

#include "ofxOsc.h"
#include "MSAOpenCL.h"
#include "Configuration.h"
#include "ClPhysarum.h"

class ClTrailPulse : public ClPhysarum {
	public:
		ClTrailPulse();
		virtual ~ClTrailPulse();
		virtual const char * sceneName();
		virtual void setup();
	    virtual const string& selectDefaultSlProgram();
	    virtual const string& selectClProgram();
};
#endif // WITH_openCL
