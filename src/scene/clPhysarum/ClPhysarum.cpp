#include "operate.h"
#ifdef WITH_openCL

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "globalvars.h"
#include "Pool.h"
#include "calc/FloatArrays.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "scene/clPhysarum/ClPhysarum.h"


ClPhysarum::ClPhysarum() : ClScene() {
}

ClPhysarum::~ClPhysarum() {
    delete[] position;
    delete[] velocity;
    delete[] heading;
delete[] dst;
}

void ClPhysarum::init_buffers() {
    pingPongIndex = 0;
    if (position == nullptr)
        position = floats.uniform(0.0f, 1.0f, 2 * agentlength);
    clPositionBuffers[0].write(position, 0, agentlength * 2 * sizeof(float));

    if (velocity == nullptr)
        velocity = floats.uniform(0.001f, 0.02f, agentlength);
    clVelocityBuffers[0].write(velocity, 0, agentlength * sizeof(float));

    if (heading == nullptr)
        heading = floats.uniform(-M_PI, M_PI, agentlength);
    clHeadingBuffers[0].write(heading, 0, agentlength * sizeof(float));
}

void ClPhysarum::setup() {
    Scene::setup();

    mng.setSlProgName(selectDefaultSlProgram());
    reloadSl(true);

    dst = new float[w*h*4];

    /// allocate an OpenGL buffer with 4 float channels
    clOutFbo.allocate(CL_TILES_X*w, CL_TILES_Y*h, GL_RGBA32F_ARB);
    clOutFbo.begin();
    ofClear(0, 0, 0, 255);
    clOutFbo.end();
    glFinish();
    /// OpenCL uses the same buffer
    clOutImage.initFromTexture(clOutFbo.getTexture());

    clProgName = selectClProgram();
    pathClProg = clProgName;
    if(!std::filesystem::exists(pathClProg)) {
        fatalClProg = true;
        ofLog(OF_LOG_FATAL_ERROR) << "Cannot access '" << clProgName << "'";
    }
    else {
        reloadClProg(true);
    }

    clImageInput.initWithoutTexture(CL_TILES_X*w,
                                    CL_TILES_Y*h,
                                    1, // depth
                                    CL_RGBA, CL_FLOAT, CL_MEM_READ_WRITE);

    clPositionBuffers[0].initBuffer(agentlength * 2 * sizeof(float), CL_MEM_READ_WRITE);
    clPositionBuffers[1].initBuffer(agentlength * 2 * sizeof(float), CL_MEM_READ_WRITE);
    clVelocityBuffers[0].initBuffer(agentlength * sizeof(float), CL_MEM_READ_WRITE);
    clVelocityBuffers[1].initBuffer(agentlength * sizeof(float), CL_MEM_READ_WRITE);

    clHeadingBuffers[0].initBuffer(agentlength * sizeof(float), CL_MEM_READ_WRITE);
    clHeadingBuffers[1].initBuffer(agentlength * sizeof(float), CL_MEM_READ_WRITE);

    clTrailBuffers[0].initBuffer(CL_TILES_X*w * CL_TILES_Y*h * sizeof(cl_int), CL_MEM_READ_WRITE);
    clTrailBuffers[1].initBuffer(CL_TILES_X*w * CL_TILES_Y*h * sizeof(cl_int), CL_MEM_READ_WRITE);

    init_buffers();

    pingPongIndex = 0;
    run_ag_setup_kernel(ag_setup_kernel);
    run_trail_kernel(trail_setup_kernel);
    pingPongIndex = 1;
    openCL.finish();
}

void ClPhysarum::reloadSl(const bool force) {
    bool loaded = mng.reloadSlProg(force, sl_src);
    if (loaded || force) {
        mng.compile(slP0, "p0", "1.0", sl_src);
        mng.compile(slP1, "p1", "1.0", sl_src);
        mng.compile(slP2, "p2", "1.0", sl_src);
        mng.compile(slP3, "p3", "1.0", sl_src);
        mng.compile(slP4, "p4", "1.0", sl_src);
        mng.compile(slP5, "p5", "1.0", sl_src);
        mng.compile(slP6, "p6", "1.0", sl_src);
        mng.compile(slP7, "p7", "1.0", sl_src);
    }

}

bool ClPhysarum::reloadClProg(const bool force) {
    if(fatalClProg)
        return false;

    bool reloaded = ClScene::reloadClProg(force);
    if(reloaded && pcl != nullptr) {
        ag_setup_kernel = pcl->loadKernel("ag_setup");
        trail_setup_kernel = pcl->loadKernel("trail_setup");
        ag_move_kernel = pcl->loadKernel("ag_move");
        trail_disipate_kernel = pcl->loadKernel("trail_disipate");
        ag_draw_kernel = pcl->loadKernel("ag_draw");
        trail_setup_kernel = pcl->loadKernel("trail_setup");
    }
    return reloaded;
}

void ClPhysarum::run_ag_setup_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_ag_setup_kernel\n", pingPongIndex);
    int arg = 0;
    kernel->setArg(arg++, clImageInput);
    kernel->setArg(arg++, (cl_int)CL_TILES_X*w);
    kernel->setArg(arg++, (cl_int)CL_TILES_Y*h);

    kernel->setArg(arg++, clPositionBuffers[pingPongIndex]);
    kernel->setArg(arg++, clPositionBuffers[1 - pingPongIndex]);
    kernel->setArg(arg++, clVelocityBuffers[0]); // base velocity
    kernel->setArg(arg++, clVelocityBuffers[1]); // calculated velocity
    kernel->setArg(arg++, clHeadingBuffers[pingPongIndex]);
    kernel->setArg(arg++, clHeadingBuffers[1 - pingPongIndex]);

    kernel->setArg(arg++, (float)p0);
    kernel->setArg(arg++, (float)p1);
    kernel->setArg(arg++, (float)p2);
    kernel->setArg(arg++, (float)p3);
    kernel->setArg(arg++, (float)p4);
    kernel->setArg(arg++, (float)p5);
    kernel->setArg(arg++, (float)p6);
    kernel->setArg(arg++, (float)p7);
    kernel->run1D(agentlength);
}

void ClPhysarum::run_trail_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_trail_kernel\n", pingPongIndex);
    int arg = 0;
    kernel->setArg(arg++, clImageInput);
    kernel->setArg(arg++, (cl_int)CL_TILES_X*w);
    kernel->setArg(arg++, (cl_int)CL_TILES_Y*h);

    kernel->setArg(arg++, clTrailBuffers[pingPongIndex]);
    kernel->setArg(arg++, clTrailBuffers[1 - pingPongIndex]);

    kernel->setArg(arg++, (float)p0);
    kernel->setArg(arg++, (float)p1);
    kernel->setArg(arg++, (float)p2);
    kernel->setArg(arg++, (float)p3);
    kernel->setArg(arg++, (float)p4);
    kernel->setArg(arg++, (float)p5);
    kernel->setArg(arg++, (float)p6);
    kernel->setArg(arg++, (float)p7);
    kernel->run1D(CL_TILES_X*w * CL_TILES_Y*h);
}

void ClPhysarum::run_ag_move_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_ag_move_kernel\n", pingPongIndex);
    int arg = 0;
    kernel->setArg(arg++, clImageInput);
    kernel->setArg(arg++, (cl_int)CL_TILES_X*w);
    kernel->setArg(arg++, (cl_int)CL_TILES_Y*h);

    kernel->setArg(arg++, clPositionBuffers[pingPongIndex]);
    kernel->setArg(arg++, clPositionBuffers[1 - pingPongIndex]);
    kernel->setArg(arg++, clVelocityBuffers[0]); // base velocity
    kernel->setArg(arg++, clVelocityBuffers[1]); // calculated velocity
    kernel->setArg(arg++, clHeadingBuffers[pingPongIndex]);
    kernel->setArg(arg++, clHeadingBuffers[1 - pingPongIndex]);
    kernel->setArg(arg++, clTrailBuffers[pingPongIndex]);
    kernel->setArg(arg++, clTrailBuffers[1 - pingPongIndex]);

    kernel->setArg(arg++, (float)p0);
    kernel->setArg(arg++, (float)p1);
    kernel->setArg(arg++, (float)p2);
    kernel->setArg(arg++, (float)p3);
    kernel->setArg(arg++, (float)p4);
    kernel->setArg(arg++, (float)p5);
    kernel->setArg(arg++, (float)p6);
    kernel->setArg(arg++, (float)p7);
    kernel->run1D(agentlength);
}

void ClPhysarum::run_ag_draw_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_ag_draw_kernel\n", pingPongIndex);
    int arg = 0;
    kernel->setArg(arg++, clImageInput);
    kernel->setArg(arg++, (cl_int)CL_TILES_X*w);
    kernel->setArg(arg++, (cl_int)CL_TILES_Y*h);

    kernel->setArg(arg++, clTrailBuffers[pingPongIndex]);

    kernel->setArg(arg++, (float)p0);
    kernel->setArg(arg++, (float)p1);
    kernel->setArg(arg++, (float)p2);
    kernel->setArg(arg++, (float)p3);
    kernel->setArg(arg++, (float)p4);
    kernel->setArg(arg++, (float)p5);
    kernel->setArg(arg++, (float)p6);
    kernel->setArg(arg++, (float)p7);
    kernel->setArg(arg++, (float)dimmer);
    kernel->setArg(arg++, clOutImage);
    kernel->run2D(CL_TILES_X*w, CL_TILES_Y*h);
}

void ClPhysarum::update(){
    if(fatalClProg || clProgName.empty()) {
        return;
    }
    reloadSl(false);

    auto pixBuffer = Pool::stoneMoviePool.getAt(0);
    if(pixBuffer.getPixelFormat() == OF_PIXELS_RGB) {
        size_t w_min = std::min((size_t)w, (size_t)pixBuffer.getWidth());
        size_t h_min = std::min((size_t)h, (size_t)pixBuffer.getHeight());
        size_t origin[3] = { 0, 0, 0 };
        size_t region[3] = { w_min, h_min, 1 };
        auto src = pixBuffer.getData();
        size_t data_size = w_min * h_min;
        for(size_t dx=0; dx<data_size; ++dx) {
            dst[4*dx] = (float)src[3*dx] / 256.0f;
            dst[4*dx+1] = (float)src[3*dx+1] / 256.0f;
            dst[4*dx+2] = (float)src[3*dx+2] / 256.0f;
            dst[4*dx+3] = 1.0f;
        }
        clImageInput.write(dst, CL_TRUE, origin, region);
    }

    p0 = slP0.rpn.execute0();
    p1 = slP1.rpn.execute0();
    p2 = slP2.rpn.execute0();
    p3 = slP3.rpn.execute0();
    p4 = slP4.rpn.execute0();
    p5 = slP5.rpn.execute0();
    p6 = slP6.rpn.execute0();
    p7 = slP7.rpn.execute0();

    if(reloadClProg(false)) {
        clOutFbo.begin();
        ofClear(0, 0, 0, 255);
        clOutFbo.end();
        glFinish();

        init_buffers();

        pingPongIndex = 0;
        run_ag_setup_kernel(ag_setup_kernel);
        run_trail_kernel(trail_setup_kernel);
        pingPongIndex = 1;
    }
    else {
        glFinish();
    }

    run_ag_move_kernel(ag_move_kernel);
    pingPongIndex = 1 - pingPongIndex;
    run_trail_kernel(trail_disipate_kernel);
    pingPongIndex = 1 - pingPongIndex;
    run_ag_draw_kernel(ag_draw_kernel);
    pingPongIndex = 1 - pingPongIndex;
}

void ClPhysarum::drawLeftWall() {
    clOutFbo.draw(0, 0);
}

void ClPhysarum::drawRightWall() {
    ofPushMatrix();
    ofTranslate(-w, 0);
    clOutFbo.draw(0, 0);
    ofPopMatrix();
}

#endif // WITH_openCL
