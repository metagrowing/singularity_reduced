#include "operate.h"
#ifdef WITH_openCL
#include <random>

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "Configuration.h"
#include "ClTrailPulse.h"

ClTrailPulse::ClTrailPulse() : ClPhysarum() {
}

ClTrailPulse::~ClTrailPulse() {
}

const char * ClTrailPulse::sceneName() {
	return __FILE__;
}

void ClTrailPulse::setup() {
	ClPhysarum::setup();
}

const string& ClTrailPulse::selectClProgram() {
	static string prog_path = "bin/data/clphysarum/trail-pulse.cl";
	return prog_path;
}

const string& ClTrailPulse::selectDefaultSlProgram() {
	static string prog_path = "bin/data/clphysarum/trail-pulse.sl";
	return prog_path;
}
#endif // WITH_openCL
