#pragma once
#include "operate.h"
#ifdef WITH_openCL

#include "ofxOsc.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "calc/FloatArrays.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "ClScene.h"

class ClPhysarum : public ClScene {
	public:
		ClPhysarum();
		virtual ~ClPhysarum();
		virtual void setup();
	    /// For setting the file path to the simple Lisp-like expressions.
	    virtual const string& selectDefaultSlProgram() = 0;
		virtual void reloadSl(const bool force);
	    virtual bool reloadClProg(const bool force);
		virtual void update();
		virtual void drawLeftWall();
		virtual void drawRightWall();

    protected:
        msa::OpenCLImage clImageInput;

        msa::OpenCLBuffer clPositionBuffers[2]; // ping pong buffers with the same size
        msa::OpenCLBuffer clVelocityBuffers[2]; // ping pong buffers with the same size
        msa::OpenCLBuffer clHeadingBuffers[2]; // ping pong buffers with the same size
        msa::OpenCLBuffer clTrailBuffers[2]; // ping pong buffers with the same size

        msa::OpenCLKernelPtr ag_setup_kernel;
        msa::OpenCLKernelPtr trail_setup_kernel;
        msa::OpenCLKernelPtr ag_move_kernel;
        msa::OpenCLKernelPtr trail_disipate_kernel;
        msa::OpenCLKernelPtr ag_draw_kernel;

        FloatArrays floats;
//        const size_t agentlength = 50*1000;
        const size_t agentlength = 250*1000;
        int  pingPongIndex = 0;
        ofFbo clOutFbo;

        float* position = nullptr;
        float* velocity = nullptr;
        float* heading = nullptr;
    	float* dst = nullptr;

    	ScalarLispMng mng;
        std::vector<std::string> sl_src;
        ScalarLisp slP0;
        ScalarLisp slP1;
        ScalarLisp slP2;
        ScalarLisp slP3;
        ScalarLisp slP4;
        ScalarLisp slP5;
        ScalarLisp slP6;
        ScalarLisp slP7;
        double p0 = 0;
        double p1 = 0;
        double p2 = 0;
        double p3 = 0;
        double p4 = 0;
        double p5 = 0;
        double p6 = 0;
        double p7 = 0;

        void init_buffers();
        void run_ag_setup_kernel(msa::OpenCLKernelPtr kernel);
        void run_trail_kernel(msa::OpenCLKernelPtr kernel);
        void run_ag_move_kernel(msa::OpenCLKernelPtr kernel);
        void run_ag_clear_kernel(msa::OpenCLKernelPtr kernel);
        void run_ag_draw_kernel(msa::OpenCLKernelPtr kernel);
};
#endif // WITH_openCL
