#pragma once

#include "ofMain.h"
#include "Configuration.h"

#include "Word.h"
#include "sensor/minibee/MiniBeeData.h"
#include "Scene.h"

/// \class text animation
class Text : public Scene {

    public:
        Text();
        virtual ~Text();
        virtual const char * sceneName();
        virtual void setup();
        virtual void update();
        virtual void drawLeftWall();
        virtual void drawRightWall();
        vector<string> splitToWords(const string& string);
        vector<size_t> splitToInts(const string& string);

    private:
        void setupWords(vector<Word>& word);
        void fillImageVector(vector<Word>& word, const string& path);
        void updatePerLanguage(const string& lang);
        map<string, vector<Word>> word;
        size_t t0;
        ofBuffer buffer;
        // vector<string> freedomEnglish;
        map<string, vector<size_t>> timecodes;
        map<string, size_t> index;
        map<string, vector<string>> textVector;
        // vector<string> freedomEstonian;
        int wordToImage(const string& s, const string& lang);
        bool hasStarted = false;
        int lastX = 0;
};
