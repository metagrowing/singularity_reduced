#include "globalvars.h"
#include "Word.h"

//------------------------------------------------------------------
Word::Word() {
}

//------------------------------------------------------------------
void Word::loadImage(string f) {
    fileName = f;
    image.load(fileName);
}

//------------------------------------------------------------------
void Word::setup(int x=0, int y=0) {

    // 0 is center x. Check ofTranslate further down...
    // pos.x = ofRandom(-800, 800);
    // pos.y = ofRandom(400, 600);
    pos.x = x;
    pos.y = y;
    pos.z = 20000;

    vel.x = 0;
    vel.y = 0;
    vel.z = 20;

    scale = 400;

}

//------------------------------------------------------------------
void Word::update() {

    pos.z += vel.z;
    pos.x += vel.x;
    pos += vel;

    alpha = ofMap(abs(pos.z), 0, 5000, 255, 0, true);
}

//------------------------------------------------------------------
void Word::draw() {

    ofPushMatrix();
    ofTranslate(glm::vec3(ofGetWidth()/2, ofGetHeight() / 3, pos.z));
    // ofTranslate(glm::vec3(ofGetWidth() / 2, ofGetHeight() / 3, pos.z));
    image.setAnchorPoint(image.getWidth() / 2, image.getHeight() / 2);
    ofEnableAlphaBlending();
    ofSetColor(dimmer*255, 0, 0, alpha);
    image.draw(pos.x, pos.y, scale, scale);
    ofDisableAlphaBlending();
    ofPopMatrix();

}

const string Word::getName() {
    boost::filesystem::path p = fileName;
    return p.stem().string();
}
