#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "sensor/minibee/MiniBeeData.h"

class Word{

    public:
        Word();

        void loadImage(string fileName );

        void setup(int x, int y);
        void update();
        void draw();		

        glm::vec3 pos = { 0, 0, 0};
        glm::vec3 vel = { 0, 0, 0};


        float uniqueVal = 0;

        float scale = 0;

        ofImage image;
        int alpha = 0;

        const string getName();

        static size_t lastX;

    private:
        string fileName;
};
