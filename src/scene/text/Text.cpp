#include "stringhelper.h"
#include "globalvars.h"
#include "Pool.h"
#include "Text.h"

Text::Text() : Scene() {
}

Text::~Text() {
}

const char * Text::sceneName() {
    return __FILE__;
}

void Text::setup() {
    Scene::setup();

    // English
    vector<Word> eng;
    word.insert(make_pair("English", eng));

    this->fillImageVector(word["English"], "bin/data/freedomWords");
    buffer = ofBufferFromFile("freedom.txt");
    string txt = buffer.getText();
    vector<string> engWords;
    textVector.insert(make_pair("English", engWords));
    textVector["English"] = this->splitToWords(txt);
    this->setupWords(word["English"]);
    buffer = ofBufferFromFile("freedomEnglishTimecodes.txt");
    txt = buffer.getText();
    vector<size_t> engTimecodes;
    timecodes.insert(make_pair("English", engTimecodes));
    timecodes["English"] = this->splitToInts(txt);
    index.insert(make_pair("English", 0));

    // Estonian
    
    vector<Word> est;
    word.insert(make_pair("Estonian", est));

    this->fillImageVector(word["Estonian"], "bin/data/vabadusWords");
    buffer = ofBufferFromFile("vabadus.txt");
    txt = buffer.getText();
    vector<string> estWords;
    textVector.insert(make_pair("English", estWords));
    textVector["Estonian"] = this->splitToWords(txt);
    textVector["Estonian"] = this->splitToWords(txt);
    this->setupWords(word["Estonian"]);
    buffer = ofBufferFromFile("freedomEstonianTimecodes.txt");
    txt = buffer.getText();
    vector<size_t> estTimecodes;
    timecodes.insert(make_pair("English", estTimecodes));
    timecodes["Estonian"] = this->splitToInts(txt);
    index.insert(make_pair("Estonian", 0));
}

void Text::update() {
    this->updatePerLanguage("English");
    this->updatePerLanguage("Estonian");
}

void Text::drawLeftWall() {

    ofSetColor(0);
    ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
    ofSetColor(255, 45, 0, dimmer*256);
    for (size_t i = 0; i < word["English"].size(); i++) {
        word["English"][i].draw();
    };
    ofSetColor(255);
}

void Text::drawRightWall() {
    ofSetColor(0);
    ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
    ofSetColor(255, 45, 0, dimmer*256);
    for (size_t i = 0; i < word["Estonian"].size(); i++) {
        word["Estonian"][i].draw();
    };
    ofSetColor(255);
}

void Text::setupWords(vector<Word>& w) {
    int lastX = 0;
    int minX = -800;
    int maxX = 800;
    int lastY = 400;
    int minY = 400;
    int maxY = 600;
    array<int, 2> mul = {-1, 1};
    
    for (size_t i = 0; i < w.size(); i++) {
        int m = mul[(int)ofRandom(2)];
        int x = lastX + ofRandom(100, 300) * m;
        if (x < minX || x > maxX) x = 0; 

        m = mul[(int)ofRandom(2)];
        int y = lastY + ofRandom(200, 400) * m;
        if (y < minY || y > maxY) y = 500;

        w[i].setup(x, y);

        lastX = x;
        lastY = y;
    }
}

vector<string> Text::splitToWords(const string& s){
    // solution found here: https://stackoverflow.com/questions/8425214/splitting-string-into-a-vectorstring-of-words
    vector<string> ret;
    typedef string::size_type string_size;
    string_size i = 0;

    while (i != s.size()) {
        while (i != s.size() && isspace(s[i]))
            ++i;

        string_size j = i;
        while (j != s.size() && !isspace(s[j]))
            j++;

        if (i != j) {
            ret.push_back(s.substr(i, j - i));
            i = j;
        }
    }
    return ret;
}

vector<size_t> Text::splitToInts(const string& s){
    vector<string> asString;
    asString = this->splitToWords(s);
    vector<size_t> ret;
    for (size_t i = 0; i < asString.size(); i++) {
        int num = stoi(asString[i]);
        ret.push_back(num);
    }
    return ret;
}

int Text::wordToImage(const string& s, const string& lang){
    for(size_t i=0; i < word[lang].size(); i++){
        if(word[lang][i].getName() == s)
            return i;
    }
    return -1;
}

void Text::fillImageVector(vector<Word>& word, const string& p){

    string path = ofToString(p);
    ofDirectory dir(path);
    string ext = ofToString("png");
    dir.allowExt(ext);
    dir.listDir();

    int num = dir.size();

    word.assign(num, Word());

    for (int i = 0; i < num; i++) {
        string imagePath = dir.getPath(i);
        word[i].loadImage(imagePath);
    }
}

void Text::updatePerLanguage(const string& lang){

    if(!hasStarted) {
        t0 = ofGetElapsedTimeMillis();
        hasStarted = true;
    }

    if(timecodes[lang].size() == textVector[lang].size()){
        size_t elapsedTime = ofGetElapsedTimeMillis() - t0;
        if ( elapsedTime > timecodes[lang][index[lang]] && index[lang] < textVector[lang].size()) {
            size_t nextWord = this->wordToImage(textVector[lang][index[lang]], lang);
            word[lang][nextWord].pos.z = -5000;
            index[lang]++;
        }
    } else {
        ofLog(OF_LOG_WARNING) << ofToString("timecodes are not the same size as textVector ") << lang;
    }

    for (size_t i = 0; i < word[lang].size(); i++) {
        word[lang][i].update();
    }
}
