#include "ofMain.h"
#include "Pool.h"
#include "MovieImagePhysarum.h"

MovieImagePhysarum::MovieImagePhysarum() : Physarum() {
}

MovieImagePhysarum::~MovieImagePhysarum() {
}

const char * MovieImagePhysarum::sceneName() {
	return __FILE__;
}

const string& MovieImagePhysarum::selectDefaultSlProgram() {
	static string prog_path = "bin/data/physarum/movie_image_physarum.sl";
	return prog_path;
}

void MovieImagePhysarum::setup() {
    Physarum::particleMode = PHYS_NOISE;
    Physarum::threshold = 0.4;
    Physarum::speedLim = 2000;
	Physarum::setup();
}

void MovieImagePhysarum::patchPixelsLeftWall() {
	/// pixBuffer is the trail map with dimension windowWidth x 2*windowHeight
	const size_t dst_w = pixBuffer.getWidth();
	const size_t dst_h = pixBuffer.getHeight();
	const size_t bytes_per_pixel = pixBuffer.getNumChannels() * sizeof(unsigned char);

	/// copy a frame from a movie to the left half of the trail map.
	/// the movie is selected randomly from the stone movie pool
    ofPixels& pixels_copy = Pool::stoneMoviePool.getRandom();
	if(bytes_per_pixel != pixels_copy.getNumChannels() * sizeof(unsigned char))
		return;  // emergency exit

    unsigned char * src_ptr = pixels_copy.getData();
    const size_t src_w = pixels_copy.getWidth();
    const size_t src_h = pixels_copy.getHeight();

	const size_t min_w = std::min(src_w, dst_w);
	const size_t min_h = std::min(src_h, dst_h);
    const size_t stride = min_w * bytes_per_pixel;

	/// left wall trail map starts at 0
	unsigned char * dst_ptr = pixBuffer.getData();
    /// the dimension of the movie and the dimension of the window my differ.
    /// we must copy every single scan - line with memcpy.
    for(size_t line=0; line<min_h; ++line) {
        if(dst_ptr+stride < pixBuffer.getData()+dst_w*dst_h*bytes_per_pixel) {
        	memcpy(dst_ptr, src_ptr, stride);
        }
        else {
//        	printf("%p %p %x\n", dst_ptr, pixBuffer.getData(), dst_w*dst_h*bytes_per_pixel);
        	break;
        }
        dst_ptr += dst_w * bytes_per_pixel;
        src_ptr += src_w * bytes_per_pixel;
    }
}


void MovieImagePhysarum::patchPixelsRightWall() {
	/// pixBuffer is the trail map with dimension windowWidth x 2*windowHeight
	const size_t dst_w = pixBuffer.getWidth();
	const size_t dst_h = pixBuffer.getHeight();
	const size_t bytes_per_pixel = pixBuffer.getNumChannels() * sizeof(unsigned char);

	/// copy image data to the right wall of the trail map
	/// select one image randomly from the tree image pool
    ofPixels& pixels_copy = Pool::treeImagePool.getRandom();
	if(bytes_per_pixel != pixels_copy.getNumChannels() * sizeof(unsigned char))
		return;   // emergency exit

    unsigned char * src_ptr = pixels_copy.getData();
    const size_t src_w = pixels_copy.getWidth();
    const size_t src_h = pixels_copy.getHeight();

	const size_t min_w = std::min(src_w, dst_w);
	const size_t min_h = std::min(src_h, dst_h);
    const size_t stride = min_w * bytes_per_pixel;

	/// right wall trail map starts at windowWidth * 3; RGB texture -> 3 bytes per pixel
	unsigned char * dst_ptr = pixBuffer.getData() + Configuration::windowWidth * 3 * sizeof(unsigned char);
    /// the dimension of the movie and the dimension of the window my differ.
    /// we must copy every single scan - line with memcpy.
    for(size_t line=0; line<min_h; ++line) {
        if(dst_ptr+stride < pixBuffer.getData()+dst_w*dst_h*bytes_per_pixel) {
        	memcpy(dst_ptr, src_ptr, stride);
        }
        else {
//        	printf("%p %p %x\n", dst_ptr, pixBuffer.getData(), dst_w*dst_h*bytes_per_pixel);
        	break;
        }
        dst_ptr += dst_w * bytes_per_pixel;
        src_ptr += src_w * bytes_per_pixel;
    }
}
