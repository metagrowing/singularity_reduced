#include <BackWallMoviePhysarum.h>

#include "ofMain.h"
#include "Pool.h"

BackWallMoviePhysarum::BackWallMoviePhysarum() : Physarum() {
}

BackWallMoviePhysarum::~BackWallMoviePhysarum() {
}

const char * BackWallMoviePhysarum::sceneName() {
	return __FILE__;
}

const string& BackWallMoviePhysarum::selectDefaultSlProgram() {
	static string prog_path = "bin/data/physarum/back_wall_movie_physarum.sl";
	return prog_path;
}

void BackWallMoviePhysarum::setup() {
    Physarum::particleMode = PHYS_HORIZON;
    Physarum::startY = 0.65;
    Physarum::spread = 5;
    Physarum::wrap = false;
    Physarum::threshold = 0.6;
    // Physarum::threshold = 0.4;
    Physarum::speedLim = 2000;
    hasBlackHole = true;

	Physarum::setup();

//	r = 10;
//	g = 64;
//	b = 196;

}

void BackWallMoviePhysarum::patchPixelsLeftWall() {
	/// pixBuffer is the trail map with dimension windowWidth x 2*windowHeight
	const size_t dst_w = pixBuffer.getWidth(); const size_t dst_h = pixBuffer.getHeight(); const size_t bytes_per_pixel = pixBuffer.getNumChannels() * sizeof(unsigned char); /// copy a frame from movie with index 0 to the lower half of the trail map
    ofPixels& pixels_copy = Pool::iceScapeMoviePool.getAt(0);
    // ofPixels& pixels_copy = Pool::hangingMoviePool.getRandom();
	if(bytes_per_pixel != pixels_copy.getNumChannels() * sizeof(unsigned char))
		return;  // emergency exit

    unsigned char * src_ptr = pixels_copy.getData();
    const size_t src_w = pixels_copy.getWidth();
    const size_t src_h = pixels_copy.getHeight();

	const size_t min_w = std::min(src_w, dst_w);
	const size_t min_h = std::min(src_h, dst_h);
    const size_t stride = min_w * bytes_per_pixel;

	unsigned char * dst_ptr = pixBuffer.getData();
    /// the dimension of the movie and the dimension of the window my differ.
    /// we can only copy scan - lines with memcpy.
    for(size_t line=0; line<min_h; ++line) {
        if(dst_ptr+stride < pixBuffer.getData()+dst_w*dst_h*bytes_per_pixel) {
        	memcpy(dst_ptr, src_ptr, stride);
        }
        else {
//        	printf("%p %p %x\n", dst_ptr, pixBuffer.getData(), dst_w*dst_h*bytes_per_pixel);
        	break;
        }
        dst_ptr += dst_w * bytes_per_pixel;
        src_ptr += src_w * bytes_per_pixel;
    }
}


void BackWallMoviePhysarum::patchPixelsRightWall() {
	// /// pixBuffer is the trail map with dimension 2*windowWidth x windowHeight
	// const size_t bytes_per_pixel = pixBuffer.getNumChannels() * sizeof(unsigned char);

	// /// invert the lower half of the tail map
    // const size_t src_w = pixBuffer.getWidth();
    // const size_t src_h = pixBuffer.getHeight();
    // unsigned char * src_ptr = pixBuffer.getData() + (src_h / 2) * src_w * 3 * sizeof(unsigned char);

    // // only work on the lower half of the trail map -> (src_h / 2)
    // const size_t size = src_w * (src_h / 2) * bytes_per_pixel;
    // for(size_t px=0; px<size; ++px) {
    	// *src_ptr = std::max(100, 255 - *src_ptr);
    	// ++src_ptr;
    // }

	const size_t dst_w = pixBuffer.getWidth();
	const size_t dst_h = pixBuffer.getHeight();
	const size_t bytes_per_pixel = pixBuffer.getNumChannels() * sizeof(unsigned char);

    ofPixels& pixels_copy = Pool::iceScapeMoviePool.getAt(0);
    // ofPixels& pixels_copy = Pool::hangingMoviePool.getRandom();
	if(bytes_per_pixel != pixels_copy.getNumChannels() * sizeof(unsigned char))
		return;   // emergency exit

    unsigned char * src_ptr = pixels_copy.getData();
    const size_t src_w = pixels_copy.getWidth();
    const size_t src_h = pixels_copy.getHeight();

	const size_t min_w = std::min(src_w, dst_w);
	const size_t min_h = std::min(src_h, dst_h);
    const size_t stride = min_w * bytes_per_pixel;

	/// right wall trail map starts at windowWidth * 3; RGB texture -> 3 bytes per pixel
	unsigned char * dst_ptr = pixBuffer.getData() + Configuration::windowWidth * 3 * sizeof(unsigned char);
    /// the dimension of the movie and the dimension of the window my differ.
    /// we must copy every single scan - line with memcpy.
    for(size_t line=0; line<min_h; ++line) {
        if(dst_ptr+stride < pixBuffer.getData()+dst_w*dst_h*bytes_per_pixel) {
        	memcpy(dst_ptr, src_ptr, stride);
        }
        else {
//        	printf("%p %p %x\n", dst_ptr, pixBuffer.getData(), dst_w*dst_h*bytes_per_pixel);
        	break;
        }
        dst_ptr += dst_w * bytes_per_pixel;
        src_ptr += src_w * bytes_per_pixel;
    }
}
