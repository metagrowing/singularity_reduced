#include "Pool.h"
#include "globalvars.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "calc/ScalarRPN.h"
#include "Physarum.h"

Physarum::Physarum() : Scene() {
}

Physarum::~Physarum() {
	trailmap.clear();
	pixBuffer.clear();
	texBuffer.clear();
}

void Physarum::setup() {
	Scene::setup();

	mng.setSlProgName(selectDefaultSlProgram());
	reloadSl(true);

	for (int i = 0; i < numParticles; i++) {
		p[i].setup(particleMode, w * PHYSARUM_TILES_X, h * PHYSARUM_TILES_Y, startX, startY, spread);
		p[i].wrapMode = wrap;
	}

	trailmap.allocate (w * PHYSARUM_TILES_X, h * PHYSARUM_TILES_Y, GL_RGB);
	pixBuffer.allocate(w * PHYSARUM_TILES_X, h * PHYSARUM_TILES_Y, OF_IMAGE_COLOR);
	texBuffer.allocate(w * PHYSARUM_TILES_X, h * PHYSARUM_TILES_Y, GL_RGB);

	shader.load("shader/physarum");
	// shader.load("shader/physarumBlackHole");

	gui.setup();
	gui.add(numDirs.setup("Number of Directions", 3, 1, 64));
	gui.add(maxAngle.setup("Max Angle", 360.0, 0.0, 360.0));

	// sphere.setRadius( w );
	sphere.setRadius(w / 2.0);
	sphereX = 0.0;
	sphereY = 0.0;
	sphereZ = 0.0;
	sphereXSpeed = 0.0;
	sphereYSpeed = 0.5;
	sphereZSpeed = 0.0;

	t0 = t1 = t2 = t3 = ofGetElapsedTimeMillis();
}

void Physarum::setSlProgName(string pn) {
	mng.setSlProgName(pn);
}

void Physarum::updateLeftWall() {
	t3 = ofGetElapsedTimeMillis();
	if (deltaLeft > threshold && t3 - t2 > speedLim) {
		patchPixelsLeftWall();
		t2 = t3;
	}

	if (slConnectedLeft.rpn.execute0() < 0.0) {
		maxSpeed += ofSignedNoise(ofGetElapsedTimef());
		maxSpeed = ofClamp(maxSpeed, 0.0, 10.0);
		senseDist += ofSignedNoise(ofGetElapsedTimef() * 0.31) * 2;
		senseDist = ofClamp(senseDist, 0.0, 200.0);
	}
}

void Physarum::updateRightWall() {
	t1 = ofGetElapsedTimeMillis();
	if (deltaRight > threshold && t1 - t0 > speedLim) {
		patchPixelsRightWall();
		t0 = t1;
	}

	if (slConnectedRight.rpn.execute0() < 0.0) {
		maxSpeed += ofSignedNoise(ofGetElapsedTimef());
		maxSpeed = ofClamp(maxSpeed, 0.0, 10.0);
		senseDist += ofSignedNoise(ofGetElapsedTimef() * 0.3) * 2;
		senseDist = ofClamp(senseDist, 0.0, 200.0);
	}
}

void Physarum::reloadSl(const bool force) {
	bool loaded = mng.reloadSlProg(force, sl_src);
	if (loaded || force) {
		mng.compile(slHue, "hue", "0", sl_src);
		mng.compile(slSaturation, "sat", "0", sl_src);
		mng.compile(slValue, "val", "1.0", sl_src);
		mng.compile(slDepositAmt, "deposit", "170", sl_src);
		mng.compile(slDecayRate, "decay", "0.9", sl_src);
		mng.compile(slMaxSpeed, "maxSpeed", "4.0", sl_src);
		mng.compile(slDeltaLeft, "deltaLeft", "0.0", sl_src);
		mng.compile(slDeltaRight, "deltaRight", "0.0", sl_src);
		mng.compile(slSenseDist, "senseDist", "5.0", sl_src);
		mng.compile(slConnectedLeft, "connectedLeft", "-1", sl_src);
		mng.compile(slConnectedRight, "connectedRight", "-1", sl_src);
		mng.compile(slStrength, "strength", "2.0", sl_src);
		mng.compile(slScale, "scale", "1", sl_src);
		mng.compile(slOff, "off", "0.05", sl_src);
		mng.compile(slCenterX, "cx", "0.5", sl_src);
		mng.compile(slCenterY, "cy", "0.5", sl_src);
	}
}

void Physarum::update() {
	reloadSl(false);
	hue = slHue.rpn.execute0();
	sat = slSaturation.rpn.execute0();
	val = slValue.rpn.execute0();
	depositAmt = slDepositAmt.rpn.execute0();
	decayRate = slDecayRate.rpn.execute0();
    decayRate = decayRate + 0.99 * (prevDecayRate - decayRate); //lowpassfilter change
    prevDecayRate = decayRate;
    maxSpeed = slMaxSpeed.rpn.execute0();
    deltaLeft = slDeltaLeft.rpn.execute0();
    deltaRight = slDeltaRight.rpn.execute0();
    senseDist = slSenseDist.rpn.execute0();
	strength = slStrength.rpn.execute0();
	scale = slScale.rpn.execute0();
	off = slOff.rpn.execute0();
	centerX = slCenterX.rpn.execute0();
	centerY = slCenterY.rpn.execute0();

    if (!hasStarted) {
        startTime = ofGetElapsedTimeMillis();
        hasStarted = true;
    }
    if (ofGetElapsedTimeMillis() - startTime > 45000) Physarum::hasBlackHole = false;

	updateLeftWall();
	updateRightWall();

	switch(pixBuffer.getPixelFormat()) {
	case OF_PIXELS_UNKNOWN:
	case OF_PIXELS_NATIVE:
		break;
	case OF_PIXELS_RGB:
	case OF_PIXELS_RGBA: {
		//TODO test with RGBA images.
		unsigned char * pixel_ptr = pixBuffer.getData();
		size_t pix_w = pixBuffer.getWidth();
		size_t pix_h = pixBuffer.getHeight();
		size_t pix_channels = pixBuffer.getNumChannels();
		for (int i = 0; i < numParticles; i++) {
			p[i].setMaxSpeed(maxSpeed);
			p[i].setMaxAngle(maxAngle);
			p[i].setNumDirs(numDirs);
			p[i].setSenseDist(senseDist);
			p[i].setDepositAmt(depositAmt);
			p[i].sense(pixel_ptr, pix_w, pix_h, pix_channels);
			p[i].move();
			p[i].deposit(pixel_ptr, pix_w, pix_h, pix_channels);
		}
		break;
	}
	default: {
		for (int i = 0; i < numParticles; i++) {
			p[i].setMaxSpeed(maxSpeed);
			p[i].setMaxAngle(maxAngle);
			p[i].setNumDirs(numDirs);
			p[i].setSenseDist(senseDist);
			p[i].setDepositAmt(depositAmt);
			p[i].sense(pixBuffer);
			p[i].move();
			p[i].deposit(pixBuffer);
		}
	}
	}

	texBuffer.loadData(pixBuffer);

	trailmap.begin();
	shader.begin();
	shader.setUniformTexture("tex", texBuffer, 0);
	shader.setUniform1f("dR", decayRate);
	shader.setUniform1i("w", w);
	shader.setUniform1i("h", h);
	shader.setUniform1f("strength", std::max(0.0f, strength));
	shader.setUniform1f("scale", scale);
	shader.setUniform1f("off", off);
	// multiply centerX by 2: the trail map is twice the size of one screen.
	shader.setUniform2f("center", 2*centerX, centerY);
	texBuffer.draw(0, 0);
    if(hasBlackHole) drawBlackHole(sizeBlackHole);
	shader.end();
	trailmap.end();
	trailmap.readToPixels(pixBuffer);
}

void Physarum::drawBlackHole(int width) {
    ofPushMatrix();
    ofTranslate(w, h*0.65);
    ofDrawCircle(0, 0, width);
    ofPopMatrix();
}

void Physarum::drawPolyline() {

    ofSetColor(dimmer*255);
    float angle = 0;
    while (angle < TWO_PI ) {
        line.curveTo(100*cos(angle), 0, 100*sin(angle));
        line.curveTo(300*cos(angle), 300, 300*sin(angle));
        angle += TWO_PI / 30;
    };
    line.close();
    line.draw();
    ofSetColor(0, 0, 0);
}

// There is only one texture twice the size of a screen window, monitor or beamer.
// So i need to display the left part of texture in one draw call
// and the right part in the next draw call.
// This is simple on the left projection. Because the too long scanlines are clipped.
void Physarum::drawLeftWall() {
	ofSetColor(ofColor::fromHsb(hue, sat, dimmer*val));

	trailmap.draw(0, 0);

	if(showGui) {
		gui.draw();
	}
}

// Using a combination of coordinate translation and implicit clipping.
// Move in negative x direction to cut away the left part of the 3840 texture.
void Physarum::drawRightWall() {
	ofSetColor(ofColor::fromHsb(hue, sat, dimmer*val));

	ofPushMatrix();
	ofTranslate(-w, 0);
	trailmap.draw(0, 0);
	ofPopMatrix();
}


void Physarum::setImage(string p) {
	ofImage img;
	img.load(p);
	img.setImageType(OF_IMAGE_GRAYSCALE);
	img.resize(ofGetWidth(), ofGetHeight());
	pixBuffer = img;
}

void Physarum::resetParticles(int mode = PHYS_CIRCLE) {
	for (int i = 0; i < numParticles; i++) {
		p[i].setup(mode);
	}
}
void Physarum::setParticleRandomHeight(float h = 0) {
	for (int i = 0; i < numParticles; i++) {
		p[i].randomHeight = h;
	};
	resetParticles(PHYS_RANDOMHORIZON);
}

void Physarum::setParticleRandomWidth(float w = 0) {
	for (int i = 0; i < numParticles; i++) {
		p[i].randomWidth = w;
	};
	resetParticles(PHYS_RANDOMVERTICAL);
}

void Physarum::keyPressed(int key) {
	Scene::keyPressed(key);

	switch (key) {
	case 'a':
		resetParticles(PHYS_CIRCLE);
		break;
	case 's':
		resetParticles(PHYS_HORIZON);
		break;
	// case 'd':
	// 	resetParticles(PHYS_BOTTOMLINE);
	// 	break;
	// case 'f':
	// 	resetParticles(PHYS_TOPLINE);
	// 	break;
	case 'g':
		resetParticles(PHYS_NOISE);
		break;
	case 'z':
		setParticleRandomHeight(ofGetHeight() - 10);
		break;
	case 'x':
		setParticleRandomWidth(ofGetWidth() - 10);
		break;
	}
}
