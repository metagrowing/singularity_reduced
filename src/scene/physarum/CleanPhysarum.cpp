#include <CleanPhysarum.h>

#include "ofMain.h"
#include "Pool.h"

CleanPhysarum::CleanPhysarum() : Physarum() {
}

CleanPhysarum::~CleanPhysarum() {
}

const char * CleanPhysarum::sceneName() {
	return __FILE__;
}

const string& CleanPhysarum::selectDefaultSlProgram() {
	static string prog_path = "bin/data/physarum/clean_physarum.sl";
	return prog_path;
}

void CleanPhysarum::setup() {
    Physarum::startX = 0.3;
    Physarum::startY = 0.65;
	Physarum::setup();

//	r = 10;
//	g = 64;
//	b = 196;

}


void CleanPhysarum::patchPixelsLeftWall() {
	/// pixBuffer is the trail map with dimension windowWidth x 2*windowHeight
	const size_t dst_w = pixBuffer.getWidth(); const size_t dst_h = pixBuffer.getHeight(); const size_t bytes_per_pixel = pixBuffer.getNumChannels() * sizeof(unsigned char); /// copy a frame from movie with index 0 to the lower half of the trail map
    ofPixels& pixels_copy = Pool::hangingMoviePool.getRandom();
	if(bytes_per_pixel != pixels_copy.getNumChannels() * sizeof(unsigned char))
		return;  // emergency exit

    unsigned char * src_ptr = pixels_copy.getData();
    const size_t src_w = pixels_copy.getWidth();
    const size_t src_h = pixels_copy.getHeight();

	const size_t min_w = std::min(src_w, dst_w);
	const size_t min_h = std::min(src_h, dst_h);
    const size_t stride = min_w * bytes_per_pixel;

	unsigned char * dst_ptr = pixBuffer.getData();
    /// the dimension of the movie and the dimension of the window my differ.
    /// we can only copy scan - lines with memcpy.
    for(size_t line=0; line<min_h; ++line) {
        if(dst_ptr+stride < pixBuffer.getData()+dst_w*dst_h*bytes_per_pixel) {
        	memcpy(dst_ptr, src_ptr, stride);
        }
        else {
//        	printf("%p %p %x\n", dst_ptr, pixBuffer.getData(), dst_w*dst_h*bytes_per_pixel);
        	break;
        }
        dst_ptr += dst_w * bytes_per_pixel;
        src_ptr += src_w * bytes_per_pixel;
    }
}


void CleanPhysarum::patchPixelsRightWall() {

	const size_t dst_w = pixBuffer.getWidth();
	const size_t dst_h = pixBuffer.getHeight();
	const size_t bytes_per_pixel = pixBuffer.getNumChannels() * sizeof(unsigned char);

    ofPixels& pixels_copy = Pool::hangingMoviePool.getRandom();
	if(bytes_per_pixel != pixels_copy.getNumChannels() * sizeof(unsigned char))
		return;   // emergency exit

    unsigned char * src_ptr = pixels_copy.getData();
    const size_t src_w = pixels_copy.getWidth();
    const size_t src_h = pixels_copy.getHeight();

	const size_t min_w = std::min(src_w, dst_w);
	const size_t min_h = std::min(src_h, dst_h);
    const size_t stride = min_w * bytes_per_pixel;

	/// right wall trail map starts at windowWidth * 3; RGB texture -> 3 bytes per pixel
	unsigned char * dst_ptr = pixBuffer.getData() + Configuration::windowWidth * 3 * sizeof(unsigned char);
    /// the dimension of the movie and the dimension of the window my differ.
    /// we must copy every single scan - line with memcpy.
    for(size_t line=0; line<min_h; ++line) {
        if(dst_ptr+stride < pixBuffer.getData()+dst_w*dst_h*bytes_per_pixel) {
        	memcpy(dst_ptr, src_ptr, stride);
        }
        else {
//        	printf("%p %p %x\n", dst_ptr, pixBuffer.getData(), dst_w*dst_h*bytes_per_pixel);
        	break;
        }
        dst_ptr += dst_w * bytes_per_pixel;
        src_ptr += src_w * bytes_per_pixel;
    }
}
