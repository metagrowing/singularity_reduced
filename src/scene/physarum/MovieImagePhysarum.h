#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "Physarum.h"

/// \class base class for all media streams
/// \brief provides empty default functions for derivative subclasses
class MovieImagePhysarum : public Physarum {
public:
	MovieImagePhysarum();
	virtual ~MovieImagePhysarum();
	virtual const char * sceneName();
	virtual void setup();
	virtual const string& selectDefaultSlProgram();

	virtual void patchPixelsLeftWall();
	virtual void patchPixelsRightWall();
};
