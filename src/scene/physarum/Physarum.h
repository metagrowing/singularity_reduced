// Based on Samuel Cho's (https://github.com/chosamuel) implementation of 
// slime mold growth patterns. 
// Original repo here: https://github.com/chosamuel/openframeworks-physarum

#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxSlider.h"

#include "Configuration.h"
#include "PhysarumParticle.h"
#include "sensor/minibee/MiniBeeData.h"
#include "ofxThreadedImageLoader.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "Scene.h"

#define PHYSARUM_TILES_X 2
#define PHYSARUM_TILES_Y 1

/// \class slime mold growth pattern
class Physarum : public Scene {
public:
	Physarum();
	virtual ~Physarum();
	virtual void setup();
	virtual const string& selectDefaultSlProgram() = 0;
	virtual void setSlProgName(string pn);
	virtual void update();
	virtual void updateLeftWall();
	virtual void updateRightWall();
	virtual void drawPolyline();
	virtual void drawBlackHole(int radius);
	virtual void drawLeftWall();
	virtual void drawRightWall();
	virtual void keyPressed(int key);

	void resetParticles(int mode);
	void setParticleRandomHeight(float h);
	void setParticleRandomWidth(float w);
	void setImage(string p);

	ofShader shader;
	ofFbo trailmap;
	ofPixels pixBuffer;
	ofTexture texBuffer;

	bool useImage = true;
	bool useSphere = false;
	int wrap = PHYS_WRAP_ON;

	float threshold = 0.2;
	int speedLim = 1000;

	// static const int numParticles = 30000;
	static const int numParticles = 60000;
	PhysarumParticle p[numParticles];
	int particleMode = PHYS_CIRCLE;

	ofxPanel gui;
	ofxFloatSlider maxAngle;
	ofxIntSlider numDirs;

    

protected:
	virtual void patchPixelsLeftWall() = 0;
	virtual void patchPixelsRightWall() = 0;

	ScalarLispMng mng;
    std::vector<std::string> sl_src;
    ScalarLisp slHue;        // for HSV color model
    ScalarLisp slSaturation; // for HSV color model
    ScalarLisp slValue;      // for HSV color model
    ScalarLisp slDepositAmt; // for physarum
    ScalarLisp slDecayRate;  // for physarum
    ScalarLisp slMaxSpeed;  // for physarum
    ScalarLisp slDeltaLeft; // for physarum
    ScalarLisp slDeltaRight; // for physarum
    ScalarLisp slSenseDist; // for physarum
    ScalarLisp slConnectedLeft; // is miniBee connected?
    ScalarLisp slConnectedRight; // is miniBee connected?
    ScalarLisp slStrength;
    ScalarLisp slScale;
    ScalarLisp slOff;
    ScalarLisp slCenterX;
    ScalarLisp slCenterY;
    float hue = 0;
    float sat = 0;
    float val = 0;
    float depositAmt;
    float decayRate = 0.9;
    float prevDecayRate = 0.9;
	float maxSpeed = 4.0;
    float deltaLeft = 0.0;
    float deltaRight = 0.0;
    float senseDist = 5.0;
    float strength = 2.0;
    float scale = 1.0;
    float off = 0.05;
    float centerX = 0.5;
    float centerY = 0.5;
    virtual void reloadSl(const bool force);

    // used to set up particle position as ratio of screen width/height
    float startX = 0.5;
    float startY = 0.5;
    int spread = 20;

    int sizeBlackHole = 250;
    bool hasBlackHole = false;

private:
	ofSpherePrimitive sphere;
	float sphereX = 0;
	float sphereY = 0;
	float sphereZ = 0;
	float sphereXSpeed = 0;
	float sphereYSpeed = 0;
	float sphereZSpeed = 0;

	int t0 = 0;
	int t1 = 0;
	int t2 = 0;
	int t3 = 0;

    int startTime = 0;
    bool hasStarted = false;

    ofPolyline line;
};
