#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "Physarum.h"

/// \class base class for all media streams
/// \brief provides empty default functions for derivative subclasses
class BackWallMoviePhysarum : public Physarum {
public:
	BackWallMoviePhysarum();
	virtual ~BackWallMoviePhysarum();
	virtual const char * sceneName();
	virtual void setup();
	virtual const string& selectDefaultSlProgram();

	virtual void patchPixelsLeftWall();
	virtual void patchPixelsRightWall();
};
