#pragma once
#include "ofMain.h"
#include "Configuration.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "Scene.h"

typedef struct FragManagement {
    string fragProgName;
    long fragProgModTime = 0;
    bool fatalFragProg = false;
    std::filesystem::path pathFragProg;
    ofShader shader;
} frag_management_t;


/// \class base class for all scene classes doing their relevant work in a fragment shader.
/// \brief provides the call to then GLSL just in time compiler.
class FragmentScene: public Scene {
public:
    FragmentScene();
    virtual ~FragmentScene();

    virtual void setup();

    /// For setting the file path to the simple Lisp-like expressions.
    virtual const string& selectDefaultSlProgram() = 0;
	virtual void setSlProgName(string pn);

    virtual void setup_fm(frag_management_t& fm);

    /// For setting the file path to the GLSLfragment shader program.
    virtual const string& selectLeftWallFragProgram() = 0;

    /// For setting the file path to the GLSLfragment shader program.
    virtual const string& selectRightFragProgram() = 0;

    /// Load and compile the OpenCL program.
    /// FragmentScene will check the last write time of the fragment shader program files.
    /// If you save a file from an editor, the fragment shader program is reloaded.
    /// No need to stop singularity, after making small changes in the fragment shader part.
    virtual bool reloadFragProg(frag_management_t& fm, const bool force);

    virtual void update();

    /// Some uniform that can be used in fragment shader programs.
    /// No problem if a specific fragment shader did not read all these uniforms
    virtual void sendCommonUniforms(frag_management_t& fm);

    /// Draws on the left wall.
    /// You must overwrite this in a derived class.
    virtual void drawLeftWall() = 0;

    /// Draws on the right wall.
    /// You must overwrite this in a derived class.
    virtual void drawRightWall() = 0;

    /// expressions calculated per frame
    /// and send as uniforms to the fragment shader
	ScalarLispMng mng;
    std::vector<std::string> sl_src;
    ScalarLisp slP0;
    ScalarLisp slP1;
    ScalarLisp slP2;
    ScalarLisp slP3;
    ScalarLisp slP4;
    ScalarLisp slP5;
    ScalarLisp slP6;
    ScalarLisp slP7;
    float p0 = 0;
    float p1 = 0;
    float p2 = 0;
    float p3 = 0;
    float p4 = 0;
    float p5 = 0;
    float p6 = 0;
    float p7 = 0;
    float prevP0 = 0;
    float prevP1 = 0;
    float prevP2 = 0;
    float prevP3 = 0;
    float prevP4 = 0;
    float prevP5 = 0;
    float prevP6 = 0;
    float prevP7 = 0;
    virtual void reloadSl(const bool force);

    float hue = 0;
    frag_management_t fm_leftkwall;
    frag_management_t fm_rightwall;
};
