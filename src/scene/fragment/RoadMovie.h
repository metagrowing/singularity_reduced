#pragma once
#include "ofMain.h"
#include "Configuration.h"
#include "FragmentScene.h"

/// \class Mixing two viedeos in the fragment shader.
class RoadMovie: public FragmentScene {
public:
    RoadMovie();
    virtual ~RoadMovie();

    virtual const char * sceneName();

    /// For setting the file path to the simple Lisp-like expressions.
    virtual void setup();

	virtual const string& selectDefaultSlProgram();
//	virtual void setSlProgName(string pn);

    /// For setting the file path to the GLSLfragment shader program.
    virtual const string& selectLeftWallFragProgram();

    /// For setting the file path to the GLSLfragment shader program.
    virtual const string& selectRightFragProgram();

    /// Draws on the felt wall.
    virtual void drawLeftWall();

    /// Draws on the right wall.
    virtual void drawRightWall();
};
