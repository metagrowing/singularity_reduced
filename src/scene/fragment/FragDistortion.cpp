#include "ofMain.h"
#include "Configuration.h"
#include "ofApp.h"
#include "Pool.h"
#include "FragDistortion.h"

/// same media streams on both walls.
/// but different fragment shader programs

FragDistortion::FragDistortion() : FragmentScene() {
}

FragDistortion::~FragDistortion() {
}

const char * FragDistortion::sceneName() {
	return __FILE__;
}

void FragDistortion::setup() {
	FragmentScene::setup();
}

const string& FragDistortion::selectDefaultSlProgram() {
	static string prog_path = "bin/data/frag_distortion/frag_distortion.sl";
	return prog_path;
}

void FragDistortion::setSlProgName(string pn) {
	mng.setSlProgName(pn);
}

const string& FragDistortion::selectLeftWallFragProgram() {
	static string prog_path = "bin/data/frag_distortion/dist_left.frag";
	return prog_path;
}

const string& FragDistortion::selectRightFragProgram() {
	static string prog_path = "bin/data/frag_distortion/dist_right.frag";
	return prog_path;
}

void FragDistortion::drawLeftWall() {
    if(fm_leftkwall.fatalFragProg || fm_leftkwall.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
    	fm_leftkwall.shader.begin();
    	sendCommonUniforms(fm_leftkwall);
    	// attention numbering: tex0 -> texture unit 0
    	//                      tex1 -> texture unit 1
    	fm_leftkwall.shader.setUniformTexture("tex0", Pool::stoneMoviePool.getTexturAt(0), 0);
    	fm_leftkwall.shader.setUniformTexture("tex1", Pool::stoneMoviePool.getTexturAt(1), 1);
        ofDrawRectangle(0, 0, w, h);
        fm_leftkwall.shader.end();
    }
}

void FragDistortion::drawRightWall() {
    if(fm_rightwall.fatalFragProg || fm_rightwall.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
    	fm_rightwall.shader.begin();
    	sendCommonUniforms(fm_rightwall);
    	// attention numbering: tex0 -> texture unit 0
    	//                      tex1 -> texture unit 1
    	fm_rightwall.shader.setUniformTexture("tex0", Pool::stoneMoviePool.getTexturAt(0), 0);
    	fm_rightwall.shader.setUniformTexture("tex1", Pool::stoneMoviePool.getTexturAt(1), 1);
        ofDrawRectangle(0, 0, w, h);
        fm_rightwall.shader.end();
    }
}
