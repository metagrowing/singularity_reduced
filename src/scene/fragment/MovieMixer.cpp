#include "ofMain.h"
#include "Configuration.h"
#include "ofApp.h"
#include "Pool.h"
#include "MovieMixer.h"

/// same media streams on both walls.
/// but different fragment shader programs

MovieMixer::MovieMixer() : FragmentScene() {
}

MovieMixer::~MovieMixer() {
}

const char * MovieMixer::sceneName() {
	return __FILE__;
}

void MovieMixer::setup() {
	FragmentScene::setup();
}

const string& MovieMixer::selectDefaultSlProgram() {
	static string prog_path = "bin/data/movie_mixer/noise_1d.sl";
	return prog_path;
}

const string& MovieMixer::selectLeftWallFragProgram() {
	static string prog_path = "bin/data/movie_mixer/mix2channels.frag";
	return prog_path;
}

const string& MovieMixer::selectRightFragProgram() {
	static string prog_path = "bin/data/movie_mixer/stripes.frag";
	return prog_path;
}

//void MovieMixer::setSlProgName(string pn) {
//	mng.setSlProgName(pn);
//}

void MovieMixer::drawLeftWall() {
    if(fm_leftkwall.fatalFragProg || fm_leftkwall.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
    	fm_leftkwall.shader.begin();
    	sendCommonUniforms(fm_leftkwall);
    	// attention numbering: tex0 -> texture unit 0
    	//                      tex1 -> texture unit 1
    	fm_leftkwall.shader.setUniformTexture("tex0", Pool::stoneMoviePool.getTexturAt(0), 0);
    	fm_leftkwall.shader.setUniformTexture("tex1", Pool::stoneMoviePool.getTexturAt(1), 1);
        ofDrawRectangle(0, 0, w, h);
        fm_leftkwall.shader.end();
    }
}

void MovieMixer::drawRightWall() {
    if(fm_rightwall.fatalFragProg || fm_rightwall.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
    	fm_rightwall.shader.begin();
    	sendCommonUniforms(fm_rightwall);
    	// attention numbering: tex0 -> texture unit 0
    	//                      tex1 -> texture unit 1
    	fm_rightwall.shader.setUniformTexture("tex0", Pool::stoneMoviePool.getTexturAt(0), 0);
    	fm_rightwall.shader.setUniformTexture("tex1", Pool::stoneMoviePool.getTexturAt(1), 1);
        ofDrawRectangle(0, 0, w, h);
        fm_rightwall.shader.end();
    }
}
