#include "ofMain.h"
#include "Configuration.h"
#include "globalvars.h"
#include "ofApp.h"
#include "util/stringhelper.h"
#include "FragmentScene.h"

FragmentScene::FragmentScene() : Scene() {
}

FragmentScene::~FragmentScene() {
}

void FragmentScene::setup() {
	Scene::setup();

	mng.setSlProgName(selectDefaultSlProgram());
	reloadSl(true);

	fm_leftkwall.fragProgName = selectLeftWallFragProgram();
	setup_fm(fm_leftkwall);
	fm_rightwall.fragProgName = selectRightFragProgram();
	setup_fm(fm_rightwall);
}

void FragmentScene::setup_fm(frag_management_t& fm) {
    fm.pathFragProg = fm.fragProgName;
    if(!std::filesystem::exists(fm.pathFragProg)) {
    	fm.fatalFragProg = true;
        ofLog(OF_LOG_FATAL_ERROR) << "Cannot access '" << fm.fragProgName << "'";
    }
    else {
        reloadFragProg(fm, true);
    }
}

/// default vertex shader.
/// there must be one in the pipeline.
/// texCoordVarying is send to fragment shader, but in most cases not used there.
static std::string fallbackVert = STRINGIFY(#version 330\n
	uniform mat4 modelViewProjectionMatrix;\n
	in vec4 position;\n
	in vec2 texcoord;\n
	out vec2 texCoordVarying;\n
	void main()\n
	{\n
		texCoordVarying = vec2(texcoord.x, texcoord.y);\n
		gl_Position = modelViewProjectionMatrix * position;\n
	}\n
);

bool FragmentScene::reloadFragProg(frag_management_t& fm, const bool force) {
    if(fm.fatalFragProg || fm.fragProgName.empty())
        return false;

    bool need = force;
    if(!need) {
        long sourceModTime = std::filesystem::last_write_time(fm.pathFragProg);
        need = sourceModTime != fm.fragProgModTime;
    }

    if(need) {
    	fm.fragProgModTime = std::filesystem::last_write_time(fm.pathFragProg);
        std::ifstream file(fm.fragProgName);
        std::string line;
        std::stringstream src;
        while (std::getline(file, line)) {
            src << line << '\n';
        }
        std::string okV = fm.shader.setupShaderFromSource(GL_VERTEX_SHADER, fallbackVert) ? "OK" : "failed";
        std::string okF = fm.shader.setupShaderFromSource(GL_FRAGMENT_SHADER, src.str()) ? "OK" : "failed";
        std::string okP = fm.shader.linkProgram() ? "OK" : "failed";
    // for printing generated shader source
    //#define PRINT_SHADER_SRC
    #undef PRINT_SHADER_SRC
    #ifdef PRINT_SHADER_SRC
        ofLog(OF_LOG_NOTICE) << shader.getShaderSource(GL_VERTEX_SHADER) << "\n";
        ofLog(OF_LOG_NOTICE) << shader.getShaderSource(GL_FRAGMENT_SHADER) << "\n";
    #endif // PRINT_SHADER_SRC
        ofLog(OF_LOG_NOTICE) << "shader compiler " << fm.pathFragProg << ": vertex=" << okV << " fragment=" << okF <<  " link=" << okP;
    }
    return need;
}

void FragmentScene::setSlProgName(string pn) {
	mng.setSlProgName(pn);
}

void FragmentScene::reloadSl(const bool force) {
	bool loaded = mng.reloadSlProg(force, sl_src);
	if (loaded || force) {
		mng.compile(slP0, "p0", "0", sl_src);
		mng.compile(slP1, "p1", "0", sl_src);
		mng.compile(slP2, "p2", "0", sl_src);
		mng.compile(slP3, "p3", "0", sl_src);
		mng.compile(slP4, "p4", "0", sl_src);
		mng.compile(slP5, "p5", "0", sl_src);
		mng.compile(slP6, "p6", "0", sl_src);
		mng.compile(slP7, "p7", "0", sl_src);
	}
}

void FragmentScene::update() {
	reloadSl(false);
    reloadFragProg(fm_leftkwall, false);
    reloadFragProg(fm_rightwall, false);
	p0 = slP0.rpn.execute0();
	p1 = slP1.rpn.execute0();
	p2 = slP2.rpn.execute0();
	p3 = slP3.rpn.execute0();
	p4 = slP4.rpn.execute0();
	p5 = slP5.rpn.execute0();
	p6 = slP6.rpn.execute0();
	p7 = slP7.rpn.execute0();
    p0 = p0 + 0.99 * (prevP0 - p0); //lowpassfilter change TODO: no hardcoded alpha...
    p1 = p1 + 0.99 * (prevP1 - p1); //lowpassfilter change
    p2 = p2 + 0.99 * (prevP2 - p2); //lowpassfilter change
    p3 = p3 + 0.99 * (prevP3 - p3); //lowpassfilter change
    p4 = p4 + 0.99 * (prevP4 - p4); //lowpassfilter change
    p5 = p5 + 0.99 * (prevP5 - p5); //lowpassfilter change
    p6 = p6 + 0.99 * (prevP6 - p6); //lowpassfilter change
    p7 = p7 + 0.99 * (prevP7 - p7); //lowpassfilter change
    prevP0 = p0;
    prevP1 = p1;
    prevP2 = p2;
    prevP3 = p3;
    prevP4 = p4;
    prevP5 = p5;
    prevP6 = p6;
    prevP7 = p7;
}

void FragmentScene::sendCommonUniforms(frag_management_t& fm) {
	/// send window dimensions to the fragment shader.
	fm.shader.setUniform1i("w", w);
	fm.shader.setUniform1i("h", h);
	fm.shader.setUniform1f("p0", p0);
	fm.shader.setUniform1f("p1", p1);
	fm.shader.setUniform1f("p2", p2);
	fm.shader.setUniform1f("p3", p3);
	fm.shader.setUniform1f("p4", p4);
	fm.shader.setUniform1f("p5", p5);
	fm.shader.setUniform1f("p6", p6);
	fm.shader.setUniform1f("p7", p7);
	fm.shader.setUniform1f("dimmer", dimmer);
}
