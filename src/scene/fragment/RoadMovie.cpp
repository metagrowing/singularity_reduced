#include "ofMain.h"
#include "Configuration.h"
#include "ofApp.h"
#include "Pool.h"
#include "RoadMovie.h"

/// same media streams on both walls.
/// but different fragment shader programs

RoadMovie::RoadMovie() : FragmentScene() {
}

RoadMovie::~RoadMovie() {
}

const char * RoadMovie::sceneName() {
	return __FILE__;
}

void RoadMovie::setup() {
	FragmentScene::setup();
}

const string& RoadMovie::selectDefaultSlProgram() {
	static string prog_path = "bin/data/movie_mixer/road_movie.sl";
	return prog_path;
}

const string& RoadMovie::selectLeftWallFragProgram() {
	static string prog_path = "bin/data/movie_mixer/single_video.frag";
	return prog_path;
}

const string& RoadMovie::selectRightFragProgram() {
	static string prog_path = "bin/data/movie_mixer/single_video.frag";
	return prog_path;
}

void RoadMovie::drawLeftWall() {
    if(fm_leftkwall.fatalFragProg || fm_leftkwall.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
    	fm_leftkwall.shader.begin();
    	sendCommonUniforms(fm_leftkwall);
    	// attention numbering: tex0 -> texture unit 0
    	//                      tex1 -> texture unit 1
    	//                      tex2 -> texture unit 2
    	//                      tex3 -> texture unit 3
    	fm_leftkwall.shader.setUniformTexture("tex0", Pool::roadMoviePool.getTexturAt(0), 0);
    	// fm_leftkwall.shader.setUniformTexture("tex1", Pool::roadMoviePool.getTexturAt(1), 1);
//    	fm_leftkwall.shader.setUniformTexture("tex2", Pool::roadMoviePool.getTexturAt(2), 2);
//    	fm_leftkwall.shader.setUniformTexture("tex3", Pool::roadMoviePool.getTexturAt(YYY), 3);
        ofDrawRectangle(0, 0, w, h);
        fm_leftkwall.shader.end();
    }
}

void RoadMovie::drawRightWall() {
    if(fm_rightwall.fatalFragProg || fm_rightwall.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
    	fm_rightwall.shader.begin();
    	sendCommonUniforms(fm_rightwall);
    	// attention numbering: tex0 -> texture unit 4
    	//                      tex1 -> texture unit 5
    	//                      tex2 -> texture unit 6
    	//                      tex3 -> texture unit 7
    	fm_rightwall.shader.setUniformTexture("tex0", Pool::roadMoviePool.getTexturAt(1), 4);
    	// fm_rightwall.shader.setUniformTexture("tex1", Pool::roadMoviePool.getTexturAt(2), 5);
//    	fm_rightwall.shader.setUniformTexture("tex2", Pool::roadMoviePool.getTexturAt(0), 6);
//    	fm_rightwall.shader.setUniformTexture("tex3", Pool::roadMoviePool.getTexturAt(YYY), 7);
        ofDrawRectangle(0, 0, w, h);
        fm_rightwall.shader.end();
    }
}
