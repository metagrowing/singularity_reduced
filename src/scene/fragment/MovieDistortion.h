#pragma once
#include "ofMain.h"
#include "Configuration.h"
#include "FragmentScene.h"

/// \class Example for using GLSL to produce glitch effects.
class MovieDistortion: public FragmentScene {
public:
    MovieDistortion();
    virtual ~MovieDistortion();

    virtual const char * sceneName();

    /// For setting the file path to the simple Lisp-like expressions.
	virtual const string& selectDefaultSlProgram();
//	virtual void setSlProgName(string pn);

    virtual void setup();

    /// For setting the file path to the GLSLfragment shader program.
    virtual const string& selectLeftWallFragProgram();

    /// For setting the file path to the GLSLfragment shader program.
    virtual const string& selectRightFragProgram();

    /// Draws on the left wall.
    virtual void drawLeftWall();

    /// Draws on the back right wall.
    virtual void drawRightWall();

    // index to access a single image from the image pool
    size_t currentTexture = 0;

    /// expressions calculated per frame
    /// and send as uniforms to the fragment shader
//	ScalarLispMng mng;
    std::vector<std::string> sl_src;
    ScalarLisp slTrigger;
    virtual void reloadSl(const bool force);
};
