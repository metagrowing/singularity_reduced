#include "ofMain.h"
#include "Configuration.h"
#include "ofApp.h"
#include "Pool.h"
#include "MediaStream.h"
#include "MovieDistortion.h"

/// different media streams on the walls.
/// and different fragment shader programs.

MovieDistortion::MovieDistortion() : FragmentScene() {
}

MovieDistortion::~MovieDistortion() {
}

const char * MovieDistortion::sceneName() {
	return __FILE__;
}

void MovieDistortion::setup() {
	FragmentScene::setup();

	mng.setSlProgName(selectDefaultSlProgram());
	reloadSl(true);
}

const string& MovieDistortion::selectDefaultSlProgram() {
	static string prog_path = "bin/data/glitch_distorted/trigger_change_image.sl";
	return prog_path;
}

const string& MovieDistortion::selectLeftWallFragProgram() {
	static string prog_path = "bin/data/glitch_distorted/distort_by_channel.frag";
	return prog_path;
}

const string& MovieDistortion::selectRightFragProgram() {
	static string prog_path = "bin/data/glitch_distorted/glitch.frag";
	return prog_path;
}

//void MovieDistortion::setSlProgName(string pn) {
//	mng.setSlProgName(pn);
//}

void MovieDistortion::reloadSl(const bool force) {
	FragmentScene::reloadSl(force);
	bool loaded = mng.reloadSlProg(force, sl_src);
	if (loaded || force) {
		mng.compile(slTrigger, "trigger", "-1", sl_src);
	}
}

void MovieDistortion::drawLeftWall() {
    if(fm_leftkwall.fatalFragProg || fm_leftkwall.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
    	fm_leftkwall.shader.begin();
    	sendCommonUniforms(fm_leftkwall);
    	// attention numbering: tex0 -> texture unit 0
    	//                      tex1 -> texture unit 1
    	fm_leftkwall.shader.setUniformTexture("tex0", Pool::stoneMoviePool.getTexturAt(0), 0);
    	fm_leftkwall.shader.setUniformTexture("tex1", Pool::stoneMoviePool.getTexturAt(1), 1);
        ofDrawRectangle(0, 0, w, h);
        fm_leftkwall.shader.end();
    }
}

void MovieDistortion::drawRightWall() {
    if(fm_rightwall.fatalFragProg || fm_rightwall.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
    	reloadSl(false);
    	if(slTrigger.rpn.execute0() >= 0.0) {
    		++currentTexture;
    	}
    	fm_rightwall.shader.begin();
    	sendCommonUniforms(fm_rightwall);
    	// attention numbering: tex0 -> texture unit 0
    	//                      tex1 -> texture unit 1
    	fm_rightwall.shader.setUniformTexture("tex0", Pool::stoneMoviePool.getTexturAt(0), 0);
    	fm_rightwall.shader.setUniformTexture("tex1", Pool::treeImagePool.getTexturAt(currentTexture), 1);
        ofDrawRectangle(0, 0, w, h);
        fm_rightwall.shader.end();
    }
}
