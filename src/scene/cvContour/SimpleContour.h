#pragma once
#include "operate.h"
#ifdef WITH_Cv

#include "ofMain.h"
#include "ofxCv.h"

#include "Configuration.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "Scene.h"

/// \class simple openCV contour finder
/// needs ofxCv addon
class SimpleContour: public Scene {
public:
	SimpleContour();
	virtual ~SimpleContour();
	virtual const char * sceneName();
	virtual void setSlProgName(string pn);
	virtual void setup();
	virtual void update();
	virtual void drawLeftWall();
	virtual void drawRightWall();

    /// expressions calculated per frame
    /// and send as uniforms to the fragment shader
	ScalarLispMng mng;
    std::vector<std::string> sl_src;
    ScalarLisp slTriggerLevel;
    ScalarLisp slIncrease;
    double increase = 1.033;
    virtual void reloadSl(const bool force);

	ofxCv::ContourFinder contourFinder;
	size_t num_elements = 0;
	size_t index = 0;
	float circle_radius = 1.0;
};
#endif // WITH_Cv
