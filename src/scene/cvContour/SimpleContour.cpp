#include "operate.h"
#ifdef WITH_Cv

#include "opencv2/opencv.hpp"

#include "globalvars.h"
#include "Pool.h"
#include "scene/cvContour/SimpleContour.h"

SimpleContour::SimpleContour() : Scene() {
}

SimpleContour::~SimpleContour() {
}

const char * SimpleContour::sceneName() {
	return __FILE__;
}

void SimpleContour::setup() {
	Scene::setup();
	mng.setSlProgName("bin/data/simple_contour/trigger_increase.sl");
	reloadSl(true);

	num_elements = Pool::treeCvImagePool.size();
}

void SimpleContour::setSlProgName(string pn) {
	mng.setSlProgName(pn);
}

void SimpleContour::reloadSl(const bool force) {
	bool loaded = mng.reloadSlProg(force, sl_src);
	if (loaded || force) {
		mng.compile(slTriggerLevel, "level", "-1", sl_src);
		mng.compile(slIncrease, "increase", "1.033", sl_src);
	}
}

void SimpleContour::update() {
	reloadSl(false);
	if(num_elements > 0 && slTriggerLevel.rpn.execute0() >= 0.0) {

		// select a random image and find a new contours
		index = ofRandom(num_elements);
		cv::Mat& gm = Pool::treeCvImagePool.getMatAt(index);

		contourFinder.setMinAreaRadius(10);
		contourFinder.setMaxAreaRadius(200);
		contourFinder.setThreshold(128);
		contourFinder.setFindHoles(true);
		contourFinder.findContours(gm);

		circle_radius = 1.0;
	}
	increase = slIncrease.rpn.execute0();
	circle_radius *= increase;
}

void SimpleContour::drawLeftWall() {
	if(num_elements > 0) {
		// inverted tree image, but in blue
		ofSetColor(dimmer*10, dimmer*64, dimmer*196);
		cv::Mat& gm = Pool::treeCvImagePool.getMatAt(index);
		ofxCv::drawMat(gm, 0, 0);

		// contours in red
		ofNoFill();
		ofSetColor(dimmer*255, dimmer*0, dimmer*0);
		const vector<ofPolyline>& polylines = contourFinder.getPolylines();
        for(size_t i = 0; i < polylines.size(); i++) {
			polylines[i].draw();
		}

		// growing circles
		ofFill();
		ofSetColor(dimmer*196, dimmer*64, dimmer*10, 128);
		ofEnableAlphaBlending();
    	const std::vector<cv::Rect>& boundingRects = contourFinder.getBoundingRects();
        for(size_t i = 0; i < boundingRects.size(); i++) {
    		cv::Rect box = boundingRects[i];
    		ofDrawCircle(box.x + box.width / 2, box.y + box.height / 2, circle_radius);
		}
        ofDisableAlphaBlending();
	}
	else {
		ofClear(0);
	}
}

void SimpleContour::drawRightWall() {
	if(num_elements > 0) {
		// inverted tree image
		ofSetColor(dimmer*255);
		cv::Mat& gm = Pool::treeCvImagePool.getMatAt(index);
		ofxCv::drawMat(gm, 0, 0);
	}
	else {
		ofClear(0);
	}
}
#endif // WITH_Cv

