#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "Scene.h"

/// \class both windows completely black
class Black: public Scene {
public:
	Black();
	virtual ~Black();
	virtual const char * sceneName();
	virtual void drawLeftWall();
	virtual void drawRightWall();
};
