#pragma once 

#include "ofMain.h"
#include "Configuration.h"

#include "Particle.h"
#include "sensor/minibee/MiniBeeData.h"
#include "Scene.h"

/// \class number flock animation
class NumberFlock: public Scene {

    public:
        NumberFlock();
        virtual ~NumberFlock();
        virtual const char * sceneName();
        virtual void setup();
        virtual void update();
        virtual void drawLeftWall();
        virtual void drawRightWall();

        //	ofVideoPlayer background;

    private:
        void resetParticles();
        vector<Particle> p;

        ofShader dimmer_shader;
        ofFbo dimmer_fbo;
};
