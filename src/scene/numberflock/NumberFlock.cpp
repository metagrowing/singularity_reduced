#include "stringhelper.h"
#include "globalvars.h"
#include "Pool.h"
#include "NumberFlock.h"

NumberFlock::NumberFlock() : Scene() {
}

NumberFlock::~NumberFlock() {
}

/// default vertex shader.
/// there must be one in the pipeline.
static std::string fallbackVert = STRINGIFY(#version 330\n
	uniform mat4 modelViewProjectionMatrix;\n
	in vec4 position;\n
	in vec2 texcoord;\n
	out vec2 texCoordVarying;\n
	void main()\n
	{\n
		texCoordVarying = vec2(texcoord.x, texcoord.y);\n
		gl_Position = modelViewProjectionMatrix * position;\n
	}\n
);

/// hard coded fragment shader for fading to black.
static std::string fallbackFrag = STRINGIFY(#version 330\n
	out vec4 outputColor;\n
	uniform sampler2DRect tex0;\n
	uniform float dimmer;\n
	uniform int h; // height\n
	void main()\n
	{\n
		vec3 cin0 = texture(tex0, vec2(gl_FragCoord.x, gl_FragCoord.y)).xyz;\n
		outputColor = vec4(dimmer*cin0, 1.0);\n
	}\n
);

const char * NumberFlock::sceneName() {
	return __FILE__;
}

void NumberFlock::setup() {
	Scene::setup();

//	dimmer_fbo.allocate(w, h, GL_RGBA32F_ARB);
	dimmer_fbo.allocate(w, h, GL_RGBA);

    std::string okV = dimmer_shader.setupShaderFromSource(GL_VERTEX_SHADER, fallbackVert) ? "OK" : "failed";
    std::string okF = dimmer_shader.setupShaderFromSource(GL_FRAGMENT_SHADER, fallbackFrag) ? "OK" : "failed";
    std::string okP = dimmer_shader.linkProgram() ? "OK" : "failed";


//TODO use ImageStream
	string path = ofToString("numbers");
	ofDirectory dir(path);
	string ext = ofToString("png");
	dir.allowExt(ext);
	dir.listDir();

	int num = 100;

	p.assign(num, Particle());
	for (int i = 0; i < num; i++) {
		int index = int(ofRandom(dir.size()));
		p[i].loadImage(dir.getPath(index));
		p[i].scale = 40;
	}

	this->resetParticles();
}

void NumberFlock::update() {
	for (size_t i = 0; i < p.size(); i++) {
		p[i].update();
	}
}

void NumberFlock::drawLeftWall() {
	dimmer_fbo.begin();
	dimmer_shader.begin();
	dimmer_shader.setUniform1i("h", h);
	dimmer_shader.setUniform1f("dimmer", dimmer);

	Pool::snowyMoviePool.draw(0);

	// TODO
	// The movie snowyBackground.mp4 in class NumberFlock can be faded out.
	// But only the movie, not the particles.
//	for (size_t i = 0; i < p.size(); i++) {
//		p[i].draw();
//	};
	dimmer_shader.end();
	dimmer_fbo.end();

	dimmer_fbo.draw(0, 0);

	// Draws the particles over the the dimmer_fbo.
	// Particles are not faded out, due to massive graphical errors.  TODO
	// Fortunately, the particles are in dark colors, so the error is only noticeable at second glance.
	for (size_t i = 0; i < p.size(); i++) {
		p[i].draw();
	};
}

void NumberFlock::drawRightWall() {
	drawLeftWall();
}

void NumberFlock::resetParticles() {
	for (size_t i = 0; i < p.size(); i++) {
		p[i].reset();
	}
}
