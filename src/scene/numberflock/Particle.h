#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "sensor/minibee/MiniBeeData.h"

enum particleMode{
    PARTICLE_MODE_ATTRACT = 0,
    PARTICLE_MODE_REPEL,
    PARTICLE_MODE_NEAREST_POINTS,
    PARTICLE_MODE_NOISE
};

class Particle{

    public:
        Particle();

        void loadImage(string fileName );
        void setMode(particleMode newMode);

        void reset();
        void update();
        void draw();		

        glm::vec3 pos = { 0, 0, 0};
        glm::vec3 vel = { 0, 0, 0};
        glm::vec3 frc = { 0, 0, 0};
        glm::vec3 rotVel = { 0, 0, 0};

        float degree = 0;

        float drag = 0;
        float uniqueVal = 0;

        float scale = 0;

        ofImage image;
        int alpha = 0;

        particleMode mode = PARTICLE_MODE_ATTRACT;

        vector <glm::vec3> * attractPoints = nullptr;
};
