#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxOscReceiver.h"

#include "Configuration.h"
#include "MiniBee.h"

class MiniBeeData {

    public:
        MiniBeeData();

        void setup();
        void update();

        void update(size_t id, float x, float y, float z);
        void setDeltaAlpha(float alpha);
        void setXAlpha(float alpha);
        void setYAlpha(float alpha);
        float getDeltaAlpha();
        float getXAlpha();
        float getYAlpha();

        bool isConnected(size_t idx);
        float transferRate(size_t idx);

        MiniBee& getMiniBee(size_t idx);

        const int port = 12345;
        vector <MiniBee> minibee;
        vector <MiniBee> prevValues;
        const int idOffset = 9;
        const uint64_t timeOut = 1000;

    private:
        float calcDelta(vec3 current, vec3 previous);
        float calcSmooth(float current, float previous, float alpha);
        float deltaAlpha = 0.0;
        float xAlpha = 0.0;
        float yAlpha = 0.0;
        float zAlpha = 0.0;
        vector<uint64_t> t0;
};
