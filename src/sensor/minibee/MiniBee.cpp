#include "MiniBee.h"

MiniBee::MiniBee() {
}

void MiniBee::setup() {
}

void MiniBee::setX(float in) {
	data.x = ofMap(in, 0.47020, 0.534, 0.0, 1.0, true);
}
void MiniBee::setXSmooth(float in) {
	dataSmooth.x = in;
}
void MiniBee::setY(float in) {
	data.y = ofMap(in, 0.469, 0.5322, 0.0, 1.0, true);
}
void MiniBee::setYSmooth(float in) {
	dataSmooth.y = in;
}
void MiniBee::setZ(float in) {
	data.z = ofMap(in, 0.469, 0.650, 0.0, 1.0, true);
}
void MiniBee::setZSmooth(float in) {
	dataSmooth.z = in;
}
void MiniBee::setDelta(float in) {
	delta = in;
}
void MiniBee::setDeltaSmooth(float in) {
	deltaSmooth = in;
}
