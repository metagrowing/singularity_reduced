#pragma once

#include "ofMain.h"

#include "sensor/minibee/MiniBeeData.h"
#include "calc/ScalarLisp.h"

extern unsigned int fps;
extern unsigned int frame;
extern float dimmer;

extern MiniBeeData mbData;
extern const size_t numMBs;

extern ScalarLisp slT0;
extern ScalarLisp slT1;
extern ScalarLisp slT2;
extern ScalarLisp slT3;
extern ScalarLisp slT4;
extern ScalarLisp slT5;
extern ScalarLisp slT6;
extern ScalarLisp slT7;

extern float u0, u1, u2, u3, u4, u5, u6, u7;
extern float t0, t1, t2, t3, t4, t5, t6, t7;
extern float kp0, kp1, kp2, kp3, kp4, kp5, kp6, kp7;
extern float ks0, ks1, ks2, ks3, ks4, ks5, ks6, ks7;
extern float kb0, kb1, kb2, kb3, kb4, kb5, kb6, kb7;

extern float h0;
extern float s0;
extern float v0;

extern float lx;
extern float ly;
extern float rx;
extern float ry;
extern float l2;
extern float r2;

#ifdef WITH_openCL
// extern Inspector inspector;
#endif // WITH_openCL
extern ScalarLisp slInspectChannels;
extern ScalarLisp slInspectX0;
extern ScalarLisp slInspectY0;
extern ScalarLisp slInspectR0;
extern ScalarLisp slInspectX1;
extern ScalarLisp slInspectY1;
extern ScalarLisp slInspectR1;
extern ScalarLisp slInspectX2;
extern ScalarLisp slInspectY2;
extern ScalarLisp slInspectR2;
extern ScalarLisp slInspectX3;
extern ScalarLisp slInspectY3;
extern ScalarLisp slInspectR3;


extern std::string dumpState();
extern std::string dumpNkState();
extern void saveState();
extern void loadState();

extern float amplitude;
extern float pitch0;
extern float pitch1;
