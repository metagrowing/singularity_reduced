#pragma once

#include "ofMain.h"
#include "Configuration.h"

/// \class base class for all media streams
/// \brief provides empty default functions for derivative media classes

// Mermaid diagram
//
//classDiagram
//    class MediaStream
//    <<interface>> MediaStream
//
//    class Pool
//    Pool "1" --> "*" MediaStream
//
//    MediaStream <|-- ImageStream
//    MediaStream <|-- MovieStream

class MediaStream {
public:
	MediaStream();
	virtual ~MediaStream();

	/// initialize a pool, this is called by ofApp
	/// Call setup after openFramework is fully initialized.
	virtual void setup();

	/// Append media files or streams to this pool.
	///
	/// Media files are specified by glob patterns.
	/// https://en.wikipedia.org/wiki/Glob_%28programming%29
	/// \param[in] path a glob pattern to select files.
	/// \param[in] limit the number of files to load.
	virtual void append(std::string path, size_t limit);

	/// free system resources
	virtual void clear();

	/// Resize the media input to the size needed for the screen size.
	///
	/// Images are resized immediately. For improving performance during runtime.
	/// Movies and input from WEB Cam are resized when the images are used.
	/// \param[in] w_ new width
	/// \param[in] h_ new height
	virtual void resize(size_t w_, size_t h_);

	/// Per frame update of the media stream.
	/// This is called by ofApp.
	virtual void update();

	/// Draws one from the pool.
	/// \param[in] index to the image or video stream.
	virtual void draw(size_t index);

	/// Get access to the internal graphics data.
	/// The image or the video stream is randomly selected.
	virtual ofPixels& getRandom();

	/// Get access to the internal graphics data.
	/// \param[in] index to the image or video stream.
	virtual ofPixels& getAt(size_t index);

public:
	ofImage fallback;
	size_t w = 0;
	size_t h = 0;
};
