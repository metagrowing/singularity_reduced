#pragma once
#include "operate.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "MSAOpenCL.h"
#include "Configuration.h"
#include "Scene.h"

#define CL_TILES_X 2
#define CL_TILES_Y 1


/// \class base class for all scene classes using OpenCL.
/// \brief provides the call to then OpenCl just in time compiler.

class ClScene: public Scene {
public:
    ClScene();
    virtual ~ClScene();

    virtual void setup();

    /// For setting the file path to the OpenCL program.
    /// There must be one OpenCL program with two kernel functions per scene.
    /// Names of the kernel functions are: image_setup and image_update.
    virtual const string& selectClProgram() = 0;

    /// Load and compile the OpenCL program.
    /// ClScene will check the last write time of the OpenCL program file.
    /// If you save the file from an editor, the OpenCL program is reloaded.
    /// No need to stop singularity, after making small changes in the OpenCL part.
    virtual bool reloadClProg(const bool force);

    /// Draws clOutImage on the left wall.
    /// You can overwrite this in a derived class.
    virtual void drawLeftWall();

    /// Draws clOutImage on the right wall.
    /// You can overwrite this in a derived class.
    virtual void drawRightWall();

    string clProgName;
    long clProgModTime = 0;
    bool fatalClProg = false;
    std::filesystem::path pathClProg;
    msa::OpenCLProgramPtr pcl = nullptr;

    /// kernel used to initialize the GPU memory buffers.
    msa::OpenCLKernelPtr setup_kernel;
    /// kernel used to update the GPU memory buffers per frame.
    msa::OpenCLKernelPtr update_kernel;

    /// Next two variables manage the same GPU memory region.
    /// An OpenGL memory buffer at the same address as the OpenGL texture buffer.
    /// Management is done by: initFromTexture(); glFinish(); openCL.finish();

    /// This is the memory buffer that will be written by the image_setup and image_update kernel.
    msa::OpenCLImage clOutImage;

    /// This is the texture buffer that will be drawn from OpenGL.
    ofFbo clOutFbo;

};
#endif // WITH_openCL
