#pragma once

// In a production system test cases can be disabled.
//#define WITH_testcases
#undef WITH_testcases

// to enable OpenCL:
//   add an ofxMSAOpenCL line to addons.make
//   define WITH_openCL
//#define WITH_openCL

// to disable OpenCL:
//   remove ofxMSAOpenCL from addons.make
//   undefine WITH_openCL
#undef WITH_openCL

// ofxCv
// Download from: https://github.com/kylemcdonald/ofxCv
//
// to enable ofxCv:
//   add an ofxCv line to addons.make
//   define WITH_Cv
#define WITH_Cv
//#undef WITH_Cv

// NanoKontrol2
// Download from: https://github.com/arturoc/ofxMidi
// TODO Is this the correct link?
// to enable NanoKontrol2:
//   add an ofxMidi line to addons.make
//   define WITH_NanoKontrol2
//#define WITH_nanoKontrol2_midi_input
#undef WITH_nanoKontrol2_midi_input
