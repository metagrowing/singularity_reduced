#include "ofMain.h"
#include "util/pathhelper.h"
#include "MovieStream.h"

MovieStream::MovieStream() : MediaStream() {
	fallback.allocate(Configuration::windowWidth, Configuration::windowHeight, OF_IMAGE_COLOR);
	fallback.clear();
}

MovieStream::~MovieStream() {
}

void MovieStream::clear() {
	for(auto& m : movies) {
		m.stop();
		m.close();
	}
}

void MovieStream::append(std::string path, size_t limit) {
	// populate a vector full of movies file names
	vector<std::string> mediaDir = file_names("bin/data/" + path);

	// load limit_min videos from the directory
	size_t limit_min = std::min(limit, mediaDir.size());
	for (size_t i = 0; i < limit_min; i++) {
		ofVideoPlayer media;
		bool loaded = media.load(mediaDir[i]);
		if(loaded) {
            media.setLoopState(OF_LOOP_NORMAL);
            media.setVolume(0);
            media.setPaused(true);
			movies.push_back(media);
		}
		else {
			ofLogError() << "MoviePool::setup failed to load '" << mediaDir[i] << "'";
		}
	};
	ofLogNotice() << movies.size() << " movies loaded from '" << path << "'";
}

void MovieStream::MovieStream::draw(size_t index) {
	switch_to_play();
	movies[index % movies.size()].draw(0, 0, w, h);
}

void MovieStream::update() {
    update_counter = 0;
    if(paused)
        return;

    for(auto& m : movies) {
        m.update();
        ++update_counter;
    }
}

void MovieStream::stop() {
    for(auto& m : movies) {
        m.stop();
    }
}

void MovieStream::switch_to_pause() {
    if(paused)
        return;

    paused = true;
    for(auto& m : movies) {
        m.setPaused(true);
    }
}

void MovieStream::switch_to_play() {
    if(!paused)
        return;

    paused = false;
    for(auto& m : movies) {
        m.setPaused(false);
        m.update();
    }
}


ofPixels& MovieStream::getRandom() {
	switch_to_play();
	size_t num_elements = movies.size();
	switch(num_elements) {
	case 0:
		return fallback.getPixels();
	case 1: {
		ofPixels& p = movies[0].getPixels();
		return p.isAllocated() ? p : fallback.getPixels();
	}
	}

	{
	int index = ofRandom(num_elements);
	ofPixels& p = movies[index].getPixels();
	return p.isAllocated() ? p : fallback.getPixels();
	}
}

ofPixels& MovieStream::getAt(size_t index) {
	switch_to_play();
	size_t num_elements = movies.size();
	switch(num_elements) {
	case 0:
		return fallback.getPixels();
	case 1: {
		ofPixels& p = movies[0].getPixels();
		return p.isAllocated() ? p : fallback.getPixels();
	}
	}

	{
	ofPixels& p = movies[index % movies.size()].getPixels();
	return p.isAllocated() ? p : fallback.getPixels();
	}
}

ofTexture& MovieStream::getTexturAt(size_t index) {
	switch_to_play();
	size_t num_elements = movies.size();
	switch(num_elements) {
	case 0:
		return fallback.getTexture();
	case 1: {
		ofTexture& t = movies[0].getTexture();
		return t.isAllocated() ? t : fallback.getTexture();
	}
	}

	{
	ofTexture& t = movies[index % movies.size()].getTexture();
	return t.isAllocated() ? t : fallback.getTexture();
	}
}
