#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "MediaStream.h"

/// \class A container holding sounds.
class SoundStream : public MediaStream {
public:
	SoundStream();
	virtual ~SoundStream();

	/// stop all  sound players in this container
	virtual void clear();

	/// Append sound files to this container.
	///
	/// Sound files are specified by glob patterns.
	/// https://en.wikipedia.org/wiki/Glob_%28programming%29
	/// \param[in] path a glob pattern to select files.
	/// \param[in] limit the number of files to load.
	virtual void append(std::string path, size_t limit);

    virtual void play(size_t index);
    virtual void stop(size_t index);
    virtual bool isPlayingAt(size_t index);

protected:
	vector<ofSoundPlayer> sounds;
};
