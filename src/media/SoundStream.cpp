#include "ofMain.h"
#include "util/pathhelper.h"
#include "SoundStream.h"

SoundStream::SoundStream() {
}

SoundStream::~SoundStream() {
}

void SoundStream::clear() {
	for(auto& m : sounds) {
		m.unload();
	}
}

void SoundStream::append(std::string path, size_t limit) {
	vector<std::string> mediaDir = file_names("bin/data/" + path);

	// load limit_min sounds from the directory
	size_t limit_min = std::min(limit, mediaDir.size());
	for (size_t i = 0; i < limit_min; i++) {
		ofSoundPlayer media;
		bool loaded = media.load(mediaDir[i]);
		if(loaded) {
            media.setLoop(false);
            media.setVolume(1.0);
			sounds.push_back(media);
		}
		else {
			ofLogError() << "SoundPool::setup failed to load '" << mediaDir[i] << "'";
		}
	};
	ofLogNotice() << sounds.size() << " sounds loaded from '" << path << "'";
}

void SoundStream::play(size_t index){
    sounds[index].play();
	ofLogNotice() << " Playing sound " << index;
}

void SoundStream::stop(size_t index){
    sounds[index].stop();
	ofLogNotice() << " Stopping sound " << index;
}

bool SoundStream::isPlayingAt(size_t index) {
    bool p = sounds[index].isPlaying();
    ofLogNotice() << "Sound playing: " << index << " " << p;
    return p;
}
