#include "operate.h"
#ifdef WITH_Cv
//#include "opencv2/opencv.hpp"

#include "ofMain.h"
#include "ofxCv.h"

#include "util/pathhelper.h"
#include "media/CvImageStream.h"

CvImageStream::CvImageStream() {
}

CvImageStream::~CvImageStream() {
}

void CvImageStream::append(size_t w_, size_t h_, bool invert, std::string path, size_t limit) {
	// populate a vector full of images
	vector<std::string> imageDir = file_names("bin/data/" + path);
	// load limit_min images from the directory and convert to cv::Mat
	size_t limit_min = std::min(limit, imageDir.size());
	for (size_t i = 0; i < limit_min; i++) {
		ofImage media;
		bool loaded = media.load(imageDir[i]);
		if(loaded) {
			cv::Mat src = ofxCv::toCv(media);
			cv::Mat resized;
			cv::resize(src, resized, cv::Size(w_,  h_));
			if(invert)
				cv::bitwise_not(resized, resized);
			cv_images.push_back(resized);
		}
		else {
			ofLogError() << "CvGrayImageStream::setup failed to load '" << imageDir[i] << "'";
		}
	};
	ofLogNotice() << cv_images.size() << " cv gray images loaded from '" << path << "'";
}

cv::Mat& CvImageStream::getRandomMat() {
	size_t num_elements = cv_images.size();
	switch(num_elements) {
	case 0:
		return cv_fallback;
	case 1:
		return cv_images[0];
	}
	int index = ofRandom(num_elements);
	return cv_images[index];
}

cv::Mat& CvImageStream::getMatAt(size_t index) {
	size_t num_elements = cv_images.size();
	switch(num_elements) {
	case 0:
		return cv_fallback;
	case 1:
		return cv_images[0];
	}
	return cv_images[index % cv_images.size()];
}

size_t CvImageStream::size() {  //TODO all stream classes need a size function
	return cv_images.size();
}
#endif // WITH_Cv
