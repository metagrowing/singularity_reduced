#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "MediaStream.h"

/// \class A container holding movies.
class MovieStream : public MediaStream {
public:
	MovieStream();
	virtual ~MovieStream();

	/// stop all movie players in this container
	virtual void clear();

	/// Append movie files to this container.
	///
	/// Movie files are specified by glob patterns.
	/// https://en.wikipedia.org/wiki/Glob_%28programming%29
	/// \param[in] path a glob pattern to select files.
	/// \param[in] limit the number of files to load.
	virtual void append(std::string path, size_t limit);

	/// Per frame update of all movie stream.
	/// This is called by ofApp.
	virtual void update();

	/// Draws one frame from movie at index.
	/// \param[in] index to the movie stream.
	virtual void draw(size_t index);

//    /// Starts playback of all movies
//    virtual void play();

    /// Stops playback of all movies
    virtual void stop();

	virtual void switch_to_pause();
	virtual void switch_to_play();

	/// Get access to the internal graphics data of the current movie frame.
	/// The movie is randomly selected.
	/// You can not jump randomly between different frames inside a movie.
	virtual ofPixels& getRandom();

	/// Get access to the internal graphics data of the current movie frame.
	/// The movie is selected by an index.
	/// You can not jump between different frames inside a movie.
	/// \param[in] index to the movie.
	virtual ofPixels& getAt(size_t index);

	ofTexture& getTexturAt(size_t index);

	size_t update_counter = 0; // testing only
	size_t playing_counter = 0;  // TODO testing only, remove this

protected:
	vector<ofVideoPlayer> movies;

	bool paused = true;
};
