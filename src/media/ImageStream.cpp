#include "ofMain.h"
#include "util/pathhelper.h"
#include "ImageStream.h"

ImageStream::ImageStream() : MediaStream() {
	fallback.allocate(Configuration::windowWidth, Configuration::windowHeight, OF_IMAGE_COLOR);
	fallback.clear();
}

ImageStream::~ImageStream() {
}

void ImageStream::append(std::string path, size_t limit) {
	// populate a vector full of images
	vector<std::string> imageDir = file_names("bin/data/" + path);
	// load limit_min images from the directory
	size_t limit_min = std::min(limit, imageDir.size());
	for (size_t i = 0; i < limit_min; i++) {
		ofImage media;
		bool loaded = media.load(imageDir[i]);
		if(loaded) {
			images.push_back(media);
		}
		else {
			ofLogError() << "ImagePool::setup failed to load '" << imageDir[i] << "'";
		}
	};
	ofLogNotice() << images.size() << " images loaded from '" << path << "'";
}

void ImageStream::resize(size_t w_, size_t h_) {
	MediaStream::resize(w_, h_);
	for(auto& i : images) {
		i.resize(w_, h_);
	}
}

ofPixels& ImageStream::getRandom() {
	size_t num_elements = images.size();
	switch(num_elements) {
	case 0:
		return fallback;
	case 1:
		return images[0].getPixels();
	}
	int index = ofRandom(num_elements);
	return images[index].getPixels();
}

ofPixels& ImageStream::getAt(size_t index) {
	size_t num_elements = images.size();
	switch(num_elements) {
	case 0:
		return fallback;
	case 1:
		return images[0].getPixels();
	}
	return images[index % images.size()].getPixels();
}

ofTexture& ImageStream::getRandomTexture() {
	size_t num_elements = images.size();
	switch(num_elements) {
	case 0:
		return fallback.getTexture();
	case 1:
		return images[0].getTexture();
	}
	int index = ofRandom(num_elements);
	return images[index].getTexture();
}

ofTexture& ImageStream::getTexturAt(size_t index) {
	size_t num_elements = images.size();
	switch(num_elements) {
	case 0:
		return fallback.getTexture();
	case 1:
		return images[0].getTexture();
	}
	return images[index % images.size()].getTexture();
}

