#pragma once
#include "operate.h"
#ifdef WITH_Cv

#include "ofMain.h"
#include "ofxCv.h"

#include "Configuration.h"
#include "MediaStream.h"

/// \class A container holding cv:mat images.
class CvImageStream : public MediaStream {
public:
	CvImageStream();
	virtual ~CvImageStream();

	/// Append image files as cv:Mat to this container.
	///
	/// Image files are specified by glob patterns.
	/// https://en.wikipedia.org/wiki/Glob_%28programming%29
	/// Images are resized immediately. For improving performance during runtime.
	/// \param[in] w_ new width
	/// \param[in] h_ new height
	/// \param[in] invert invert colors
	/// \param[in] path a glob pattern to select files.
	/// \param[in] limit the number of files to load.
	virtual void append(size_t w_, size_t h, bool invert, std::string path, size_t limit);

	/// Get access to the internal cv:Mat.
	/// The image is randomly selected.
	cv::Mat& getRandomMat();

	/// Get access to the internal cv:Mat.
	/// The cv:Mat is selected by index.
	/// \param[in] index to the cv::Mat.
	cv::Mat& getMatAt(size_t index);

	virtual size_t size();

protected:
	vector<cv::Mat> cv_images;
	cv::Mat cv_fallback;
};
#endif // WITH_Cv
