#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "MediaStream.h"

/// \class A container holding images.
class ImageStream : public MediaStream {
public:
	ImageStream();
	virtual ~ImageStream();

	/// Append image files to this container.
	///
	/// Image files are specified by glob patterns.
	/// https://en.wikipedia.org/wiki/Glob_%28programming%29
	/// \param[in] path a glob pattern to select files.
	/// \param[in] limit the number of files to load.
	virtual void append(std::string path, size_t limit);

	/// Resize the images to the size needed for the screen size.
	///
	/// Images are resized immediately. For improving performance during runtime.
	/// \param[in] w_ new width
	/// \param[in] h_ new height
	virtual void resize(size_t w_, size_t h_);

	/// Get access to the internal graphics data.
	/// The image is randomly selected.
	virtual ofPixels& getRandom();

	/// Get access to the internal graphics data.
	/// The image is selected by index.
	/// \param[in] index to the image or video stream.
	virtual ofPixels& getAt(size_t index);

	virtual ofTexture& getRandomTexture();

	virtual ofTexture& getTexturAt(size_t index);

protected:
	vector<ofImage> images;
};
