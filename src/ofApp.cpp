#include "operate.h"
#include "ofMain.h"
#include "Configuration.h"
#include "globalvars.h"
#include "util/stringhelper.h"
#include "util/pathhelper.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLispTest.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "scene/testcard/Testcard.h"
#include "scene/black/Black.h"
#include "scene/physarum/BackWallMoviePhysarum.h"
#include "scene/physarum/CleanPhysarum.h"
#include "scene/physarum/MovieImagePhysarum.h"
#include "scene/numberflock/NumberFlock.h"
#include "scene/fragment/FragDistortion.h"
#include "scene/fragment/MovieMixer.h"
#include "scene/fragment/RoadMovie.h"
#include "scene/fragment/MovieDistortion.h"
#include "scene/cvContour/SimpleContour.h"
#include "scene/lsys/LsysScene.h"
#include "scene/lsys/LsysMachineTest.h"
#include "scene/text/Text.h"
#include "sensor/NanoKontrol2/NanoKontrol2_midi_input.h"
#ifdef WITH_openCL
#include "MSAOpenCL.h"
#include "scene/clPingPong/ClMcCabe.h"
#include "scene/clPhysarum/ClTrailPulse.h"
#endif
#include "Pool.h"
#include "ofApp.h"

#ifdef WITH_openCL
msa::OpenCL openCL;
#endif


//--------------------------------------------------------------
void ofApp::setup(){
    ofSetVerticalSync(Configuration::verticalSync);
    mbData.setup();

#ifdef WITH_openCL
    int num_cl_gpu = openCL.getDeviceInfos(CL_DEVICE_TYPE_GPU);
    if(num_cl_gpu > 0) {
        openCL.setupFromOpenGL();
    }
    else {
        ofLogError() << "No GPU OpenCL device found.";
    }
#else
    ofLog(OF_LOG_NOTICE) << "removed OpenCL support ";
#endif

#ifdef WITH_nanoKontrol2_midi_input
    nk0.setup();
#else
    ofLog(OF_LOG_NOTICE) << "removed NanoKontrol2 support ";
#endif // WITH_nanoKontrol2_midi_input

#ifdef WITH_testcases
    stringhelper_runTestCases();
    ScalarRPN::runTestCases();
    ScalarLispTest::runTestCases();
    ScalarLispTest::runProfile();
    ScalarLispMng::runTestCases();
    LsysMachineTest::runTestCases();
#endif // WITH_testcases

    Pool::treeImagePool.append("trees/*.png", 10);
    Pool::treeImagePool.resize(Configuration::windowWidth, Configuration::windowHeight);

#ifdef WITH_Cv
    Pool::treeCvImagePool.append(Configuration::windowWidth, Configuration::windowHeight, true, "trees/*.png", 25);
#endif // WITH_Cv

    if(Configuration::screenMode == ScreenMode::FULLSCREEN) {
    	// cd bin/data/dance_p1080
    	// ffmpeg -i ../kulliStoneDance.mp4 -vf scale=1920:1080 kulliStoneDance_p1080.mp4
    	// ffmpeg -i ../snowdance1.mp4 -vf scale=1920:1080 snowdance1_p1080.mp4
        Pool::stoneMoviePool.append("dance_p1080/*.mp4", 2);
        Pool::roadMoviePool.append("roadSide_p1080/*.mp4", 2);
    }
    else {
    	// cd bin/data/dance_p540
    	// ffmpeg -i ../kulliStoneDance.mp4 -vf scale=960:540 kulliStoneDance_p540.mp4
    	// ffmpeg -i ../snowdance1.mp4 -vf scale=960:540 snowdance1_p540.mp4
        Pool::stoneMoviePool.append("dance_p540/*.mp4", 2);
    }
    Pool::stoneMoviePool.resize(Configuration::windowWidth, Configuration::windowHeight);
    Pool::roadMoviePool.resize(Configuration::windowWidth, Configuration::windowHeight);

    Pool::snowyMoviePool.append("snowyBackground.mp4", 1);
    Pool::snowyMoviePool.resize(Configuration::windowWidth, Configuration::windowHeight);

    Pool::iceScapeMoviePool.append("iceScape_p1080/iceScape_p1080.mp4", 1);
    Pool::iceScapeMoviePool.resize(Configuration::windowWidth, Configuration::windowHeight);

    Pool::hangingMoviePool.append("hanging/*.mp4", 6);
    Pool::hangingMoviePool.resize(Configuration::windowWidth, Configuration::windowHeight);

    // Pool::soundPool.append("sounds/*.wav", 6);

    // index 0 test image for both walls
//    current = std::make_shared<Testcard>();
//    current->setup();
//    scenePool.push_back(current);

    // index 0 distortion calculated in the fragment shader
    current = std::make_shared<FragDistortion>();
    current->setup();
    scenePool.push_back(current);

    // index 1 all black
    current = std::make_shared<Black>();
    current->setup();
    scenePool.push_back(current);

    // index 2 Physarum
    current = std::make_shared<BackWallMoviePhysarum>();
    current->setup();
    scenePool.push_back(current);

    // index 3 Physarum
    current = std::make_shared<MovieImagePhysarum>();
    current->setup();
    scenePool.push_back(current);

    // index 4 NumberFlock
    current = std::make_shared<NumberFlock>();
    current->setup();
    scenePool.push_back(current);

    // index 5 MovieMixer
    current = std::make_shared<MovieMixer>();
    current->setup();
    scenePool.push_back(current);

    // index 6 MovieDistortion
    current = std::make_shared<MovieDistortion>();
    current->setup();
    scenePool.push_back(current);

    // index 7 Image2Polyline
#ifdef WITH_Cv
    current = std::make_shared<SimpleContour>();
    current->setup();
    scenePool.push_back(current);
#endif // WITH_Cv

    // index 8 LsysScene
    current = std::make_shared<LsysScene>();
    current->setup();
    scenePool.push_back(current);

    // index 9 CleanPhysarum
    current = std::make_shared<CleanPhysarum>();
    current->setup();
    scenePool.push_back(current);

    // index 10 RoadMovie
    current = std::make_shared<RoadMovie>();
    current->setup();
    scenePool.push_back(current);

    // index 11 Text
    current = std::make_shared<Text>();
    current->setup();
    scenePool.push_back(current);

    //
    // index 10 TODO OpenCL
#ifdef WITH_openCL
    // index 11
    current = std::make_shared<ClMcCabe>();
    current->setup();
    scenePool.push_back(current);

    // index 12 Physarum
    current = std::make_shared<ClTrailPulse>();
    current->setup();
    scenePool.push_back(current);
#endif


    loadState();
    switchScene(Configuration::startIndex, "");

    frame = 0;
    ofSetFrameRate(Configuration::fps);

    oscReceiver.setup(port);
}

//--------------------------------------------------------------
void ofApp::update() {
	try {
	    ++frame;
		Pool::update();

#ifdef WITH_testcases
		{
			ScalarLispMng mng;
		    std::vector<std::string> sl_src;
		    sl_src.push_back("(defun mb0avr (/ (+ mb0x mb0y mb0z) 3))");
		    ScalarLisp slTest0;
		    mng.hasSlProg = true; // path for testing only
			mng.compile(slTest0, "mb0avr", "0", sl_src);
			printf("%s\n", slTest0.rpn.dump().c_str());
		    double test0 = slTest0.rpn.execute0();
		    double expected0 = (mbData.getMiniBee(0).getX() + mbData.getMiniBee(0).getY() + mbData.getMiniBee(0).getZ()) / 3.0;
		    printf("%f %f %f\n", expected0, test0, expected0 - test0);
		}
		{
			ScalarLispMng mng;
		    std::vector<std::string> sl_src;
		    sl_src.push_back("(defun sdmb7 mb7_smooth_delta)");
		    ScalarLisp slTest7;
		    mng.hasSlProg = true; // path for testing only
			mng.compile(slTest7, "sdmb7", "0", sl_src);
			printf("%s\n", slTest7.rpn.dump().c_str());
		    double test7 = slTest7.rpn.execute0();
		    double expected7 = mbData.getMiniBee(7).getDeltaSmooth();
		    printf("%f %f %f\n", expected7, test7, expected7 - test7);
		}
#endif // WITH_testcases

		if (current) {
			if (Configuration::time_trace) {
				timespec time1, time2;
				clock_gettime(CLOCK_MONOTONIC, &time1);
				current->update();
				clock_gettime(CLOCK_MONOTONIC, &time2);
				ofLog(OF_LOG_NOTICE) << "ofApp::update    "
						             << (time2.tv_sec * 1000000000l + time2.tv_nsec)
								        - (time1.tv_sec * 1000000000l + time1.tv_nsec);
			} else {
				current->update();
			}
		}
	} catch (const std::exception &ex) {
		ofLogError() << "ofApp::update failed with exception '" << ex.what() << "'";
	}

    while(oscReceiver.hasWaitingMessages()) {
        ofxOscMessage m;
        oscReceiver.getNextMessage(m);

        if (m.getAddress() == "/dimmer") {
            if(m.getNumArgs() == 1) {
                dimmer = ofClamp(m.getArgAsFloat(0), 0.0, 1.0);
            }
        }
        else if (m.getAddress() == "/scene") {
            switch(m.getNumArgs()) {
                case 1:
                    switchScene(std::max(m.getArgAsInt(0), 0), "");
                    break;
                case 2:
                    switchScene(std::max(m.getArgAsInt(0), 0), m.getArgAsString(1));
                    break;
            }
        } 
        else if (m.getAddress() == "/setup") {
            if(m.getNumArgs() == 1) {
                size_t s = m.getArgAsInt(0);
                scenePool[s]->setup();
            }
        }
        else if (m.getAddress() == "/amplitude") {
            if(m.getNumArgs() == 3) {
                amplitude = m.getArgAsFloat(0);
                pitch0 = m.getArgAsFloat(1);
                pitch1 = m.getArgAsFloat(2);
            }
        }
        else if (m.getAddress() == "/minibee/data" && m.getNumArgs() == 4) {
            int id = m.getArgAsInt(0);
            float x = m.getArgAsFloat(1);
            float y = m.getArgAsFloat(2);
            float z = m.getArgAsFloat(3);

            mbData.update(id, x, y, z);
        }
    }
    
}

//--------------------------------------------------------------
void ofApp::draw(){
	try {
		if (current) {
			ofPushView();
			ofPushStyle();
			if (Configuration::time_trace) {
				timespec time1, time2;
				clock_gettime(CLOCK_MONOTONIC, &time1);
				current->drawLeftWall();
				clock_gettime(CLOCK_MONOTONIC, &time2);
				ofLog(OF_LOG_NOTICE) << "ofApp::draw      "
						             << (time2.tv_sec * 1000000000l + time2.tv_nsec)
								        - (time1.tv_sec * 1000000000l + time1.tv_nsec);
			} else {
				current->drawLeftWall();
			}
			ofPopStyle();
			ofPopView();
		}

		if(showFrameRate) {
			ofDrawBitmapStringHighlight(to_string((int) std::round(ofGetFrameRate())), 0, 20);
			ofDrawBitmapStringHighlight(current->sceneName(), 0, 40);
			ofDrawBitmapStringHighlight(dumpState().c_str(), 0, 60);
            ofDrawBitmapStringHighlight(to_string((float) amplitude), 0, 220);
            ofDrawBitmapStringHighlight(to_string((float) pitch0), 0, 240);
            ofDrawBitmapStringHighlight(to_string((float) pitch1), 0, 260);
			ofDrawBitmapStringHighlight(Pool::dumpMovieState().c_str(), 0, 290);
#ifdef WITH_nanoKontrol2_midi_input
			ofDrawBitmapStringHighlight(dumpNkState().c_str(), 0, 400);
#endif // WITH_nanoKontrol2_midi_input
		}
	} catch (const std::exception &ex) {
		ofLogError() << "ofApp::draw failed with exception '" << ex.what() << "'";
	}
}

void ofApp::drawRight(__attribute__((unused)) ofEventArgs& args ){
	try {
		if (current) {
			ofPushView();
			ofPushStyle();
			if (Configuration::time_trace) {
				timespec time1, time2;
				clock_gettime(CLOCK_MONOTONIC, &time1);
				current->drawRightWall();
				clock_gettime(CLOCK_MONOTONIC, &time2);
				ofLog(OF_LOG_NOTICE) << "ofApp::drawRightWall "
						             << (time2.tv_sec * 1000000000l + time2.tv_nsec)
								        - (time1.tv_sec * 1000000000l + time1.tv_nsec);
			} else {
				current->drawRightWall();
			}
			ofPopStyle();
			ofPopView();
		}
	} catch (const std::exception &ex) {
		ofLogError() << "ofApp::drawRightWall failed with exception '" << ex.what() << "'";
	}

}

void ofApp::exit(){
    saveState();
    ofLog() << "Ciao, my friend!";
}

void ofApp::switchScene(size_t nextIndex, const string& nextSl_name) {
	size_t ni = nextIndex % scenePool.size();
	if(ni != currentIndex) {
		Pool::pause();
		currentIndex = ni;
		current = scenePool[currentIndex];
	}
	// for an empty nextSl_name function file_exists returns false
	if(file_exists(nextSl_name)) {
		current->setSlProgName(nextSl_name);
		current->reloadSl(true);
	}
}

void ofApp::toggleSoundPlaying(size_t index) {
    bool p = Pool::soundPool.isPlayingAt(index);
    if (p){
        Pool::soundPool.stop(index);
    } else {
        Pool::soundPool.play(index);
    };
}
//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	// delegate key press only to the currently active scene.
	if (current) {
		current->keyPressed(key);
	}

	switch (key) {
	case '!': {
		// provoke a segment violation. for testing the run_release bash script.
//		static int* n = nullptr;
//		*n = 1;
		break;
	}
	case '.':
		showFrameRate = !showFrameRate;
		break;
	case '-':
		ofLog() << "\n"
				<< dumpState()
#ifdef WITH_nanoKontrol2_midi_input
				<< dumpNkState()
#endif // WITH_nanoKontrol2_midi_input
				<< "\n";
		break;
	case 'q':
		for(auto s : scenePool) {
			s->exit();
		}
		// same as:
		// kill -9 <pid>
		std::exit(137);
		break;
	case ' ':
		switchScene(currentIndex+1, "");
		break;
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		switchScene(key-'0', "");
		break;
        // starting a new row of keys...
    // case 'a':
    //     switchScene(10);
    //     break;
    // case 'z':
    //     toggleSoundPlaying(0);
    //     break;
    // case 'x':
    //     toggleSoundPlaying(1);
    //     break;
    // case 'c':
    //     toggleSoundPlaying(2);
    //     break;
    // case 'v':
    //     toggleSoundPlaying(3);
    //     break;
    // case 'b':
    //     toggleSoundPlaying(4);
    //     break;
	}

//	// delegate key press to all scene instances.
//	// even the not visible scenes are informed.
//	for(auto s : scenePool) {
//		s->keyPressed(key);
//	}
}


