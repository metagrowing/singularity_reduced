#version 330
in vec2 texCoordVarying; // not used
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform int w; // width,  not used
uniform int h; // height, not used
uniform float p0; // generic parameter 0, calculated by a *.sl program
uniform float p1; // generic parameter 1, calculated by a *.sl program
uniform float dimmer;

#define width_of_two_stipes 30

void main()
{
    float m = int(gl_FragCoord.x) % width_of_two_stipes;
    if(m < (width_of_two_stipes / 2)) {
        vec3 cin0 = texture(tex0, vec2(gl_FragCoord.x,
                                       h-gl_FragCoord.y-p0)).xyz;
        // draw originnal inverted color channels
        outputColor = vec4((vec3(1)-cin0)*dimmer, 1.0);
    }
    else {
        vec3 cin1 = texture(tex1, vec2(gl_FragCoord.x,
                                       h-gl_FragCoord.y+p1)).xyz;
        // draw originnal color channels
        outputColor = vec4(cin1*dimmer, 1.0);
    }
}
