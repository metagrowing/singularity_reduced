#version 330
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform sampler2DRect tex2;
//uniform sampler2DRect tex3; // not used
uniform int h; // height
uniform float dimmer;

void main()
{
    vec3 cin0 = texture(tex0, vec2(gl_FragCoord.x, h-gl_FragCoord.y)).xyz;
    vec3 cin1 = texture(tex1, gl_FragCoord.xy).xyz;
    vec3 cin2 = texture(tex2, vec2(gl_FragCoord.x, h-gl_FragCoord.y)).xyz;
    outputColor = vec4(dimmer * mix(cin0, cin1, length(cin2)), 1.0);
}
