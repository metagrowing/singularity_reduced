; used in Physarum. Physarum is implemented in the 0 .. 255 range, unsigned char
(defun hue 63)
; animate saturation
(defun sat (+ 150 (* 100 (sin (* 0.1 f)))))
(defun val 255)
(defun connectedLeft mb0_iscon)
(defun connectedLeft mb1_iscon)
(defun deltaLeft mb0_delta)
(defun deltaRight mb1_delta)
(defun maxSpeed (map (+ mb0_delta mb1_delta) 0 2 0 45))
; (defun maxSpeed (map (+ mb0_delta mb1_delta) 0 2 1 80))
; (defun maxSpeed (* 40 (+ mb0_delta mb1_delta)))
; (defun senseDist (map (+ mb0_smooth_delta mb1_smooth_delta) 0 2 3 1200))
(defun senseDist (map (+ mb0_smooth_delta mb1_smooth_delta) 0 2 4 800))
;(defun senseDist (* 300 ks0))
(defun strength 1)
(defun scale 3)
(defun off 0.05)
