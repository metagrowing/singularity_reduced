; used in Physarum. Physarum is implemented in the 0 .. 255 range, unsigned char
; (defun hue (clamp (* 255  mb0x) 0 255))
(defun hue 0)
; animate saturation
; (defun sat (+ 150 (* 10 mb1x_smooth)))
; (defun sat (map (sin (* 0.1 f) -1.0 1.0 200 255)))
(defun sat 255)
; (defun sat (+ 150 (* 100 (sin (* 0.1 f)))))
(defun val 255)
; animate deposit amount and decay rate of Physarum from nanoKontrol2
;(defun deposit (* ks0 255))
;(defun decay ks1)
; constant deposit amount and decay rate for Physarum
(defun deposit (map amplitude 0 1 400 1200))
(defun decay (clamp (map amplitude 0 1 0 10) 0.2 1))
; (defun deposit (+ 400 (* (+ mb0_smooth_delta mb1_smooth_delta) 400 )))
; (defun decay (clamp (max (* (+ mb0_smooth_delta mb1_smooth_delta) 100) 0.2) 0.2 1))
; (defun delta (* 25 (+ mb0_delta mb1_delta mb2_delta mb3_delta)))
(defun delta 0 )
(defun maxSpeed (map amplitude 0 1 0 80))
; (defun maxSpeed (* 40 (+ mb0_smooth_delta mb1_smooth_delta)))
; (defun decay 0.9)
(defun senseDist (map amplitude 0 1 6 850))
; (defun senseDist (map (+ mb0_smooth_delta mb1_smooth_delta) 0 2 3 350))
;(defun senseDist (* 300 ks0))
(defun strength 1)
(defun scale 8)
(defun off 0.05)
(defun cx 0.5)
(defun cy 0.65)
