#version 330
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform int w; // width
uniform int h; // height
uniform float p0; // generic parameter 0, calculated by a *.sl program
uniform float p1; // generic parameter 1
uniform float p2; // generic parameter 2
uniform float p3; // generic parameter 3
uniform float p4; // generic parameter 4
uniform float p5; // generic parameter 5
uniform float p6; // generic parameter 6
uniform float p7; // generic parameter 7
uniform float dimmer;

vec2 distort(vec2 pin)
{
    // translate to +/- coordinates and move to center
    vec2 pcenter = pin / vec2(w, h);
    pcenter -= vec2(0.5);
    
    float theta  = atan(pcenter.y, pcenter.x);
    float radius = length(pcenter)-0.8*p0;
    vec2 pout;
    pout.x = radius * cos(theta);
    pout.y = radius * sin(theta);

    // translate back to fragment coordinat space.
    pout += vec2(0.5);
    return pout * vec2(w, h);
}

// for testing only
vec2 identity(vec2 p)
{
    return vec2(p.x, p.y);
}

void main()
{
    vec3 cin0 = texture(tex1, distort(vec2(gl_FragCoord.x, h-gl_FragCoord.y))).xyz;
    outputColor = vec4(cin0*dimmer, 1.0);
}
