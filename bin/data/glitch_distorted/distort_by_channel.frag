#version 330
in vec2 texCoordVarying;
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform int w;
uniform int h;
uniform float dimmer;

void main()
{
    vec3 cin1 = texture(tex1, vec2(gl_FragCoord.x, h-gl_FragCoord.y)).xyz;
    vec3 cin0 = texture(tex0, vec2(gl_FragCoord.x, h-gl_FragCoord.y)
                                + 50 * cin1.xy - 25).xyz;
    outputColor = vec4(cin0*dimmer, 1.0);
}
