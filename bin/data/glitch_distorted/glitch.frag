#version 330
in vec2 texCoordVarying;
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform int w;
uniform int h;
uniform float dimmer;

// 2d rotation matrix
mat2 rot(float angle) {
    float cos_a = cos(angle);
    float sin_a = sin(angle);
    return mat2(cos_a, -sin_a,
                sin_a,  cos_a);
}

void main() {
    vec3 cin0 = texture(tex0, vec2(gl_FragCoord.x, h-gl_FragCoord.y)).xyz;
    // use the red channel cin0.x as an rotation angle. 
    vec2 r = rot(0.5*cin0.x) * vec2(w-gl_FragCoord.x, h-gl_FragCoord.y);
    vec3 cin1 = texture(tex1, r*1.3).xyz;
    outputColor = vec4(cin1*dimmer, 1.0);
}
