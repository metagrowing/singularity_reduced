; agent velocity
;(defun p0 (* ks0 0.1))
(defun p0 (* 0.59055 0.1))

; agent sense distance;
;(defun p1 (* ks1 0.01))
(defun p1 (* 0.71654 0.01))

; agent sense angle
;(defun p2 (* ks2 1.5))
(defun p2 (* 1 1.5))

; agent rotate angle
;(defun p3 (* ks3 0.5))
(defun p3 (* 0.1811 0.5))

; not used
(defun p4 0)

; trigger to disturbe the trail map
;(defun p5 (sign (- (rand) 0.3)))
(defun p5 1)

; not color hue
;(defun p6 ks6)
(defun p6 0.094488)

; none linear scaling from trail map to color
;(defun p7 (+ ks7 0.1))
(defun p7 (+ 0.13386 0.1))
